﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public abstract class Transport : ITransport
    {
        public int HumanCount { get; set; }

        public void GoGO()
        {
            Console.WriteLine("GoGO");
        }
        public abstract void Go();

        public abstract void Stop();
    }
}
