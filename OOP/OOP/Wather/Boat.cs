﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public class Boat : WaterTransport
    {
        public override void Go()
        {
            Console.WriteLine("Boat Go");
        }

        public override void Stop()
        {
            Console.WriteLine("Boat Stop");
        }

        public override void SwimmingSpeed()
        {
            Console.WriteLine("Boat SwimmingSpeed");
        }
    }
}
