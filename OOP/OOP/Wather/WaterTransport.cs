﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public abstract class WaterTransport : Transport, ISwimmable
    {
        public string Material { get; set; }

        public abstract void SwimmingSpeed();
    }
}
