﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public class AirPlane : AirTransport
    {
        public override void Go()
        {
            Console.WriteLine("AirPlane Go");
        }

        public override void SOS_Signal()
        {
            Console.WriteLine("Airplane SOS_Signal");
        }

        public override void Stop()
        {
            Console.WriteLine("AirPlane Stop");
        }
    }
}
