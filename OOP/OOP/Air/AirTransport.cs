﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public abstract class AirTransport : Transport,IFlyable
    {
        public DateTime FlyTime { get; set; }

        public abstract void SOS_Signal();
    }
}
