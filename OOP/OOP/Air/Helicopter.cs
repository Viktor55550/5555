﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public class Helicopter : AirTransport
    {
        public override void Go()
        {
            Console.WriteLine("Helicopter Go");
        }

        public override void SOS_Signal()
        {
            Console.WriteLine("Helicopter SOS_Signal");
        }

        public override void Stop()
        {
            Console.WriteLine("Helicopter Stop");
        }
    }
}
