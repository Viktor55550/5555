﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World");
            Transport trans = new AirPlane();
            trans.Go();
            AirPlane airplane = new AirPlane();
            airplane.SOS_Signal();
            airplane.Go();
            airplane.GoGO();

            Transport heli = new Helicopter();
            heli.Go();

            Bus bus = new Bus
            {
                WheelsCount = 6
            };
            int x = bus.WheelsCount;
            Console.WriteLine($"Number of bus wheels is {x}");
            bus.Go();

            Transport land = new Bus();
            land.Go();

            Console.ReadKey();
        }
    }
}
