﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public class Car : LandTransport
    {
        public override void Beep()
        {
            Console.WriteLine("Car Beep");
        }

        public override void Go()
        {
            Console.WriteLine("Car Go");
        }

        public override void Stop()
        {
            Console.WriteLine("Car Stop");
        }
    }
}
