﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public class Bus : Automobile
    {
        public override void Beep()
        {
            Console.WriteLine("Bus Beep");
        }

        public override void Go()
        {
            Console.WriteLine("Bus Go");
        }

        public override void Stop()
        {
            Console.WriteLine("Bus Stop");
        }
    }
}
