﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public abstract class LandTransport : Transport , IDriveable
    {
        public int WheelsCount { get; set; }

        public abstract void Beep();
    }
}
