﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace construktor
{
    class Program
    {
        static void Main(string[] args)
        {
            Product product1 = new Product();
            product1.showInfo();

            Console.ReadKey();
        }
        
    }
    public class Product
    {
        public string ProductName;
        public double ProductPrice;
        public int ProductQuality;
        public bool Active;

        /*public Product()
        {
        }
        public Product(string ProductName, double ProductPrice)
        {
            this.ProductName = ProductName;
            this.ProductPrice = ProductPrice;
        }

        public Product(string ProductName, double ProductPrice, int ProductQuality)
        {
            this.ProductName = ProductName;
            this.ProductPrice = ProductPrice;
            this.ProductQuality = ProductQuality;
        }

       

        public Product(string ProductName, double ProductPrice, int ProductQuality, bool Active)
        {
            this.ProductName = ProductName;
            this.ProductPrice = ProductPrice;
            this.ProductQuality = ProductQuality;
            this.Active = Active;
         }*/

        public Product() { ProductName = "LG"; ProductPrice = 15000; ProductQuality = 100; Active = true; }

        public void showInfo()
        {
            Console.WriteLine($"Name:{ProductName} Price:{ProductPrice} " +
                $"Quality:{ProductQuality}% The best:{true} ");
        }
    }
}
