﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matematika
{
    class Program
    {
        static void Main(string[] args)
        {
            
            /*int result = Matem.Sign(-15);
            Console.WriteLine(result);*/
        }
    }
    public static class Matem
    {
        public const double PI = 3.1415926535897931;
        public const double E = 2.7182818284590451;
        
        
        
        //Matem.Abs() veradarcnum e drakan arjeq@
        public static decimal Abs(decimal value)
        {
            if (value < 0)
                return value * -1;
            else
                return value;
        }
        public static double Abs(double value)
         {
            if (value < 0)
                return value * -1;
            else
                return value;
        }
        public static float Abs(float value)
        {
            if (value < 0)
                return value * -1;
            else
                return value;
        }
        public static int Abs(int value)
        {
            if (value < 0)
                return value * -1;
            else
                return value;
        }
        /*public static short Abs(short value)
        {
            if (value < 0)
                return value * -1;
            else
                return value;
        }
        public static sbyte Abs(sbyte value)
        {
            if (value< 0)
                return value * -1;
            else
                return value;
        }*/
        public static long Abs(long value)
        {
            if (value< 0)
                return value* -1;
            else
                return value;
        }

        //Matem.BigMull veradarcnum artadryal@ vorpes long
        public static long BigMul(int a, int b)
        {
            long c = a * b;
            return c;
        }


        //Matem.Max() veradacnum e mecaguyn@
        public static short Max(short val1, short val2)
        {
            if (val1 > val2)
                return val1;
            else
                return val2;
        }
        public static ushort Max(ushort val1, ushort val2)
        {
            if (val1 > val2)
                return val1;
            else
                return val2;
        }
        public static int Max(int val1, int val2)
        {
            if (val1 > val2)
                return val1;
            else
                return val2;
        }
        public static long Max(long val1, long val2)
        {
            if (val1 > val2)
                return val1;
            else
                return val2;
        }
        public static ulong Max(ulong val1, ulong val2)
        {
            if (val1 > val2)
                return val1;
            else
                return val2;
        }
        public static float Max(float val1, float val2)
        {
            if (val1 > val2)
                return val1;
            else
                return val2;
        }
        public static double Max(double val1, double val2)
        {
            if (val1 > val2)
                return val1;
            else
                return val2;
        }
        public static decimal Max(decimal val1, decimal val2)
        {
            if (val1 > val2)
                return val1;
            else
                return val2;
        }
        public static uint Max(uint val1, uint val2)
        {
            if (val1 > val2)
                return val1;
            else
                return val2;
        }
        public static sbyte Max(sbyte val1, sbyte val2)
        {
            if (val1 > val2)
                return val1;
            else
                return val2;
        }
        public static byte Max(byte val1, byte val2)
        {
            if (val1 > val2)
                return val1;
            else
                return val2;
        }



        //Matem.Min() veradarcnum e poqraguyn@
        public static int Min(int val1, int val2)
        {
            if (val1 < val2)
                return val1;
            else
                return val2;
        }
        public static decimal Min(decimal val1, decimal val2)
        {
            if (val1 < val2)
                return val1;
            else
                return val2;
        }
        public static double Min(double val1, double val2)
        {
            if (val1 < val2)
                return val1;
            else
                return val2;
        }
        public static float Min(float val1, float val2)
        {
            if (val1 < val2)
                return val1;
            else
                return val2;
        }
        public static ulong Min(ulong val1, ulong val2)
        {
            if (val1 < val2)
                return val1;
            else
                return val2;
        }
        public static long Min(long val1, long val2)
        {
            if (val1 < val2)
                return val1;
            else
                return val2;
        }
        public static uint Min(uint val1, uint val2)
        {
            if (val1 < val2)
                return val1;
            else
                return val2;
        }
        public static ushort Min(ushort val1, ushort val2)
        {
            if (val1 < val2)
                return val1;
            else
                return val2;
        }
        public static short Min(short val1, short val2)
        {
            if (val1 < val2)
                return val1;
            else
                return val2;
        }
        public static sbyte Min(sbyte val1, sbyte val2)
        {
            if (val1 < val2)
                return val1;
            else
                return val2;
        }
        public static byte Min(byte val1, byte val2)
        {
            if (val1 < val2)
                return val1;
            else
                return val2;
        }


        //Matem.Sign() veradarcnum e 1 ete drakan e , ev -1 ete bacasakan e
        public static int Sign(sbyte value)
        {
            if (value > 0)
                return 1;
            else
                return -1;
        }
        public static int Sign(decimal value)
        {
            if (value > 0)
                return 1;
            else
                return -1;
        }
        public static int Sign(double value)
        {
            if (value > 0)
                return 1;
            else
                return -1;
        }
        public static int Sign(float value)
        {
            if (value > 0)
                return 1;
            else
                return -1;
        }
        public static int Sign(long value)
        {
            if (value > 0)
                return 1;
            else
                return -1;
        }
        public static int Sign(short value)
        {
            if (value > 0)
                return 1;
            else
                return -1;
        }
        public static int Sign(int value)
        {
            if (value > 0)
                return 1;
            else
                return -1;
        }
        
    }

}
