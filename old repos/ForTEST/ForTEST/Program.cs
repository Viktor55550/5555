﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForTEST
{
    class Program
    {

        //vid 19
        //08 named params ev optional params->skzbnarjeqavorvac
        /*static float RectangleArea(float height,float width = 10)//with-@ optional
        {
            float area = height * width;
            return area;
        }*/
        //07 params
        /*static int Add(int N1,int N2,params int [] list)
        {
            int sum = N1 + N2;
            foreach (int item in list)
            {
                sum = sum + item;
            }
            return sum;
        }*/
        //06 out a-n spasum e vor x-@ skzbnarjeqavori iren
        /*static void SampleMethod(out int x)
        {
            //Console.WriteLine("Initial value of formalal parametr: " + x);
            x = 100;
            Console.WriteLine("Final value of formalal parametr: " + x);//->100
        }*/
        //05 ref x-@ spasum e vor a-n skzbnarjeqavori iren
        /*static void SampleMethod(ref int x)
        {
            Console.WriteLine("Initial value of formalal parametr: " + x);//->10
            x = 100;
            Console.WriteLine("Final value of formalal parametr: " + x);//->100
        }*/
        //04
        /*static char GetGrade()
        {
            float percent = GetPercentage();
            if (percent >= 80)
                return 'A';
            if (percent >= 60)
                return 'B';
            if (percent >= 40)
                return 'C';
            else
                return 'D';
        }

        static float GetPercentage()
        {
            float totalmarks = GetTotal();
            float percentage = totalmarks * 100 / 300;
            return percentage;
        }

        static float GetTotal()
        {
            Console.Write("Enter marks1 : ");
            float marks1 = float.Parse(Console.ReadLine());
            Console.Write("Enter marks2 : ");
            float marks2 = float.Parse(Console.ReadLine());
            Console.Write("Enter marks3 : ");
            float marks3 = float.Parse(Console.ReadLine());
            float total = marks1 + marks2 + marks3;
            return total;
        }*/
        //03
        /*static int Add(int x, int y)
        {
            int sum = x + y;
            return sum;
        }*/
        //02
        /*static void Add(int x,int y)//formal arguments
        {
            int sum = x + y;
            Console.WriteLine(sum);
        }*/
        //01
        /*static void SampleMethod()
        {
            Console.WriteLine("This is sample method...");
        }*/
        static void Main(string[] args)
        {
            //vid 19 methods
            /*//08 named params ev optional params->skzbarjeqavorvac
            //float result = RectangleArea(width: 12,height: 10.5f);//width-@ erkrord paramn e bayc  
            //aysteg menq iren arajinn enq kanchel
            //named params-i jamanak argument@ arjeqavorum enq invoki vaxt
            float result = RectangleArea(height: 10.5f);
            Console.WriteLine(result);*/
            //07 params
            /*Console.WriteLine(Add(10,20,30,40,50));//ktpi 150
            Console.WriteLine(Add(10,20));//ktpi 30*/
            //06 out a-n spasum e vor x-@ skzbnarjeqavori iren
            /*int a; //= 10;
            //Console.WriteLine("Initial value of actual parametr: " + a);
            SampleMethod(out a);
            Console.WriteLine("Final value of actual parametr: " + a);//->100*/
            //05 ref x-@ spasum e vor a-n skzbnarjeqavori iren
            /*int a = 10;
            Console.WriteLine("Initial value of actual parametr: "+ a);//->10
            SampleMethod(ref a);
            Console.WriteLine("Final value of actual parametr: " + a);//->100*/
            //04
            /*Console.WriteLine("Grade : " + GetGrade());*/
            //03
            /*int num1 = 10, num2 = 20;
            int result = Add(num1, num2);
            Console.WriteLine(result);*/
            //02
            /*int num1 = 10, num2 = 20;
            Add(num1, num2);//actual arguments
            Add(20, 50);*/
            //01
            /*Console.WriteLine("Program started");
            SampleMethod();
            Console.WriteLine("Program ended");*/

            //vid 18 array
            //jagged-i tpum
            /*int[][] arr = new int[3][];
            arr[0] = new int[3] { 1, 2, 3 };
            arr[1] = new int[2] { 10, 20 };
            arr[2] = new int[4] { 11, 22, 33, 44 };
            foreach (int[] ar in arr)
            {
                foreach (int item in ar)
                {
                    Console.Write(item + "\t");
                }
                Console.WriteLine("\n");
            }*/
            //matrix tpum
            /*int[,] arr = new int[3, 4] { { 1,2,3,4 }, { 10,20,30,40 }, { 11,22,33,44 } };
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.Write(arr[i,j] + "\t");
                }
                Console.WriteLine("\n");
            }*/
            //int[] arr = new int[] { 7,3,5,1,9,2 };//nerqevi orinakneri hamar haytararvac miachap
            //voronum
            /*int num = int.Parse(Console.ReadLine());
            bool found = false;
            for (int i = 0; i < arr.Length; i++)
            {
                if (num == arr[i])
                {
                    found = true;
                    break;
                }
            }
            if (found)
                Console.WriteLine("Element found");
            else
                Console.WriteLine("Element not found");*/
            //bubble sort
            /*int temp;
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr.Length - 1; j++)
                {
                    if (arr[j] > arr[j + 1])
                    {
                        temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
            foreach(int bulki in arr)
            {
                Console.WriteLine(bulki);
            }*/
            /*foreach(int tandz in arr)
            {
                Console.WriteLine(tandz);
            }*/

            /*for (int i = 0;i < arr.Length ; i++)//i < 6,i<=5,i<=arr.Length-1
            {
                Console.Write(arr[i]);
            }*/

            //vid 17 jump statements
            //return
            /*bool tandz = true;
            while (tandz)
            {
                Console.Write("Enter the first number: ");
                int num1 = int.Parse(Console.ReadLine());
                Console.Write("Enter the second number: ");
                int num2 = int.Parse(Console.ReadLine());

                if (num2 == 0)
                {
                    Console.WriteLine("Cannot devide a number by 0");
                    return;
                }
                int result = num1 / num2;
                Console.WriteLine($"Result is:{result}");
            }*/
            //goto
            /*int i = 1;
        abc:
            Console.WriteLine(i);
            i++;
            if (i <= 10)
                goto abc;*/
            //continue
            /*for (int i = 1; i <= 10; i++)
            {
                if (i == 5 || i == 7)
                    continue;

                Console.WriteLine("i= " + i);
            }*/
            //break
            /*for (int i = 1; i <= 10; i++)
            {
                if (i == 5)
                    break;

                Console.WriteLine("i= " + i);
            }*/

            //vid 16 do while loop
            /*string actualpin = "4321";
            int count = 0;
            string pin;
            do
            {
                pin = Console.ReadLine();
                count++;
            } while (pin != actualpin && count < 3);

            if (pin == actualpin)
                Console.WriteLine("Welcome User");
            else
                Console.WriteLine("Account Locked");

            //vid 15 while loop
            //parzum enq te nermucvac tiv@ qani nishanoc e
            /*int num = int.Parse(Console.ReadLine());//nermucvac tiv
            int temp = num;//ciklum nisher@ hashvelu hamar
            int digits = 0;//nisheri qanak
            while (temp != 0)
            {
                digits++;
                temp = temp / 10;//sa nisher@ hertov krchatum e
            }
            Console.WriteLine(num + " is " + digits + "digit number");


            //arajin 100 hat parz tverii tpum
            bool isprime = true;//ete mi tiv isprime-@ false sarqel sa 
            //hajord ptuyti hamar true e sarqum
            int count = 0;//hashvum e num-eri qanak@
            int num = 2;//mer tver@ voronq ciklum piti stuven parz linelu hamar
            while (count < 100)
            {
                isprime = true;
                for (int i = 2; i <= Math.Sqrt(num); i++)
                {
                    if (num % i == 0)
                    {
                        isprime = false;
                        break;
                    }
                }
                if (isprime)
                {
                    Console.Write(num + "\t");
                    count++;
                }
                num++;
            }*/

            //vid 14 FOR LOOP
            //100-ic poqr bolor parz tveri tpum
            /*for (int num = 2;num <= 100; num++)
            {
                //int num = int.Parse(Console.ReadLine());
                bool isprime = true;
                for (int i = 2; i <= Math.Sqrt(num); i++)
                {
                    if (num % i == 0)
                    {
                        isprime = false;
                        break;
                    }
                }
                if (isprime)
                    Console.Write(num + "\t");
                    /*Console.WriteLine("number is prime");
                else
                    Console.WriteLine("number is not prime");
            }*/



            //vid 13 loop/iteration statement
            //1-10 hatvacum zuygeri ev nranc gumari tpum
            /*int sum = 0;
            for (int i = 1; i <= 10; i++)
            {
                if (i % 2 == 0)
                {
                    Console.WriteLine(i);
                    sum = sum + i;
                }
            }
            Console.WriteLine("-----------------");
            Console.WriteLine(sum);*/

            //vid 12 switch statement
            /*char ch = char.Parse(Console.ReadLine());
            string s = ch.ToString().ToLower();
            switch (s)
            {
                case "a":
                    Console.WriteLine("a is a vowel");
                    break;
                case "e":
                    Console.WriteLine("e is a vowel");
                    break;
                case "i":
                    Console.WriteLine("i is a vowel");
                    break;
                case "o":
                    Console.WriteLine("o is a vowel");
                    break;
                case "u":
                    Console.WriteLine("u is a vowel");
                    break;
                default :
                    Console.WriteLine("Constant / Special character");
                    break;
            }

            //vid 11 if else statement
            /*Console.Write("Enter your percentage: ");
            float percent = float.Parse(Console.ReadLine());

            if (percent >= 80)
                Console.WriteLine("A");
            if (percent >= 60)
                Console.WriteLine("B");
            if (percent >= 40)
                Console.WriteLine("C");
            else
                Console.WriteLine("D");


                int num = int.Parse(Console.ReadLine());

                if (num % 2 == 0)
                {
                    Console.WriteLine("it is even number");
                }
                else
                {
                    Console.WriteLine("it is odd number");
                }*/

            /*Console.Write("Your name: ");
            string username = Console.ReadLine();
            Console.Write("Your cod: ");
            int password = int.Parse(Console.ReadLine());

            string valid = (username == "andi" && password == 25) ?
                "Welcome user" : "invalid user";
            Console.WriteLine(valid);*/

            /*int a = 10;//,b=15,c=20
            int b = ++a;
            Console.WriteLine(a);
            Console.WriteLine(b);*/

            /*int marks = 72, max = 80;

            int perc = marks / max * 100;
            int per = marks * 100 / max;

            Console.WriteLine("arajin tarberak" + perc);
            Console.WriteLine(per + "erkrord tarberak");*/


            /*int a = 10;
            Console.WriteLine(""+ a + sizeof(int) + a.GetType());
            //Console.WriteLine(typeof(int));
            Console.WriteLine(a.GetType());
            Console.WriteLine(sizeof(int));*/




            Console.ReadKey();



        }
    }
}
