﻿using System;

namespace ForTEST03
{
    //vid-46
    delegate int Del(int x);
    delegate int Del2(int x,int y);
    delegate void Del3(int x, int y);

    //vid-45
    /*delegate void Del();
    delegate int Del1(int x);*/


    //vid-44 in program


    //vid-43
    /*delegate int MyDel(int x, int y);
    delegate void MyDel2(int x, int y);*/


    //vid-42
    /*enum Color
    {
        White = 0,
        Black = 1,
        Orange//=2
    }
    class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Color Color { get; set; }
    }*/


    class Program
    {
        //vid-44
        /*public static void SampleMethod(string name)
        {
            Console.WriteLine($"Welcome { name}");
        }
        public static int Add(int x,int y)
        {
            return x + y;
        }
        public static bool Login(string uid)
        {
            if (uid == "andi")
                return true;
            else
                return false;
        }*/

        //vid-43
        /*public static int Add(int x, int y)
        {
            return x + y;
        }
        public static int Multiply(int x, int y)
        {
            return x * y;
        }
        public static int Devide(int x, int y)
        {
            return x - y;
        }
        public static void Add2void(int x, int y)
        {
            Console.WriteLine(x + y);
        }
        public static void Multiply2void(int x, int y)
        {
            Console.WriteLine(x * y);
        }
        public static void tandz(int x, int y)
        {
            Console.WriteLine("aziz" + x * y);
        }*/

        static void Main(string[] args)
        {
            //vid-46 Lambda Expression
            Del del = x => x * x;
            int a = 2 * 4;
            int rez = del(4);
            Console.WriteLine(rez);
            
            Del deleg = delegate (int x)
            {
                Console.WriteLine("This is Anonymous Method");
                return 0;
            };

            Del2 del2 = (x,y) => x * y;
            int b = 2 * 4;
            int rez2 = del2(4,6);
            Console.WriteLine(rez2);

            Del3 del3 = (x, y) => 
            {
                Console.WriteLine(x * y);
                Console.WriteLine("hajox");
            };
            del3(4, 6);


            //vid-45 Anonymous Method
            /*Del del = delegate ()
            {
                Console.WriteLine("This is Anonymous Method");
            };
            Del1 del1 = delegate (int x)
            {
                Console.WriteLine("This is Anonymous Method");
                return 0;
            };*/

            //vid-44 generic delegate
            /*Action<string> action1 = SampleMethod;
            Func<int, int, int> func1 = Add;
            Predicate<string> pred1 = Login;
            action1("Tut");*/

            //vid-43 delegate ==>class program
            /*MyDel del = new MyDel(Add);
            //minchev (+=)-ov grel@
            //int rez = del(40, 30);
            //Console.WriteLine(rez);
            //del = Multiply;
            //rez = del(40, 30);
            //Console.WriteLine(rez);
            del += Multiply;//multicast
            del -= Devide;
            del -= Add;
            int rez = del(40, 30);//kanchum e verjin (+=)-ov@
            Console.WriteLine(rez);

            MyDel2 del2 = Add2void;//void-ov bolor (+=) egacner@ kanchelu e
            del2 += Multiply2void;
            del2 += tandz;
            del2(30, 50);//=>80-1500*/


            //vid-42 enum
            /*Person p1 = new Person
            {
                Name = "Andi",
                Age = 28,
                Color = Color.Orange
            };

            int col = (int)p1.Color;
            Console.WriteLine(col);//2*/



            Console.ReadKey();
        }
    }
}
