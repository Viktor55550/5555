﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STACK_CLASS_Tnayin
{
    class MyStack : ICollection, IEnumerable
    {
        private int[] _array { get; set; }

        private int position = -1;

        public int Current
        {
            get
            {
                return _array[position];
            }
        }

        public MyStack()
        {
            _array = new int[] {};
        }
        public MyStack(string a)
        {
            _array = a.ToCharArray();
        }
        public int this[int index]
        {
            get
            {
                return this._array[index];
            }
            set
            {
                this._array[index] = value;
            }
        }
        public IEnumerator GetEnumerator()
        {
            return this;
        }
        public bool MoveNext()
        {
            if (position < _array.Length - 1)
            {
                position++;
                return true;
            }
            return false;
        }
        public void Reset()
        {
            position = -1;
        }

        public void Clear()
        {
            _array = new int[0];
        }

        public bool Contains(int item)
        {
            for (int i = 0; i < _array.Length; i++)
            {
                if (_array[i].Equals(item))
                {
                    return true;
                }
            }

            return false;
        }

        private bool _remove(int item)
        {
            int[] temp = new int[_array.Length - 1];
            bool isFound = false;

            for (int i = 0, j = 0; i < _array.Length; i++, j++)
            {
                if (_array[i].Equals(item) && !isFound)
                {
                    isFound = true;
                    j--;
                    continue;
                }
                if (i != j || i != _array.Length - 1)
                {
                    temp[j] = _array[i];
                }
            }

            if (isFound)
            {
                _array = temp;
            }

            return isFound;
        }

        public int Pop()
        {
            _remove(_array[_array.Length-1]);
            return _array[_array.Length - 1];
        }
        
        public int Peek()
        {
            return _array[_array.Length - 1];
        }

        public void Push(int insertedObject)
        {
            int[] temp = new int[_array.Length + 1];

            for (int i = 0; i < _array.Length; i++)
            {
                temp[i] = _array[i];
            }
            temp[0] = insertedObject;

            _array = temp;
        }

        public void ToArray()
        {
            int[] arr;
            _array = arr;
        }

        public bool Equals(int obj)
        {
            if (Current == obj)
                return true;
            else
                return false;
        }
    }
}
