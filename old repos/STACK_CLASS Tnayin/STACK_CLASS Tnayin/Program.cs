﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace STACK_CLASS_Tnayin
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int a = 5;
                int b = 0;
                int c = a / b;
                bool t = true;
                int f = default(int);
            }
            catch (LogicException ex) when (f > 5)
            {
                Console.WriteLine(ex.Message);
                // log to database
                throw new ArgumentException();
            }
            catch (ArgumentException ex) when (f < 0)
            {
                Console.WriteLine("Global Exception");
            }
            finally
            {
                if (!t)
                {
                    f = 6;
                }
                else
                {
                    f = 5;
                }
            }
        }
    }
}
