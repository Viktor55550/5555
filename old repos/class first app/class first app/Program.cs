﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace class_first_app
{
    class Program
    {
        static void Main(string[] args)
        {

            /*Ram kingston16A = new Ram();

            kingston16A.Generation = "DDR 5";
            kingston16A.MemorySize = 4;
            kingston16A.Speed = 1.6;
            kingston16A.ModelName = "Kingston A160";

            kingston16A.ShowInfo();
            Console.WriteLine();
            //Console.ReadKey(kingston16A.ModelName);*/

            Helicopter BlackHawk = new Helicopter();
            BlackHawk.Name = "BlackHawk Hawk UH 60";
            BlackHawk.Manufacturerr = "Sikorsky Aircraft";
            BlackHawk.FirstFlightt = 1974;
            BlackHawk.Statuss = "In servise";
            BlackHawk.RangeOfFlightt = 583;
            BlackHawk.MaximumHeightt = 30;
            BlackHawk.Speedd = 357;
            BlackHawk.Weightt = 6191;

            BlackHawk.ShowInfo();
            
            Console.WriteLine("-----------------------------------");

            Aircraft Mig29 = new Aircraft();

            Mig29.ModelName = "Mig 29";
            Mig29.Manufacturer = "Mikoyan";
            Mig29.FirstFlight = 1997;
            Mig29.Status = "In servise";
            Mig29.RangeOfFlight = 1500;
            Mig29.MaximumHeight = 22500;
            Mig29.Speed = 2446;
            Mig29.Weight = 11000;

            Mig29.ShowInfo();


            Console.ReadKey();

        }
    }

    public class Helicopter
    {
        public string Name;
        public string Manufacturerr;
        /// <summary>
        /// 6 October year
        /// </summary>
        public int FirstFlightt;
        public string Statuss;
        /// <summary>
        /// in km
        /// </summary>
        public int RangeOfFlightt;
        /// <summary>
        /// in meters
        /// </summary>
        public int MaximumHeightt;
        /// <summary>
        /// in km/h
        /// </summary>
        public int Speedd;
        /// <summary>
        /// in kg
        /// </summary>
        public int Weightt;

        public void ShowInfo()
        {
            Console.WriteLine($"Model:{Name}");
            Console.WriteLine($"Manufactured by:{Manufacturerr}");
            Console.WriteLine($"FirstFlight in:17 October {FirstFlightt} year");
            Console.WriteLine($"Current status is:{Statuss}");
            Console.WriteLine($"Maximal range of flight:{RangeOfFlightt}km");
            Console.WriteLine($"Helicopter maximum height:{MaximumHeightt}meters");
            Console.WriteLine($"Maximum speed:{Speedd}km/h");
            Console.WriteLine($"Helicopter weight:{Weightt}kg");
        }
    }


    public class Aircraft
    {
        public string ModelName;
        public string Manufacturer;
        /// <summary>
        /// 6 October year
        /// </summary>
        public int FirstFlight;
        public string Status;
        /// <summary>
        /// in km
        /// </summary>
        public int RangeOfFlight;
        /// <summary>
        /// in meters
        /// </summary>
        public int MaximumHeight;
        /// <summary>
        /// in km/h
        /// </summary>
        public int Speed;
        /// <summary>
        /// in kg
        /// </summary>
        public int Weight;

        public void ShowInfo()
        {
            Console.WriteLine($"Model:{ModelName}");
            Console.WriteLine($"Manufactured by:{Manufacturer}");
            Console.WriteLine($"FirstFlight in:6 October {FirstFlight} year");
            Console.WriteLine($"Current status is:{Status}");
            Console.WriteLine($"Maximal range of flight:{RangeOfFlight}km");
            Console.WriteLine($"Plane maximum height:{MaximumHeight}meters");
            Console.WriteLine($"Maximum speed:{Speed}km/h");
            Console.WriteLine($"Plane weight:{Weight}kg");
        }
    }
    /*public class Ram
    {

        public string ModelName;
        /// <summary>
        /// in GB
        /// </summary>
        public int MemorySize;
        public string Generation;
        /// <summary>
        /// in Mhz(megahertz)
        /// </summary>
        public double Speed;

        public void ShowInfo()
        {
            Console.WriteLine($"Model: {ModelName}");
            Console.WriteLine($"Memory Size: {MemorySize}Gb");
            Console.WriteLine($"Generation: {Generation}");
            Console.WriteLine($"Speed: {Speed}Mhz");
        }
    }*/

}
