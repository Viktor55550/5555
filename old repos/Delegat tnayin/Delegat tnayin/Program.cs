﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegat_tnayin
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> sourceList = new List<int>(new int[] {1, 4, 7, 5, 6, 3, 2, 9 });
            /*Random rand = new Random();
            int ran = rand.Next(List.Count);*/
            NumberFilterDelegate numberFilter = GetZuygNumber;

            List<int> filteredList = FilterNumbers(sourceList, GetZuygNumber);
            filteredList = FilterNumbers(sourceList, Get5icMecNumber);
            filteredList = FilterNumbers(sourceList, GetQarKentNumber);

            PrintList(filteredList);

            Console.ReadKey();
        }
        private static bool GetZuygNumber(int item)
        {
            return item % 2 == 0;
        }
        private static bool Get5icMecNumber(int item)
        {
            return item > 5 == true;
        }
        private static bool GetQarKentNumber(int item)
        {
            return (item * item) % 2 != 0;
        }

        private static List<int> FilterNumbers(List<int> sourceList,NumberFilterDelegate filterCondition)
        {
            List<int> result = new List<int>();
            foreach (int item in sourceList)
            {
                if (filterCondition(item))
                {
                    result.Add(item);
                }
            }

            return result;
        }

        private static void PrintList<T>(List<T> list)
        {
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
        }
    }

    delegate bool NumberFilterDelegate(int number);

}
