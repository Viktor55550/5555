﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqOrinak01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start");
            //Linq baysic syntax
            string[] words = { "hello","wonderfull","Linq","beautifull","world" };

            var shortwords = from word in words
                             where word.Length <= 5
                             select word;

            foreach (var word in shortwords)
            {
                Console.WriteLine(word);
            }


            int[] numbers = { 1, 55, 4567, 64, 88, 99, 57, 44, 777, 16, 9, 5, 8 };

            var newnums = from number in numbers
                          where number > 50
                          select number;

            foreach (var number in newnums)
            {
                Console.WriteLine(number);
            }

            //filtring operator
            List<Employee> emplist;
            emplist = new List<Employee>()
            {
                new Employee {Id = 10,Name="andi",Salary= 3000,Designation="teamlider"},
                new Employee {Id = 11,Name="andiik",Salary= 5000,Designation="pl"},
                new Employee {Id = 12,Name="andiiko",Salary= 8000,Designation="pm"},
                new Employee {Id = 13,Name="andulik",Salary= 3000,Designation="jahel"},
                new Employee {Id = 14,Name="andinav",Salary= 86900,Designation="shofer"},
                new Employee {Id = 15,Name="andilakrkaj",Salary= 89000,Designation="teamlider"},
                new Employee {Id = 16,Name="andiaatdh",Salary= 305200,Designation="teamlider"},
                new Employee {Id = 17,Name="andiadtn",Salary= 3045600,Designation="teamlider"},
                new Employee {Id = 18,Name="andiadtn",Salary= 3064500,Designation="teamlider"},
                new Employee {Id = 19,Name="andatji",Salary= 304600,Designation="teamlider"},
            };

            var emps = from emp in emplist
                       where emp.Salary >= 5000
                       select emp;

            foreach (var emp in emps)
            {
                Console.WriteLine(emp);
            }


            Console.WriteLine("End");

            Console.ReadKey();
        }
    }
}
