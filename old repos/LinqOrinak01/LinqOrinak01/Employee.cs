﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinqOrinak01
{
    class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Salary { get; set; }
        public string Designation { get; set; }
    }
}
