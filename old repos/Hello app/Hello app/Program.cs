﻿using System;

namespace HelloApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Write your name: ");
            string name = Console.ReadLine();
            Console.WriteLine($"Hi {name}");

            int a = 4;
            int b = 5;
            int c = 6;
            int d = 7;

            int f = a++ - --b + c++ - --d + d++ + --c + b++ - --a +
                    a++ - --d + ++b + --c + c++ + ++d - --b + ++a;

            Console.WriteLine(f);//Console.WriteLine("{0}",f); or Console.WriteLine($"{f}");

            Console.ReadKey();
        }
    }
}