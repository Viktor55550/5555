﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xndir_ref_ev_out
{
    class Program
    {
        static void Main(string[] args)
        {
            Time time1 = new Time();
            time1.Input();

            Time time2 = new Time();
            time2.Input();

            time1.Swap(ref time2);

            time1.Print();
            time2.Print();
        }
    }


    public struct Time
    {
        private int _hour;
        private int _minute;

        public int Hour
        {
            get
            {
                return this._hour;
            }
            set
            {
                if (value >= 0 && value <= 24)
                {
                    this._hour = value;
                }
                else
                {
                    this._hour = 0;
                    Console.WriteLine("Hour wrong value!");
                }
            }
        }
        public int Minute
        {
            get
            {
                return this._minute;
            }
            set
            {
                if (value >= 0 && value <= 24)
                {
                    this._minute = value;
                }
                else
                {
                    this._minute = 0;
                    Console.WriteLine("Minute wrong value!");
                }
            }
        }

        public Time(Time time)
        {
            this = time;
        }

        //public Time(int hour, int minute)
        //{
        //    this.Hour = hour;
        //    this.Minute = minute;
        //}

        public void Input()
        {
            bool isValidHourValue = false;

            while (true)
            {
                if (!isValidHourValue)
                {
                    Console.Write("Input hour: ");
                    //string inputed = Console.ReadLine();
                    //int value;
                    //bool isValid = int.TryParse(inputed, out value);

                    if (int.TryParse(Console.ReadLine(), out int hour) &&
                        hour >= 0 &&
                        hour <= 24)
                    {
                        this.Hour = hour;
                        isValidHourValue = true;
                    }
                    else
                    {
                        Console.WriteLine("Wrong hour value. Please input in 0 - 24 range!");
                        continue;
                    }
                }

                Console.Write("Input Minute: ");

                if (int.TryParse(Console.ReadLine(), out int minute) &&
                    minute >= 0 &&
                    minute <= 60)
                {
                    this.Minute = minute;
                    break;
                }
                else
                {
                    Console.WriteLine("Wrong minute value. Please input in 0 - 60 range!");
                    continue;
                }
            }
        }

        public void Print()
        {
            Console.WriteLine($"{this.Hour} : {this.Minute}");
        }

        public void Swap(ref Time time)
        {
            Time temp = time;
            time = this;
            this = temp;
        }
    }

}
