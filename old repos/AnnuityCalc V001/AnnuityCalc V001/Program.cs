﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnnuityCalc_V001
{
    class Program
    {
        static void Main(string[] args)
        {
            Start();
            Console.ReadKey();
        }

        public static void GetResul(Credit credit)
        {

            List<ResultListModel> resultLists = credit.GetMontsPayments();
            foreach (var list in resultLists)
            {
                Console.WriteLine($"{list.MonthNumber}    {list.SumCreditBalance}     {list.Principal}    {list.MonthPercent}   {list.MonthCredit}");
            }

        }
        public static void Start()
        {
            Console.WriteLine("Write type of your credirt:Annuity->1  Diferency->2");
            int type = int.Parse(Console.ReadLine());
            Console.WriteLine("Credit:");
            decimal Credit = decimal.Parse(Console.ReadLine());
            Console.WriteLine("Annual percentage:");
            double Percent = double.Parse(Console.ReadLine()) + 100;
            Console.WriteLine("Count of monts:");
            short Months = short.Parse(Console.ReadLine());

            switch (type)
            {
                case (int)CreditEnums.Annity:
                    Credit creditA = new AnnuityCredit(Credit, Percent, Months);
                    GetResul(creditA);
                    break;
                case (int)CreditEnums.Differency:
                    Credit creditD = new DiferencialCredit(Credit, Percent, Months);
                    GetResul(creditD);
                    break;
                default:
                    break;

            }
        }

        public static double Xirr(Credit credit, List<ResultListModel> list)
        {
            var xlApp = new Application();

            List<double> payment = new List<double>();
            payment.Add(-(double)(list.FirstOrDefault().SumCreditBalance + list.FirstOrDefault().Principal));

            foreach (var lis in list)
            {
                payment.Add((double)lis.MonthCredit);

            };

            var days = new double[credit.Months + 1];
            int j = 0;
            for (int i = 1; i <= credit.Months / 12; i++)
            {
                if (j == 0)
                {
                    days[j] = 1;
                    j++;
                }
                for (int k = 1; j <= i * 12; j++, k++)
                {
                    days[j] = days[j - 1] + DateTime.DaysInMonth(DateTime.Now.Year, k);
                }
            }
            double[] payments = payment.ToArray();
            double result = XIRR.Newtons_method(0.1, XIRR.total_f_xirr(payments, days), XIRR.total_df_xirr(payments, days));
            return result;

        }
    }
}
