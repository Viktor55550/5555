﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnnuityCalc_V001
{
    public class ResultListModel
    {
        public decimal SumCreditBalance { get; set; }
        public decimal Principal { get; set; }
        public decimal MonthPercent { get; set; }
        public decimal MonthCredit { get; set; }
        public int MonthNumber { get; set; }
    }
}
