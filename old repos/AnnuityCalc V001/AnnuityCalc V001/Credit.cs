﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnnuityCalc_V001
{
    public abstract class Credit
    {
        public decimal SumCredit { get; set; }
        public double Percent { get; set; }
        public short Months { get; set; }
        
        public Credit(decimal sumcredit, double percent, short months)
        {
            Months = months;
            Percent = percent;
            SumCredit = sumcredit;
        }


        public abstract List<ResultListModel> GetMontsPayments();
    }
}
