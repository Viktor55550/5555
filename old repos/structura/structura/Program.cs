﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace structura
{
    class Program
    {
        static void Main(string[] args)
        {

            // int c1 = 5;
            Coordinate coordinate1 = new Coordinate(5, 6);

            // c1 = int.Parse(Console.ReadLine());
            coordinate1.NermucumConsoleov();

            // int c2 = c1;
            Coordinate coordinate2 = new Coordinate(coordinate1);

            // int c2 += c1;
            coordinate2.Append(coordinate1);

            // int c3 = c2 + c1;
            Coordinate coordinate3 = coordinate2.Sum(coordinate1);

            // Console.WriteLine(c3);
            coordinate3.Print();

            Console.ReadKey();
        }
    }

    public struct Coordinate
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Coordinate(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public Coordinate(Coordinate coordinate)
        {
            this = coordinate;
        }

        public void NermucumConsoleov()
        {
            Console.Write("Input x: ");
            this.X = int.Parse(Console.ReadLine());
            Console.Write("Input y: ");
            this.Y = int.Parse(Console.ReadLine());
        }

        public void Print()
        {
            Console.WriteLine($"X: {this.X} - Y: {this.Y}");
            //Console.WriteLine("X: {0} - Y: {1}", this.X, this.Y);
            //Console.WriteLine("X: " + this.X + " - Y: "+ this.Y);
        }

        public Coordinate Sum(Coordinate coordinate)
        {
            int resultX = this.X + coordinate.X;
            int resultY = this.Y + coordinate.Y;

            Coordinate result = new Coordinate(resultX, resultY);

            return result;
        }

        public void Append(Coordinate coordinate)
        {
            this.X += coordinate.X;
            this.Y += coordinate.Y;
        }

        public Coordinate Divide(Coordinate coordinate)
        {
            int resultX = this.X / coordinate.X;
            int resultY = this.Y / coordinate.Y;

            Coordinate result = new Coordinate(resultX, resultY);

            return result;
        }
    }
}
