﻿using System;
using System.Collections.Generic;
using System.Text;

namespace V
{
    public class EmployeeTerritory
    {
        public int EmployeeId { get; set; }
        public int /*string*/ TerritoryId { get; set; }

        public Employee Employee { get; set; }
        public Territory Territory { get; set; }
    }
}
