﻿using System;
using System.Collections.Generic;
using System.Text;

namespace V
{
    public class Territory
    {
        public int TerritoryId { get; set; }
        public string TerritoryDescription { get; set; }
        public int RegionId { get; set; }

        public Region region { get; set; }
        public ICollection<EmployeeTerritory> EmployeeTerritory { get; set; }
    }
}
