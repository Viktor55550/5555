﻿using System;
using System.Collections.Generic;
using System.Text;

namespace V
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Title { get; set; }
        public string TitleOfCourtesy { get; set; }
        public DateTime? BirthDate
        {
            get { return BirthDate; }
            set
            {
                if (value < DateTime.Today)
                {
                    BirthDate = value;
                }
            }
        }
        public DateTime? HireDate { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string HomePhone { get; set; }
        public string Extention { get; set; }
        public Byte[] Photo { get; set; }
        public string Notes { get; set; }
        public int ReportsTo { get; set; }
        public string PhotoPath { get; set; }

        public Employee employee { get; set; }
        public ICollection<Employee> EmployeeCol { get; set; }
        public ICollection<EmployeeTerritory> EmployeeTerritory { get; set; }

        public ICollection<Order> Order { get; set; }
    }
}
