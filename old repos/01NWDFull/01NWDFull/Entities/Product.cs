﻿using System;
using System.Collections.Generic;
using System.Text;

namespace V
{
    public class Product
    {
        /*public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int SupplierId { get; set; }
        public int CategoryId { get; set; }
        public string QuantityPerUnit { get; set; }
        public string UnitPrice { get; set; }
        public string UnitsInStock { get; set; }
        public string UnitsOnOrder { get; set; }
        public string ReorderLevel { get; set; }
        public string Discountinued { get; set; }*/

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string QuantityPerUnit { get; set; }
        public decimal? UnitPrice
        {
            get { return UnitPrice; }
            set
            {
                if (value >= 0)
                {
                    UnitPrice = value;
                }
            }
        }
        public short? UnitsInStock
        {
            get { return UnitsInStock; }
            set
            {
                if (value >= 0)
                {
                    UnitsInStock = value;
                }
            }
        }
        public short? UnitsOnOrder
        {
            get { return UnitsOnOrder; }
            set
            {
                if (value >= 0)
                {
                    UnitsOnOrder = value;
                }
            }
        }
        public short? ReorderLevel
        {
            get { return ReorderLevel; }
            set
            {
                if (value >= 0)
                {
                    ReorderLevel = value;
                }
            }
        }
        public bool Discontinued { get; set; }
        public int SupplierId { get; set; }
        public int CategoryId { get; set; }
        public Supplier Supplier { get; set; }
        public Category Category { get; set; }
        public ICollection<OrderDetail> OrderDetail { get; set; }

    }
}
