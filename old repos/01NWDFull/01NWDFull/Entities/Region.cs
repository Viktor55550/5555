﻿using System;
using System.Collections.Generic;
using System.Text;

namespace V
{
    public class Region
    {
        public int RegionId { get; set; }
        public string RegionDescription { get; set; }

        public ICollection<Territory> Territory { get; set; }
        public Region(string RegionDescription)
        {
            this.RegionDescription = RegionDescription;
        }
    }
}
