﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace V
{
    public class Category
    {
        //[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        /*public int CategoryId { get; set; }

        public string CategoryName { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }*/


        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public Byte[] Picture { get; set; }
        public ICollection<Product> Product { get; set; }
    }
}
