﻿using System;
using System.Collections.Generic;
using System.Text;

namespace V
{
    public class Order
    {
        public int OrderId { get; set; }
        public int? CustomerId { get; set; }
        public int? EmployeeId  { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
        public int? ShipVia  { get; set; }
        public decimal? Freight { get; set; }
        public string ShipName { get; set; }
        public string ShipAddress { get; set; }
        public string ShipCity { get; set; }
        public string ShipRegion { get; set; }
        public string ShipPostalCode { get; set; }
        public string ShipCountry { get; set; }


        public Shipper shiper { get; set; }

        public Customer customer { get; set; }


        public Employee employee { get; set; }

        public ICollection<OrderDetail> OrderDetail { get; set; }
    }
}
