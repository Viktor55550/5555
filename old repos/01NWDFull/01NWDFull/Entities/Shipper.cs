﻿using System;
using System.Collections.Generic;
using System.Text;

namespace V
{
    public class Shipper
    {
        public int ShipperId { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }

        public ICollection<Order> Order { get; set; }
    }
}
