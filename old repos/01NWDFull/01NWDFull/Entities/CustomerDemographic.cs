﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace V
{
    public class CustomerDemographic
    {
        public string CustomersDemographicId { get; set; }
        public string CustomerDesc { get; set; }

        public ICollection<CustomerCustomerDemo> customerCustomerDemo { get; set; }
    }
}
