﻿using System;
using System.Collections.Generic;
using System.Text;

namespace V
{
    public class OrderDetail
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public decimal UnitPrice
        {
            get { return UnitPrice; }

            set
            {
                if (value >= 0)
                {
                    UnitPrice = value;
                }
            }
        }
        public short Quantity
        {
            get { return Quantity; }

            set
            {
                if (Quantity > 0)
                {
                    Quantity = value;
                }
            }
        }
        public double Discount
        {
            get { return Discount; }

            set
            {
                if (value >= 0 && value <= 1)
                {
                    Discount = value;
                }
            }
        }

        public Order Order { get; set; }

        public Product Product { get; set; }
    }
}
