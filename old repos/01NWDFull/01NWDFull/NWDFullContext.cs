﻿//using _01NWDFull.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
//using V.Entities;
using V.EntityConfigurations;

namespace V
{
    // SQL SERVER C#-um
    public class NWDFullContext : DbContext
    {
        
       // Users Tabel c#-um
       public DbSet<Category> Categories { get; set; }
       public DbSet<CustomerDemographic> CustomerDemographics { get; set; }
       //public DbSet<Product> Product { get; set; }
       //public DbSet<OrderProduct> OrderProducts { get; set; }
       
       public NWDFullContext(DbContextOptions<NWDFullContext> options)
           : base(options)
       {
           // Jnjel Migration-i hamar
           //Database.EnsureCreated();
       }

        public NWDFullContext()
        {
        }

        // Bazayin mer kargavorumnern enq talis
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
       {
           // talis enq vor sql server e ev inch hasceyov kpni nran
           //optionsBuilder.UseSqlServer();
       }
       
       protected override void OnModelCreating(ModelBuilder modelBuilder)
       {
           base.OnModelCreating(modelBuilder);
           modelBuilder.ApplyConfiguration(new CategoryConfiguration());
           modelBuilder.ApplyConfiguration(new CustomerDemographicConficuration());
           modelBuilder.ApplyConfiguration(new CustomerConfiguration());
           modelBuilder.ApplyConfiguration(new RegionConfiguration());
           modelBuilder.ApplyConfiguration(new ShipperConfiguration());
           modelBuilder.ApplyConfiguration(new SupplierConfiguration());
           modelBuilder.ApplyConfiguration(new CustomerCustomerDemoConfiguration());
           modelBuilder.ApplyConfiguration(new ProductConfiguration());
           modelBuilder.ApplyConfiguration(new TerritoryConfiguration());
           modelBuilder.ApplyConfiguration(new EmployeeConfiguration());
           modelBuilder.ApplyConfiguration(new EmployeeTerritoryConfiguration());
           modelBuilder.ApplyConfiguration(new OrderConfiguration());
           modelBuilder.ApplyConfiguration(new OrderDetailConfiguration());
       }
    }
}
