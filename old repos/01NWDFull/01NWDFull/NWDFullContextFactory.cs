﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _01NWDFull
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Design;
    using Microsoft.EntityFrameworkCore.Infrastructure;
    using Microsoft.Extensions.Configuration;
    using System.IO;
    using V;

    namespace MyProject
    {
        public class NWDFullContextFactory : IDesignTimeDbContextFactory<NWDFullContext>
        {
            public NWDFullContext CreateDbContext(string[] args)
            {

                ConfigurationBuilder builder = new ConfigurationBuilder();
                // установка пути к текущему каталогу
                builder.SetBasePath(Directory.GetCurrentDirectory());
                // получаем конфигурацию из файла appsettings.json
                builder.AddJsonFile("appsettings.json");
                // создаем конфигурацию
                IConfiguration config = builder.Build();
                // получаем строку подключения
                string connectionString = config.GetConnectionString("NWDFullConnection");

                DbContextOptionsBuilder<NWDFullContext> optionsBuilder = new DbContextOptionsBuilder<NWDFullContext>();
                DbContextOptions<NWDFullContext> options = optionsBuilder
                    .UseSqlServer(connectionString).Options;

                return new NWDFullContext(options);
            }
        }
    }
}
