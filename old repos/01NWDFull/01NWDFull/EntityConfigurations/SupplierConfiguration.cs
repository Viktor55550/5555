﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace V.EntityConfigurations
{
    class SupplierConfiguration : IEntityTypeConfiguration<Supplier>
    {
        public void Configure(EntityTypeBuilder<Supplier> builder)
        {
            builder.HasKey(a => a.SupplierId);
            builder.Property(a => a.SupplierId).ValueGeneratedOnAdd();
            builder.Property(a => a.CompanyName).HasColumnType("nvarchar(40)")
                                      .IsRequired();
            builder.Property(a => a.ContactName).HasColumnType("nvarchar(30)");
            builder.Property(a => a.ContactTitle).HasColumnType("nvarchar(30)");
            builder.Property(a => a.Address).HasColumnType("nvarchar(60)");
            builder.Property(a => a.City).HasColumnType("nvarchar(15)");
            builder.Property(a => a.Region).HasColumnType("nvarchar(15)");
            builder.Property(a => a.PostalCode).HasColumnType("nvarchar(10)");
            builder.Property(a => a.Country).HasColumnType("nvarchar(15)");
            builder.Property(a => a.Fax).HasColumnType("nvarchar(24)");
            builder.Property(a => a.Phone).HasColumnType("nvarchar(24)");
            builder.HasIndex(a => a.CompanyName);
            builder.HasIndex(a => a.PostalCode);
        }
    }
}
