﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace V.EntityConfigurations
{
    public class CustomerDemographicConficuration : IEntityTypeConfiguration<CustomerDemographic>
    {
        public void Configure(EntityTypeBuilder<CustomerDemographic> builder)
        {
            builder.HasKey(a => a.CustomersDemographicId);
            builder.Property(a => a.CustomersDemographicId).ValueGeneratedOnAdd();
        }
    }
}
