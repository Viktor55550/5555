﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace V.EntityConfigurations
{
    class ShipperConfiguration : IEntityTypeConfiguration<Shipper>
    {
        public void Configure(EntityTypeBuilder<Shipper> builder)
        {
            builder.HasKey(a => a.ShipperId);
            builder.Property(a => a.ShipperId).ValueGeneratedOnAdd();
            builder.Property(a => a.CompanyName).HasColumnType("nvarchar(40)")
                                                .IsRequired();
            builder.Property(a => a.Phone).HasColumnType("nvarchar(24)");
        }
    }
}
