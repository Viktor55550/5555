﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace V.EntityConfigurations
{
    class EmployeeTerritoryConfiguration : IEntityTypeConfiguration<EmployeeTerritory>
    {
        public void Configure(EntityTypeBuilder<EmployeeTerritory> builder)
        {
            builder.HasKey(a => new { a.EmployeeId, a.TerritoryId });
            builder.HasOne(a => a.Employee)
                    .WithMany(a => a.EmployeeTerritory)
                    .HasForeignKey(a => a.EmployeeId)
                    .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(a => a.Territory)
                    .WithMany(a => a.EmployeeTerritory)
                    .HasForeignKey(a => a.TerritoryId)
                    .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
