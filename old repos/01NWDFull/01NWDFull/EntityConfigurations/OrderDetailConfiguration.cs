﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace V.EntityConfigurations
{
    class OrderDetailConfiguration : IEntityTypeConfiguration<OrderDetail>
    {
        public void Configure(EntityTypeBuilder<OrderDetail> builder)
        {
            builder.Property(a => a.UnitPrice).HasDefaultValue(0);
            builder.Property(a => a.Quantity).HasDefaultValue(1);
            builder.Property(a => a.Discount).HasDefaultValue(0);

            builder.HasKey(a => new { a.ProductId, a.OrderId });

            builder.HasOne(a => a.Order)
                    .WithMany(a => a.OrderDetail)
                    .HasForeignKey(a => a.OrderId)
                    .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(a => a.Product)
                   .WithMany(a => a.OrderDetail)
                   .HasForeignKey(a => a.ProductId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasIndex(a => a.ProductId);
            builder.HasIndex(a => a.OrderId);
        }
    }
}
