﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace V.EntityConfigurations
{
    class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasKey(a => a.ProductId);
            builder.Property(a => a.ProductId).ValueGeneratedOnAdd();

            builder.Property(a => a.ProductName).HasColumnType("nvarchar(40)");
            builder.Property(a => a.QuantityPerUnit).HasColumnType("nvarchar(20)")
                                                    .IsRequired();
            builder.Property(a => a.UnitPrice).HasDefaultValue(0m);
            builder.Property(a => a.UnitsInStock).HasDefaultValue(0);
            builder.Property(a => a.UnitsOnOrder).HasDefaultValue(0);
            builder.Property(a => a.ReorderLevel).HasDefaultValue(0);

            builder.HasOne(a => a.Supplier)
                   .WithMany(a => a.Product)
                   .HasForeignKey(a => a.SupplierId)
                   .OnDelete(DeleteBehavior.Restrict);


            builder.HasOne(a => a.Category)
                   .WithMany(a => a.Product)
                   .HasForeignKey(a => a.CategoryId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasIndex(a => a.CategoryId);
            builder.HasIndex(a => a.SupplierId);
            builder.HasIndex(a => a.ProductName);
        }
    }
}
