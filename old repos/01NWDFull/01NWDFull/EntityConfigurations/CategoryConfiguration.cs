﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace V.EntityConfigurations
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.HasKey(a => a.CategoryId);
            builder.Property(a => a.CategoryId).ValueGeneratedOnAdd();
            builder.Property(a => a.CategoryName).HasColumnType("nvarchar(15)")
                                                 .IsRequired();
            builder.HasIndex(a => a.CategoryName);


        }
    }
}
