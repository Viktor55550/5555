﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace V.EntityConfigurations
{
    class RegionConfiguration : IEntityTypeConfiguration<Region>
    {
        public void Configure(EntityTypeBuilder<Region> builder)
        {
            builder.HasKey(a => a.RegionId);
            builder.Property(a => a.RegionId).ValueGeneratedOnAdd();
            builder.Property(a => a.RegionDescription).HasColumnType("nvarchar(50)")
                                                      .IsRequired();
        }
    }
}
