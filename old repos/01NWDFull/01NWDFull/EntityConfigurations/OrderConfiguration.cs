﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace V.EntityConfigurations
{
    class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(a => a.OrderId);
            builder.Property(a => a.Freight).HasDefaultValue(0m);
            builder.Property(a => a.ShipName).HasColumnType("nvarchar(40)");
            builder.Property(a => a.ShipAddress).HasColumnType("nvarchar(60)");
            builder.Property(a => a.ShipCity).HasColumnType("nvarchar(15)");
            builder.Property(a => a.ShipRegion).HasColumnType("nvarchar(15)");
            builder.Property(a => a.ShipPostalCode).HasColumnType("nvarchar(10)");
            builder.Property(a => a.ShipCountry).HasColumnType("nvarchar(15)");

            builder.HasOne(a => a.customer)
                   .WithMany(a => a.Order)
                   .HasForeignKey(a => a.CustomerId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(a => a.employee)
                   .WithMany(a => a.Order)
                   .HasForeignKey(a => a.EmployeeId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(a => a.shiper)
                   .WithMany(a => a.Order)
                   .HasForeignKey(a => a.ShipVia)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasIndex(a => a.CustomerId);
            builder.HasIndex(a => a.EmployeeId);
            builder.HasIndex(a => a.OrderDate);
            builder.HasIndex(a => a.ShippedDate);
            builder.HasIndex(a => a.ShipVia);
            builder.HasIndex(a => a.ShipPostalCode);
        }
    }
}
