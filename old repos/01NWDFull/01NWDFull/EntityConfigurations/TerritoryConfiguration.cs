﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace V.EntityConfigurations
{
    class TerritoryConfiguration : IEntityTypeConfiguration<Territory>
    {
        public void Configure(EntityTypeBuilder<Territory> builder)
        {
            builder.HasKey(a => a.TerritoryId);
            builder.Property(a => a.TerritoryId).ValueGeneratedOnAdd();
            builder.Property(a => a.TerritoryDescription).HasColumnType("nchar(50)");

            builder.HasOne(a => a.region)
                    .WithMany(a => a.Territory)
                    .HasForeignKey(a => a.RegionId)
                    .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
