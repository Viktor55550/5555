﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace V.EntityConfigurations
{
    class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.HasKey(a => a.EmployeeId);
            builder.Property(a => a.EmployeeId).ValueGeneratedOnAdd();
            builder.Property(a => a.LastName).HasColumnType("nvarchar(20)").IsRequired();
            builder.Property(a => a.FirstName).HasColumnType("nvarchar(10)").IsRequired();
            builder.Property(a => a.Title).HasColumnType("nvarchar(30)");
            builder.Property(a => a.TitleOfCourtesy).HasColumnType("nvarchar(25)");
            builder.Property(a => a.Address).HasColumnType("nvarchar(60)");
            builder.Property(a => a.City).HasColumnType("nvarchar(15)");
            builder.Property(a => a.Region).HasColumnType("nvarchar(15)");
            builder.Property(a => a.PostalCode).HasColumnType("nvarchar(10)");
            builder.Property(a => a.Country).HasColumnType("nvarchar(15)");
            builder.Property(a => a.HomePhone).HasColumnType("nvarchar(24)");
            builder.Property(a => a.Extention).HasColumnType("nvarchar(4)");
            builder.Property(a => a.Photo).HasColumnType("varbinary(16)");
            builder.Property(a => a.Notes).HasColumnType("nvarchar(8)");
            builder.Property(a => a.PhotoPath).HasColumnType("nvarchar(225)");
            builder.HasIndex(a => a.LastName);
            builder.HasIndex(a => a.PostalCode);

            builder.HasOne(a => a.employee)
                   .WithMany(a => a.EmployeeCol)
                   .HasForeignKey(a => a.ReportsTo);
        }
    }
}
