﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace V.EntityConfigurations
{
    class CustomerCustomerDemoConfiguration : IEntityTypeConfiguration<CustomerCustomerDemo>
    {
        public void Configure(EntityTypeBuilder<CustomerCustomerDemo> builder)
        {
            builder.HasKey(a => new { a.CustomerId, a.CustomerTypeId });

            builder.HasOne(a => a.customer)
                   .WithMany(a => a.customerCustomerDemo)
                   .HasForeignKey(a => a.CustomerId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(a => a.customerDemographic)
                   .WithMany(a => a.customerCustomerDemo)
                   .HasForeignKey(a => a.CustomerTypeId)
                   .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
