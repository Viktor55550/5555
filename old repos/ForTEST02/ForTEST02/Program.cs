﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForTEST02
{
    //vid-41
    /*struct MS
    {
        public int val { get; set; }
    }
    class MC
    {
        public int va { get; set; }
    }*/


    //vid-40
    /*class MC<T1, T2>
    {
        public T1 MyProp { get; set; }
        public void M(T1 p1,T2 p2)
        {

        }
    }*/

    //vid-39
    /*partial class MC
    {
        public void M1()
        {
            Console.WriteLine("M1");
        }
    }
    partial class MC
    {
        public void M2()
        {
            Console.WriteLine("M2");
        }
    }*/

    //vid-38 
    //public sealed class a { int x = default(int); }

    //vid-37
    /*interface Iinterface1
    {
        void Method1();
        void Method2();
    }
    interface Iinterface2
    {
        void Method2();
    }
    class MyClass1
    {
        public void M1()
        {
            Console.WriteLine("M1");
        }
    }
    class MyClass2 : MyClass1,Iinterface1, Iinterface2 //implicit implementation
        //Method2-@ arden implement egac e
    {
        public void Method1()
        {
            Console.WriteLine("Method 1");
        }
        public void Method2()
        {
            Console.WriteLine("Method 2");
        }
    }
    class MyClass3 : MyClass1, Iinterface1, Iinterface2//explicit implementation
        //ays depqum nshum enq te vor interface-@ implement ani
    {
        public void Method1()
        {
            Console.WriteLine("Method 1 M3");
        }
        void Iinterface1.Method2()
        {
            Console.WriteLine("Method 2 in1");
        }
        void Iinterface2.Method2()
        {
            Console.WriteLine("Method 2 in2");
        }
    }*/


    //vid-36
    /*abstract class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public abstract void Work();//aranc body
    }
    class Student : Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public override void Work()
        {
            Console.WriteLine("It studies...");
        }
    }
    class Employes : Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public /*new*//* override void Work()
        {
            Console.WriteLine("It earns...");
        }
    }
    class Manager : Employes
    {
        public sealed override void Work()
        {
            Console.WriteLine("It manages a team...");
        }
    }
    class BranchManager : Manager
    {
    }*/

    //vid-35=(vid-28)
    /*class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public virtual void Work()
        {
            Console.WriteLine("It works...");
        }
    }
    class Student : Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public override void Work()
        {
            Console.WriteLine("It studies...");
        }
    }
    class Employes : Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public /*new*/ /*override void Work()
        {
            Console.WriteLine("It earns...");
        }
    }
    class Manager : Employes
    {
        public sealed override void Work()
        {
            Console.WriteLine("It manages a team...");
        }
    }
    class BranchManager : Manager
    {
    }*/

    //vid-34
    /*class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public void Work()
        {
            Console.WriteLine("It works");
        }
    }
    class Employee : Person
    {
        public double Salary { get; set; }
        public string Designation { get; set; }
        public new void Work()
        {
            Console.WriteLine("It earns");
        }
    }*/


    //vid-33
    /*class MyClass1
    {
        protected int x;//erevume miayn ir jarangum
        internal int y;

        public MyClass1()
        {
            Console.WriteLine("Def constructor of base");
        }
        public MyClass1(int a)
        {
            Console.WriteLine("param. constructor of base");
        }
        static MyClass1()
        {
            Console.WriteLine("papa static constructor");
        }
        public void Method1()
        {
            Console.WriteLine("Method1 from base");
        }
    }
    class MyClass2 : MyClass1
    {
        public MyClass2()
        {
            Console.WriteLine("Def constructor of derived");
        }
        public MyClass2(int b) : base(b)
        {
            Console.WriteLine("param. constructor of derived");
        }
        static MyClass2()
        {
            Console.WriteLine("child static constructor");
        }
        public void Method2()
        {
            Console.WriteLine("Method2 from derived");
        }
    }*/

    //vid-32
    /*class Employee
    {
        public int Id { get; set;}
        public string EmpName { get; set; }
        public double Salary { get; set; }
    }
    class Department
    {
        public int DepId { get; set; }
        public string DepName { get; set; }
        Employee[] EmpList;

        public Department()
        {
            DepId = 10;
            DepName = "Sales";
            EmpList = new Employee[3]
            {
                new Employee { Id = 101, EmpName = "Alex", Salary = 50000},
                new Employee { Id = 102, EmpName = "Brad", Salary = 45000},
                new Employee { Id = 103, EmpName = "Chris", Salary = 40000},
            };

        }
        public Employee GetEmployee(int id)
        {
            foreach (Employee emp in EmpList)
            {
                if (id == emp.Id)
                    return emp;
            }
            return null;
        }
        public Employee GetEmployee(string name)
        {
            foreach (Employee emp in EmpList)
            {
                if (name == emp.EmpName)
                    return emp;
            }
            return null;
        }
        public Employee this[int id]
        {
            get
            {
                foreach (Employee emp in EmpList)
                {
                    if (id == emp.Id)
                        return emp;
                }
                return null;
            }
        }
        public Employee this[string name]
        {
            get
            {
                foreach (Employee emp in EmpList)
                {
                    if (name == emp.EmpName)
                        return emp;
                }
                return null;
            }
        }
    }*/

    //vid-31
    /*class Accounts
    {
        float init_amount;
        static float interest;
        public string Name { get; set; }

        public float InitialAmount
        {
            set
            {
                if (value < 1000)
                {
                    Console.WriteLine("Initial amount cannot " +
                        "be less than 1000");
                    return;
                }
                init_amount = value;
            }
            get
            {
                return init_amount;
            }
        }
        public static float InterestRate
        {
            private set { interest = value; }
            get { return interest; }
        }
        public Accounts()
        {
            this.InitialAmount = 10000;
        }
        public Accounts(float amt)
        {
            this.InitialAmount = amt;
        }
        static Accounts()
        {
            Accounts.InterestRate = 9.5f;
        }
    }*/

    //vid 30
    /*public class MyClass1//internal
    {
        public int a;
        internal int b;
    }*/

    //vid-29
    /*class Length
    {
        int feet, inch;
        public Length()
        {
            this.feet = 0;
            this.inch = 0;
        }
        public Length(int feet, int inch)
        {
            this.feet = feet;
            this.inch = inch;
        }
        public static Length operator +(Length l1, Length l2)
        {
            Length l3 = new Length();
            l3.feet = l1.feet + l2.feet;
            l3.inch = l1.inch + l2.inch;
            if (l3.inch >= 12)
            {
                l3.feet++;
                l3.inch -= 12;
            }
            return l3;
        }
        public static bool operator >(Length l1, Length l2)
        {
            if (l1.feet > l2.feet)
                return true;
            else
            if (l1.feet == l2.feet)
            {
                if (l1.inch > l2.inch)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        public static bool operator <(Length l1, Length l2)
        {
            if (l1.feet < l2.feet)
                return true;
            else
            if (l1.feet == l2.feet)
            {
                if (l1.inch < l2.inch)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        public string GetLength()
        {
            return string.Format($"Length : {feet}' {inch}\"");
        }
    }*/

    //vid-28
    /*class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public virtual void Work()
        {
            Console.WriteLine("It works...");
        }
    }
    class Student : Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public override void Work()
        {
            Console.WriteLine("It studies...");
        }
    }
    class Employes : Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public /*new*//* override void Work()
        {
            Console.WriteLine("It earns...");
        }
    }
    class Manager : Employes
    {
        public sealed override void Work()
        {
            Console.WriteLine("It manages a team...");
        }
    }
    class BranchManager : Manager
    {
    }*/

    //vid-27
    /*class Person
    {
       internal int age;
       internal string name;
       internal char gender;
    }*/

    //vid-26
    /*class Test
    {
        public int marks;
        public static int maxmarks;
        public Test()//default constructor
        {
            this.marks = 30;
        }
        public Test(int marks)//parametravorvac constructor
        {
            this.marks = marks;
        }
        public Test(Test t)//copy constructor
        {
            this.marks = t.marks;
        }
        static Test()//static constructor executed before Main
        {
            Test.maxmarks = 50;
        }
        public void CalculatePercent()
        {
            int percent = this.marks * 100 / Test.maxmarks;
            Console.WriteLine(percent);
        }
    }*/

    //vid-24 ev 25
    /*class Test
    {
        public int marks;//instance
        public static int maxmarks = 50;//class //25-i hamar
        public void CalculatePercent()
        {
            int percent = this.marks * 100 / Test.maxmarks;//25-i hamar
            //int percent = this.marks * 100 / 50;//24-i hamar
            Console.WriteLine(percent);
        }
    }*/

    class Program
    {
        static void Main(string[] args)
        {
            //vid-41 struct
            /*MS ms1 = new MS();
            ms1.val = 10;

            MS ms2 = ms1;

            Console.WriteLine(ms2.val);//->10

            ms1.val = 20;
            Console.WriteLine(ms2.val);//->10 

            MC mc1 = new MC();
            mc1.va = 10;

            MC mc2 = mc1;

            Console.WriteLine(mc2.va);//->10

            mc1.va = 20;
            Console.WriteLine(mc2.va);//->20*/

            //vid-40 generic class
            /*MC<int, string> obj1 = new MC<int, string>();
            obj1.MyProp = 10;
            obj1.M(10, "tut");*/


            //vid-39 partial class
            /*MC m = new MC();
            m.M1();
            m.M2();*/

            //vid-38 sealed class


            //vid-37 interface
            /*MyClass2 m2 = new MyClass2();
            //m2.-ov erevume MyClass2-um jaranvacner@ ev implement egacner@
            Iinterface1 i1 = new MyClass3();
            i1.Method2();
            Iinterface2 i2 = new MyClass3();
            i2.Method2();*/

            //vid-36 abstruct class
            /*Person p1 = new Student();
            p1.Work();
            p1 = new Employes();
            p1.Work();
            p1 = new BranchManager();
            p1.Work();*/


            //vid-35 method overriding==vid 28
            /*//Person p1 = new Employes();
            //p1.Work();//minches student sarqel@
            Person p1 = new Student();
            p1.Work();
            p1 = new Employes();
            p1.Work();
            p1 = new BranchManager();
            p1.Work();*/

            //vid-34 data hiding
            /*Employee emp1 = new Employee();
            emp1.Work();*/

            //vid-33 inheritance
            /*//MyClass1 obj1 = new MyClass1();
            MyClass2 obj2 = new MyClass2(10);//ete derived-um :base chgrenq=>apa ayd depqum kanchum e
                                             //MyClass1-i default constructor@ ev heto derived-i param. constructor@
            //skzbum MyClass1-i static constructorn e kanchvum ev heto MyClass2-in@
            obj2.Method1();
            obj2.Method2();*/

            //vid-32 indexer
            /*Department dep = new Department();
            Console.WriteLine(dep[101].EmpName);//Alex
            Console.WriteLine(dep["Brad"].Id);//102
            Console.WriteLine(dep[102].Salary);//45000
            //Console.WriteLine(dep[45000].EmpName);//->exeption*/

            //vid-31 properties
            /*Accounts ac1 = new Accounts();//new Accounts(5000);
            //ac1.InitialAmount = 500;
            //Accounts.InterestRate = 10;->error=>set is private
            Console.WriteLine(ac1.InitialAmount);
            Console.WriteLine(Accounts.InterestRate);
            ac1.Name = "Andi"*/

            //vid-30 access specifiers
            /*MyClass1 m = new MyClass1();
            m.a = 100;*/

            //vid-29 Operator Overloading
            /*Length len1 = new Length(2, 8);
            Length len2 = new Length(4, 5);
            Length len3 = len1 + len2;
            if (len1 > len2)
                Console.WriteLine("Len1 is greater");
            else
                Console.WriteLine("Len1 is not greater");
            Console.WriteLine(len1.GetLength());
            Console.WriteLine(len2.GetLength());
            Console.WriteLine($"Total : {len3.GetLength()}");*/

            //vid-28 method overloding,overriding
            /*Person p1 = new Employes();
            p1.Work();*///minches student sarqel@
                        /*Person p1 = new Student();
                        p1.Work();
                        p1 = new Employes();
                        p1.Work();
                        p1 = new BranchManager();
                        p1.Work();*/

            //vid-27 object initialiazer
            /*Person p1 = new Person { name = "Andi", gender = 'M', age = 28 };*/

            //vid-26 constructors
            /*Test t1 = new Test();
            t1.CalculatePercent();
            Test t2 = new Test(35);
            t2.CalculatePercent();
            Test t3 = new Test(t2);
            t3.CalculatePercent();
            Test t4 = new Test(45);
            Test t5 = t4;
            t4.marks = 48;
            t5.CalculatePercent();//kvercni 48-i arjeq@*/

            //vid-24 implementing class ev 25 class members
            /*//Test.maxmarks = 50;//25-i hamar
            Test t1 = new Test();
            t1.marks = 43;
            t1.CalculatePercent();

            Test t2 = new Test();
            t2.marks = 38;
            t2.CalculatePercent();*/

            //vid-23 OOP

            //vid-22 Preprocessor Directive 
            //it is conditional compilation
            /*#if (PI)//using-neric verev grel <<#define PI>>
                Console.WriteLine("PI is defined");
            #else
                        Console.WriteLine("PI is not defined");
                        abba-error chi talu ete PI-n ka
            #endif*/


            //vid-21 String Builder
            /*string str = "Tut";
            str = str + "Point";
            //Strings are immutable, which means we are creating new 
            //memory everytime instead of working on 
            //existing memory.
            Console.WriteLine(str);

            string strBuilder = "Tut";
            StringBuilder sb = new StringBuilder(strBuilder);
            sb.Append("Point");
            //StringBuilder is a mutable type, that means we are 
            //using the same memory location and keep on 
            //appending/modifying the stuff to one instance.
            Console.WriteLine(sb);*/

            //vid-20 string
            /*string str = "Tut Point Ind Private Limited";

            Console.WriteLine(str.Length);//property e->37

            string lower = str.ToLower();
            string upper = str.ToUpper();
            Console.WriteLine(str.Equals(upper));//false
            //upper = "Tp";//nerqevum ktpi false ays depqum
            Console.WriteLine(str.Equals(upper, StringComparison.CurrentCultureIgnoreCase));//true
            Console.WriteLine(str.IndexOf("in", StringComparison.CurrentCultureIgnoreCase));//ktpi arajin in-i dirq@->12
            Console.WriteLine(str.IndexOf("in", 13, StringComparison.CurrentCultureIgnoreCase));//13-rd indexsic heto ktpi in-i dirq@->16
            Console.WriteLine(lower);
            Console.WriteLine(upper);

            Console.WriteLine(str.StartsWith("Tu"));//true
            Console.WriteLine(str.EndsWith("ed"));//true

            Console.WriteLine(str.Contains("Ind"));//true*/

            /*string fname, lname;
            fname = "Andi";
            lname = "Sharma";

            //string fullname = fname + " " + lname;
            string fullname = string.Concat(fname, lname);
            Console.WriteLine($"Full name: {fullname}");


            //by using string constructor
            char[] letters = new char[] { 'H', 'e', 'l', 'l', 'o' };
            //char[] letters = { 'H', 'e', 'l', 'l', 'o' };
            string greetings = new string(letters);//char-@ sarqec string
            Console.WriteLine($"Greetings: {greetings}");
            char[] ch = greetings.ToCharArray();//string@ sarqecinq char

            //methods returning string
            //string[] sarray = new string[] { "Hello", "From", "Tut", "Point" };
            string[] sarray = { "Hello", "From", "Tut", "Point" };
            string message = string.Join("-", sarray);
            Console.WriteLine($"Message: {message}");

            //formating method to convert a value
            DateTime waiting = new DateTime(2019, 06, 23, 22, 38, 1);
            string chat = String.Format($"Message sent at {waiting:t} on {waiting:D}");
            Console.WriteLine($"Message: {chat}");*/

            Console.ReadKey();
        }
    }
}
