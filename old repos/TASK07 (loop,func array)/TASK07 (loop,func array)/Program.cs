﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK07__loop_func_array_
{
    class Program
    {

        static void Main(string[] args)
        {
            // nermucel zangvac@
            int[][] arr = ZangvaciNermucum([4][5]);

            // tpel nermucvac zangvac@
            TpelErkchapZangvac(arr);

            // zuyg tarreri stacum
            int[] miachap = ZuygTarreriStacum(arr);

            // tpel stacvac zangvac@
            TpelMiachapZangvac(miachap);
        }

        static int[][] ZangvaciNermucum(int[][] height, int[][] width)
        {
            int[][] arr = new int[height][width];
            Random rand = new Random();

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    arr[i][j] = rand.Next(1, 9);
                }
            }

            return arr;
        }

        static void TpelErkchapZangvac(int[][] arr)
        {
            foreach (int item in arr)
            {
                Console.Write(item);
            }
        }

        static int[][] ZuygTarreriStacum(int[][] arr)
        {
            int zuygTarreriQanak = default(int);

            foreach (int item in arr)
            {
                if (item % 2 == 0)
                {
                    zuygTarreriQanak++;
                }
            }

            int[] miachap = new int[zuygTarreriQanak];

            int index = default(int);
            foreach (int item in arr)
            {
                if (item % 2 == 0)
                {
                    miachap[index++] = item;
                }
            }

            return miachap;
        }

        static void TpelMiachapZangvac(int[][] miachap)
        {
            foreach (int item in miachap)
            {
                Console.Write(item);
            }
        }
    }
    
}