﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jarangakanutyun_orinak
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    /*public class Shape3D : Shape
    {
        public Shape3D() : base()
        {
            Console.WriteLine("Shape3d");
        }
        public Shape3D(int height, int width, int length)
            : base(height, width)
        {
            Console.WriteLine("Shape 3d with args");
            Length = length;
        }


        //public // true
        //protected // true
        //protected internal // true
        //private protected // true for same assembly
        //private // false

        public int Length { get; set; }

        public int Space()
        {
            return Area() * Length;
        }
    }*/


    public class Shape
    {
        public Shape()
        {
            Console.WriteLine("Shape");
        }
        public Shape(int height, int width)
        {
            Console.WriteLine("Shape with args");

            Height = height;
            Width = width;
        }
        protected int poxos { get; set; }

        public int Height { get; set; }
        public int Width { get; set; }

        public int Area()
        {
            return Height * Width;
        }
    }



    public class Shape3D : Shape
    {
        public Shape3D(int height, int width, int length)
        {
            Height = height;
            Width = width;
            Length = length;
        }

        public int Height { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }

        public int Space()
        {
            return Height * Width * Length;
        }
    }
}
