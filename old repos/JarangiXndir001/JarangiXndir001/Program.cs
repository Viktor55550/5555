﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JarangiXndir001
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public class Transport
    {
        int humanCount { get; set; }

        public void Go()
        {

        }

        public void Stop()
        {

        }
    }

    public class AirTransport : Transport
    {
        int FlyTime { get; set; }

        /*public void Go()
        {

        }

        public void Stop()
        {

        }*/

        public void SOS_Signal()
        {

        }

        public void Go()
        {
            throw new System.NotImplementedException();
        }

        public void Stop()
        {
            throw new System.NotImplementedException();
        }
    }

    public class Airplane : AirTransport
    {
        public void Go()
        {
            throw new System.NotImplementedException();
        }

        public void SOS_Signal()
        {
            throw new System.NotImplementedException();
        }

        public void Stop()
        {
            throw new System.NotImplementedException();
        }
    }


    public class Helicopter : AirTransport
    {
        public void SOS_Signal()
        {
            throw new System.NotImplementedException();
        }

        public void Go()
        {
            throw new System.NotImplementedException();
        }

        public void Stop()
        {
            throw new System.NotImplementedException();
        }
    }

    public class WatherTransport : Transport
    {
        char Material { get; set; }

        public void Swimming_Speed()
        {

        }

        public void Go()
        {
            throw new System.NotImplementedException();
        }

        public void Swimming_Speed()
        {
            throw new System.NotImplementedException();
        }

        public void Stop()
        {
            throw new System.NotImplementedException();
        }
    }

    public class Boat : WatherTransport
    {
        public void Go()
        {
            throw new System.NotImplementedException();
        }

        public void Stop()
        {
            throw new System.NotImplementedException();
        }

        public void Swimming_Speed()
        {
            throw new System.NotImplementedException();
        }
    }



    public class LandTransport : Transport
    {
        int WhellsCount { get; set; }

        public void Beep()
        {

        }

        public void Go()
        {
            throw new System.NotImplementedException();
        }

        public void Stop()
        {
            throw new System.NotImplementedException();
        }
    }


    public class Automobile : LandTransport
    {
        public void Beep()
        {
            throw new System.NotImplementedException();
        }

        public void Go()
        {
            throw new System.NotImplementedException();
        }

        public void Stop()
        {
            throw new System.NotImplementedException();
        }
    }

    public class Bus : Automobile
    {
        public void Beep()
        {
            throw new System.NotImplementedException();
        }

        public void Go()
        {
            throw new System.NotImplementedException();
        }

        public void Stop()
        {
            throw new System.NotImplementedException();
        }
    }

    public class Car : Automobile
    {
        public void Go()
        {
            throw new System.NotImplementedException();
        }

        public void Beep()
        {
            throw new System.NotImplementedException();
        }

        public void Stop()
        {
            throw new System.NotImplementedException();
        }
    }

}
