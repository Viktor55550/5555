﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EF_Test
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Design;
    using Microsoft.EntityFrameworkCore.Infrastructure;
    using Microsoft.Extensions.Configuration;
    using System.IO;
    using valod;

    namespace MyProject
    {
        public class NorthwindContextFactory : IDesignTimeDbContextFactory<NorthWindContext>
        {
            public NorthWindContext CreateDbContext(string[] args)
            {

                ConfigurationBuilder builder = new ConfigurationBuilder();
                // установка пути к текущему каталогу
                builder.SetBasePath(Directory.GetCurrentDirectory());
                // получаем конфигурацию из файла appsettings.json
                builder.AddJsonFile("appsettings.json");
                // создаем конфигурацию
                IConfiguration config = builder.Build();
                // получаем строку подключения
                string connectionString = config.GetConnectionString("NorthWindConnection");

                DbContextOptionsBuilder<NorthWindContext> optionsBuilder = new DbContextOptionsBuilder<NorthWindContext>();
                DbContextOptions<NorthWindContext> options = optionsBuilder
                    .UseSqlServer(connectionString).Options;

                return new NorthWindContext(options);
            }
        }
    }
}
