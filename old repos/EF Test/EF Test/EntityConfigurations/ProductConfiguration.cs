﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using valod.Entities;

namespace valod.EntityConfigurations
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Products", schema: "dbo");

            // primary key
            builder.HasKey(x => x.Id);
            // identity
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
        }
    }
}
