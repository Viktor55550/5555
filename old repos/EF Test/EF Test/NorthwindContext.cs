﻿using EF_Test.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using valod.Entities;
using valod.EntityConfigurations;

namespace valod
{
    // SQL SERVER C#-um
    public class NorthWindContext : DbContext
    {
        // Users Tabel c#-um
        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<OrderProduct> OrderProducts { get; set; }

        public NorthWindContext(DbContextOptions<NorthWindContext> options)
            : base(options)
        {
            // Jnjel Migration-i hamar
            //Database.EnsureCreated();
        }
        // Bazayin mer kargavorumnern enq talis
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // talis enq vor sql server e ev inch hasceyov kpni nran
            //optionsBuilder.UseSqlServer();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new OrderConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new OrderProductConfiguration());
        }
    }
}
