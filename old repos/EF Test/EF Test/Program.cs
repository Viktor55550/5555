﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using System.IO;
using EF_Test.MyProject;

namespace valod
{
    class Program
    {
        static void Main(string[] args)
        {
            NorthwindContextFactory factory = new NorthwindContextFactory();
            NorthWindContext db = factory.CreateDbContext(null);

            JsonOperations();

            AddUsersToDb(db);
            UpdateUsersFromDb(db);
            DeleteUsersFromDb(db);
            GetUsersFromDb(db);
        }

        private static void JsonOperations()
        {
            User a = new User { Age = 18, Id = 5, Name = "Valod" };

            // obyektic stanal json
            string jsonData = JsonConvert.SerializeObject(a);
            Console.WriteLine("Json User -> " + jsonData);

            // jsonic stanal obyekt
            User b = JsonConvert.DeserializeObject<User>(jsonData);
        }

        public static void AddUsersToDb(NorthWindContext db)
        {
            // создаем два объекта User
            User user1 = new User { Name = "Tom", Age = 33 };
            User user2 = new User { Name = "Alice", Age = 26 };
            // добавляем их в бд
            // AddRange - xmbov avelacnelu hamar
            db.Users.Add(user1);// INSERT INTO
            db.Users.Add(user2);
            db.SaveChanges();
            Console.WriteLine("Объекты успешно сохранены");
        }
        public static void GetUsersFromDb(NorthWindContext db)
        {

            List<User> users = db.Users.ToList();// SELECT * FROM Users
            foreach (User item in users)
            {
                Console.WriteLine($"{item.Id} - {item.Name} - {item.Age}");
            }
        }
        public static void UpdateUsersFromDb(NorthWindContext db)
        {

            /**
             * AsNoTracking-ov user popxakani kap@ ktrumm enq bazayic, 
             * hakarak depqum user-@ popoxeluc aranc Update-@ kanchelu bazayic poxvelu e
             */
            User user = db.Users.AsNoTracking().FirstOrDefault();
            user.Name = "Aramik";

            // SQL - UPDATE Users SET Name = 'Aramik' WHERE Id = 1
            // UpdateRange - ete cank e
            db.Users.Update(user);
            db.SaveChanges();
        }
        public static void DeleteUsersFromDb(NorthWindContext db)
        {

            User user = db.Users.LastOrDefault();

            // SQL - DELETE FROM Users WHERE Id = 1
            // RemoveRange - xmbov jnjelu hamar
            db.Users.Remove(user);
            db.SaveChanges();
        }
    }
}
