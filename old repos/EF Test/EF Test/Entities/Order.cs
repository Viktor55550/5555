﻿using EF_Test.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using valod.Entities;

namespace valod
{
    public class Order
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime RegisterDate { get; set; }

        public User User { get; set; }
        public ICollection<OrderProduct> OrderProducts{ get; set; }
    }
}
