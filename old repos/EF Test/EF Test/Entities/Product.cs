﻿using EF_Test.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace valod.Entities
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<OrderProduct> OrderProducts { get; set; }
    }
}
