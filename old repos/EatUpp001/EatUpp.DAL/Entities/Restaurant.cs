﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp001.DAL.DALInterfaces;

namespace EatUpp001.DAL.Entities
{
    public class Restaurant : IEntityWithId
    {
        public int Id { get; set; }
        public int MenuId { get; set; }
        public string RestaurantName { get; set; }
        public string RestaurantDescription { get; set; }
        public byte[] Picture { get; set; }
        public int? Ranking { get; set; }
        public string Address { get; set; }
        public bool RestaurantStatus { get; set; }
        public string RestaurantPassword { get; set; }

        public Menu Menu { get; set; }
        public ICollection<Event> Events { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
