﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp001.DAL.DALInterfaces;

namespace EatUpp001.DAL.Entities
{
    public class Menu : IEntityWithId
    {
        public int Id { get; set; }

        public ICollection<Restaurant> Restaurants { get; set; }
    }
}
