﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp001.DAL.DALInterfaces;

namespace EatUpp001.DAL.Entities
{
    public class Event : IEntityWithId
    {
        public int Id { get; set; }
        public int RestaurantId { get; set; }
        public string EventCategory { get; set; }

        public Restaurant Restaurant { get; set; }
    }
}
