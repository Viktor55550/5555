﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp001.DAL.DALInterfaces;

namespace EatUpp001.DAL.Entities
{
    public class User : IEntityWithId
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public string FirstName { get; set; }
        public string LastName{ get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public byte[] QRCode { get; set; }
        public string UserPassword { get; set; }
        public string Comments { get; set; }

        public Order Order { get; set; }
    }
}
