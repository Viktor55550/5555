﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp001.DAL.DALInterfaces;


namespace EatUpp001.DAL.Entities
{
    public class Order : IEntityWithId
    {
        public int Id { get; set; }
        public int RestaurantId { get; set; }
        public int Quantity { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime? CheckDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public bool? VIPZone { get; set; }
        public bool? SmokingZone { get; set; }
        public int ClientCount { get; set; }

        public Restaurant Restaurant { get; set; }
        public ICollection<User> Users { get; set; }
    }
}
