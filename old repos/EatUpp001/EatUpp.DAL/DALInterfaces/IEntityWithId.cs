﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp001.DAL.DALInterfaces
{
    public interface IEntityWithId
    {
        int Id { get; set; }
    }
}
