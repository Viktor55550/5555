﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary_tnayin
{
    class Program
    {
        static void Main(string[] args)
        {
            //Dictionary
        }
    }



    //List-i himan vra sarqecinq. bolor T-eri teg@ "KeyValuePair<TKey, TValue>" grelov ev ayd constructor@ grlov _array-i dimac,indexatorn e poxvac himqic
    public class MyDictionary<TKey, TValue> : IDictionary<TKey, TValue>, IEnumerable<KeyValuePair<TKey, TValue>>, IEnumerator<KeyValuePair<TKey, TValue>>
    {
        private KeyValuePair<TKey, TValue>[] _array;

        private int position = -1;

        public MyDictionary()
        {
            _array = new KeyValuePair<TKey, TValue>[0];
        }
        public MyDictionary(IEnumerable<KeyValuePair<TKey, TValue>> Collection)
        {
            _array = new KeyValuePair<TKey, TValue>[Collection.Count()];
            int index = 0;

            foreach (KeyValuePair<TKey, TValue> item in Collection)
            {
                _array[index++] = item;
            }
        }

        public TValue this[TKey key]
        {
            get
            {
                foreach (var item in _array)
                {
                    if (item.Key.Equals(key))
                    {
                        return item.Value;
                    }
                }
                return default(TValue);
            }
            set
            {
                for (int i = 0; i < _array.Length; i++)
                {
                    if (_array[i].Key.Equals(key))
                    {
                        KeyValuePair<TKey, TValue> item = new KeyValuePair<TKey, TValue>(_array[i].Key, value);
                        _array[i] = item;
                    }
                }
            }
        }

        public int Count
        {
            get
            {
                return _array.Length;
            }
        }

        public bool IsReadOnly => ((IList<KeyValuePair<TKey, TValue>>)_array).IsReadOnly;

        public KeyValuePair<TKey, TValue> Current
        {
            get
            {
                return _array[position];
            }
        }

        public ICollection<TKey> Keys
        {
            get
            {
                ICollection<TKey> collection = new List<TKey>();

                foreach (var item in _array)
                {
                    collection.Add(item.Key);
                }

                return collection;
            }
        }

        public ICollection<TValue> Values
        {
            get
            {
                ICollection<TValue> collection = new List<TValue>();

                foreach (var item in _array)
                {
                    collection.Add(item.Value);
                }

                return collection;
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return _array[position];
            }
        }
        public void Add(KeyValuePair<TKey, TValue> item)
        {
            KeyValuePair<TKey, TValue>[] temp = new KeyValuePair<TKey, TValue>[_array.Length + 1];

            for (int i = 0; i < _array.Length; i++)
            {
                temp[i] = _array[i];
            }
            temp[_array.Length] = item;

            _array = temp;
        }
        public void Add(TKey key, TValue value)
        {
            KeyValuePair<TKey, TValue> item = new KeyValuePair<TKey, TValue>(key, value);
            this.Add(item);
        }

        public void Clear()
        {
            _array = new KeyValuePair<TKey, TValue>[0];
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            for (int i = 0; i < _array.Length; i++)
            {
                if (_array[i].Equals(item))
                {
                    return true;
                }
            }

            return false;
        }

        public bool ContainsKey(TKey key)
        {
            for (int i = 0; i < _array.Length; i++)
            {
                if (_array[i].Key.Equals(key))
                {
                    return true;
                }
            }

            return false;
        }

        public void CopyTo(int index, KeyValuePair<TKey, TValue>[] array, int arrayIndex, int count)
        {
            for (int i = index, j = arrayIndex; i < index + count; i++, j++)
            {
                array[j] = _array[i];
            }
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return this;
        }


        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            KeyValuePair<TKey, TValue>[] temp = new KeyValuePair<TKey, TValue>[_array.Length - 1];
            bool isFound = false;

            for (int i = 0, j = 0; i < _array.Length; i++, j++)
            {
                if (_array[i].Equals(item) && !isFound)
                {
                    isFound = true;
                    j--;
                    continue;
                }
                if (i != j || i != _array.Length - 1)
                {
                    temp[j] = _array[i];
                }
            }

            if (isFound)
            {
                _array = temp;
            }

            return isFound;
        }

        public bool Remove(TKey item)
        {
            KeyValuePair<TKey, TValue>[] temp = new KeyValuePair<TKey, TValue>[_array.Length - 1];
            bool isFound = false;

            for (int i = 0, j = 0; i < _array.Length; i++, j++)
            {
                if (_array[i].Key.Equals(item) && !isFound)
                {
                    isFound = true;
                    j--;
                    continue;
                }
                if (i != j || i != _array.Length - 1)
                {
                    temp[j] = _array[i];
                }
            }

            if (isFound)
            {
                _array = temp;
            }

            return isFound;
        }


        IEnumerator IEnumerable.GetEnumerator()
        {
            return this;
        }

        public void Dispose()
        {
            // chgitenq hl@
        }

        public bool MoveNext()
        {
            if (position < _array.Length - 1)
            {
                position++;
                return true;
            }

            return false;
        }

        public void Reset()
        {
            position = -1;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            foreach (var item in _array)
            {
                if (item.Key.Equals(key))
                {
                    value = item.Value;
                    return true;
                }
            }
            value = default(TValue);
            return false;
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            CopyTo(0, array, arrayIndex, _array.Length);
        }
    }
}
