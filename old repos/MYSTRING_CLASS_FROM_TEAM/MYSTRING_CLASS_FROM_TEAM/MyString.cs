﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MYSTRING_CLASS_FROM_TEAM
{
    class MyString : IEnumerable, IEnumerator
    {
        private char[] _array { get; set; }
        private int position = -1;
        public char Current
        {
            get
            {
                return _array[position];
            }
        }
        object IEnumerator.Current
        {
            get
            {
                return _array[position];
            }
        }
        public MyString()
        {
            _array = new char[3] { 'p', 'o', 'x' };
        }
        public MyString(string a)
        {
            _array = a.ToCharArray();
        }
        public char this[int index]
        {
            get
            {
                return this._array[index];
            }
            set
            {
                this._array[index] = value;
            }
        }
        public IEnumerator GetEnumerator()
        {
            return this;
        }
        public bool MoveNext()
        {
            if (position < _array.Length - 1)
            {
                position++;
                return true;
            }
            return false;
        }
        public void Reset()
        {
            position = -1;
        }
        public int indexof(string value)
        {
            char[] _value = value.ToCharArray();

            int k = 0;
            if (_array[_array.Length - 1].Equals(_value[0]) && _value.Length > 1)
            {
                return -1;
            }
            if (_array.Length < _value.Length)
            {
                return -1;
            }
            for (int i = 0; i < _array.Length; i++)
            {
                if (_array[i].Equals(_value[0]))
                {

                    for (int j = 1; j < _value.Length; j++)
                    {
                        if (_array[i + j].Equals(_value[j]))
                        {
                            ++k;
                        }
                        if (k.Equals(_value.Length - 1))
                        {
                            return i;
                        }
                    }
                }

            }
            return -1;
        }
        public bool Endswith(string value)
        {
            char[] _value = value.ToCharArray();
            if (_value.Length > _array.Length)
            {
                return false;
            }
            int k = 0;
            for (int i = _array.Length - _value.Length, j = 0; i < _array.Length; i++, j++)
            {
                if (_array[i].Equals(_value[j]))
                {
                    ++k;
                }
                if (k.Equals(_value.Length))
                {
                    return true;
                }
            }
            return false;
        }
        public int IndexOfAny(char[] _value)
        {
            for (int i = 0; i < _value.Length; i++)
            {
                for (int j = 0; j < _array.Length; j++)
                {
                    if (_value[i].Equals(_array[j]))
                    {
                        return j;
                    }
                }
            }
            return -1;
        }
        public bool Contains(string value)
        {
            char[] _value = value.ToCharArray();
            int k = 0;
            for (int i = 0; i < _array.Length; i++)
            {
                if (_array[i].Equals(_value[0]))
                {
                    for (int j = 1; j < _value.Length; j++)
                    {
                        if (_array[i + j].Equals(_value[j]))
                        {
                            ++k;
                        }
                        if (k.Equals(_value.Length - 1))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        public string Insert(int index, string item)
        {
            char[] arr = item.ToCharArray();
            char[] temp = new char[_array.Length + arr.Length];
            for (int i = 0, j = 0; i < _array.Length; i++, j++)
            {
                if (i == index)
                {
                    for (int k = 0; k < arr.Length; k++)
                    {
                        temp[j++] = arr[k];
                    }
                }
                temp[j] = _array[i];
            }
            if (index == _array.Length)
            {
                for (int i = 0; i < arr.Length; i++)
                {
                    temp[_array.Length + i] = arr[i];
                }
            }
            string value = new string(temp);
            return value;
        }

        public int LastIndexOf(string value)
        {
            char[] _value = value.ToCharArray();
            int k = 0;
            if (_array[_array.Length - 1].Equals(_value[0]) && _value.Length > 1)
            {
                return -1;
            }
            if (_array.Length < _value.Length)
            {
                return -1;
            }
            for (int i = _array.Length - 1; i > 0; i--)
            {
                if (_array[i].Equals(_value[0]))
                {
                    for (int j = 1; j < _value.Length; j++)
                    {
                        if (_array[i + j].Equals(_value[j]))
                        {
                            ++k;
                        }
                        if (k.Equals(_value.Length - 1))
                        {
                            return i;
                        }
                    }
                }
            }
            return -1;
        }
        public void CopyTo(int sourceIndex, char[] value, int destinationIndex, int count)
        {
            for (int i = sourceIndex, j = destinationIndex; i < sourceIndex + count; i++, j++)
            {
                value[j] = _array[i];
            }
        }
        public int LastIndexOfAny(char[] _value)
        {
            for (int i = 0; i < _value.Length; i++)
            {
                for (int j = _array.Length - 1; j > 0; j--)
                {
                    if (_value[i].Equals(_array[j]))
                    {
                        return j;
                    }
                }
            }
            return -1;
        }
        public string remove(int value, int count)
        {
            char[] _value = new char[_array.Length - count];
            if (value.Equals(_array.Length - 1) && !count.Equals(1))
            {
                return "error";
            }
            for (int i = 0; i < value; i++)
            {
                _value[i] = _array[i];
            }
            for (int i = value + count, j = value; i < _array.Length; i++, j++)
            {
                _value[j] = _array[i];
            }
            string a = new string(_value);
            return a;

        }
        public string TrimStart()
        {
            char[] deleteStart = new char[1] { ' ' };
            int j = 0;
            for (int i = 0; i < _array.Length; i++)
            {
                if (_array[i] != deleteStart[0])
                {
                    break;
                }
                j++;
            }
            char[] temp = new char[_array.Length - j];
            for (int k = j, i = 0; k < _array.Length; k++, i++)
            {
                temp[i] = _array[k];
            }
            string p = new string(temp);
            return p;
        }
        public string TrimEnd()
        {
            char[] deleteStart = new char[1] { ' ' };
            int j = 0;
            for (int i = _array.Length - 1; i > 0; i--)
            {
                if (_array[i] != deleteStart[0])
                {
                    break;
                }
                j++;
            }
            char[] temp = new char[_array.Length - j];
            for (int i = 0; i < _array.Length - j; i++)
            {
                temp[i] = _array[i];
            }
            string p = new string(temp);
            return p;
        }
        public string Trim()
        {
            string temp = new string(_array);
            string value = temp.TrimEnd();
            string _value = value.TrimStart();
            return _value;
        }
        public bool StartsWith(String value)
        {
            char[] _value = value.ToCharArray();
            if (_value.Length > _array.Length)
            {
                return false;
            }
            int k = 0;
            for (int i = 0, j = 0; i < _array.Length; i++, j++)
            {
                if (_array[i].Equals(_value[j]))
                {
                    ++k;
                }
                if (k.Equals(_value.Length))
                {
                    return true;
                }
            }
            return false;
        }
        public string[] Split()
        {
            int k = 0;
            int value = 0;
            for (int i = 0; i < _array.Length; i++)
            {
                if (_array.Length.Equals(','))
                {
                    value++;
                }
            }
            string[] temp = new string[value + 1];
            char[] array = new char[_array.Length];

            for (int i = 0, j = 0; i < _array.Length; i++, k++, j++)
            {
                if (_array[i].Equals(','))
                {
                    temp[k] = new string(array);
                    continue;
                }
                array[j] = _array[i];
            }
            temp[k] = new string(array);
            return temp;

        }
        public string Substring(int index, int count)
        {
            if (_array.Length < count)
            {
                return "error";
            }
            char[] temp = new char[count];
            for (int i = index, j = 0; j < count; i++, j++)
            {
                temp[j] = _array[i];
            }
            string b = new string(temp);
            return b;
        }
        public string toupper()
        {
            int[] temp = new int[_array.Length];
            char[] temp1 = new char[_array.Length];
            for (int i = 0; i < _array.Length; i++)
            {
                temp[i] = Convert.ToInt32(_array[i]) - 32;
                temp1[i] = Convert.ToChar(temp[i]);
            }
            string b = new string(temp1);
            return b;
        }
        public string tolower()
        {
            int[] temp = new int[_array.Length];
            char[] temp1 = new char[_array.Length];
            for (int i = 0; i < _array.Length; i++)
            {
                temp[i] = Convert.ToInt32(_array[i]) + 32;
                temp1[i] = Convert.ToChar(temp[i]);
            }
            string b = new string(temp1);
            return b;
        }

    }
}
