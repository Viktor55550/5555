﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreadingTest
{
    public partial class Form1 : Form
    {
        Thread voidThreadExample = new Thread(() => { });
        Thread stopProgressBarThread = new Thread(stopWork);
        public Form1()
        {
            InitializeComponent();
        }

        private void startProgressBar(object progressBar)
        {
            /* syncron - standard state */
            //startWork(progressBar);

            /* Thread class */
            //startInNewThread(progressBar);


            /* ThreadPool class */
            //startInThreadPool(progressBar);

            /* Async version */
            /* .GetAwaiter().GetResult() - mer syncron method@ spasum e async method@-i veradarcvox arjeqin  */
            //startInAsync(progressBar).GetAwaiter().GetResult();
        }


        #region ------- standard version -------

        private static void startWork(object progressBar)
        {
            for (int i = 0; i <= 100; i++)
            {
                Thread.Sleep(30);
                ((ProgressBar)progressBar).Value = i;
            }
        }

        #endregion ------- standard version -------


        #region ------- Thread Class Using -------

        Thread startProgressBarThread = new Thread(startWork);

        private void startInNewThread(object progressBar)
        {
            startProgressBarThread = new Thread(startWork);
            startProgressBarThread.Start(progressBar); // progressBar-@ startWork-i argumentn e

            // karox eq grel kod@ vor@ karox e zugaher ashxatel

            //startProgressBarThread.Join(); // kanchum enq vor mer mainThread@ aysinqn cragir@ spasi minchev gorts@ avartvi
            //startProgressBarThread.Abort(); // anjatum enq thread-i ashxatanq@
        }

        #endregion ------- Thread Class Using -------


        #region ------- ThreadPool version -------

        private void startInThreadPool(object progressBar)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(startWork), progressBar);
        }

        #endregion ------- ThreadPool version -------

        #region ------- Async version -------

        private async Task startInAsync(object progressBar)
        {
            // inch vor async mthod
            // Task.Run-@ ira meji gortsoxutyun@ katarum e nor Task-i mej
            Task result = Task.Run(() => { startWork(progressBar); });

            //await result;
        }

        #endregion ------- ThreadPool version -------



        private void stopProgressBar(object progressBar)
        {
            startProgressBarThread.Abort();
        }

        //private static void stopInNewThread(object progressBar)
        //{
        //    ((ProgressBar)progressBar).Value = 0;
        //}
        private static void stopWork(object progressBar)
        {
            ((ProgressBar)progressBar).Value = 0;
        }

        private void Start1_Click(object sender, EventArgs e)
        {
            startProgressBar(progressBar1);
        }

        private void Start2_Click(object sender, EventArgs e)
        {
            startProgressBar(progressBar2);
        }

        private void Start3_Click(object sender, EventArgs e)
        {
            startProgressBar(progressBar3);
        }

        private void Start4_Click(object sender, EventArgs e)
        {
            startProgressBar(progressBar4);
        }

        private void Stop1_Click(object sender, EventArgs e)
        {
            stopProgressBar(progressBar1);
        }

        private void Stop2_Click(object sender, EventArgs e)
        {
            stopProgressBar(progressBar2);

        }

        private void Stop3_Click(object sender, EventArgs e)
        {
            stopProgressBar(progressBar3);

        }

        private void Stop4_Click(object sender, EventArgs e)
        {
            stopProgressBar(progressBar4);

        }

        private void Start_all_Click(object sender, EventArgs e)
        {
            startProgressBar(progressBar1);
            startProgressBar(progressBar2);
            startProgressBar(progressBar3);
            startProgressBar(progressBar4);
        }

        private void Stop_all_Click(object sender, EventArgs e)
        {
            stopProgressBar(progressBar1);
            stopProgressBar(progressBar2);
            stopProgressBar(progressBar3);
            stopProgressBar(progressBar4);
        }


        // thread-i sahamanapakman orinakner

        #region -- lock --
        static object obj = new object();

        void lockExample()
        {
            lock(obj)
            {
                int a = 5;
                int b = 6;

                int c = a + b;
            }
        }

        #endregion -- lock --

        #region -- monitor --

        void monitorExample()
        {
            Monitor.Enter(obj);
            int a = 5;
            int b = 6;

            int c = a + b;
            Monitor.Exit(obj);
        }

        #endregion -- monitor --


        #region -- mutex --
        Mutex mutex = new Mutex();

        void mutexExample()
        {
            mutex.WaitOne();
            int a = 5;
            int b = 6;

            int c = a + b;
            mutex.ReleaseMutex();
        }

        #endregion -- mutex --

        #region -- semaphore --
        Semaphore semaphore = new Semaphore(1, 1);

        void semaphoreExample()
        {
            semaphore.WaitOne();
            int a = 5;
            int b = 6;

            int c = a + b;
            semaphore.Release();
        }

        #endregion -- semaphore --

        #region -- semaphore --
        SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);

        void semaphoreSlimExample()
        {
            semaphoreSlim.Wait();
            int a = 5;
            int b = 6;

            int c = a + b;
            semaphoreSlim.Release();
        }

        #endregion -- semaphore --
    }
}
