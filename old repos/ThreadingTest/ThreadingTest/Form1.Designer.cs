﻿namespace ThreadingTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.progressBar3 = new System.Windows.Forms.ProgressBar();
            this.progressBar4 = new System.Windows.Forms.ProgressBar();
            this.start1 = new System.Windows.Forms.Button();
            this.start2 = new System.Windows.Forms.Button();
            this.start3 = new System.Windows.Forms.Button();
            this.start4 = new System.Windows.Forms.Button();
            this.stop4 = new System.Windows.Forms.Button();
            this.stop3 = new System.Windows.Forms.Button();
            this.stop2 = new System.Windows.Forms.Button();
            this.stop1 = new System.Windows.Forms.Button();
            this.start_all = new System.Windows.Forms.Button();
            this.stop_all = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(115, 111);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(521, 23);
            this.progressBar1.TabIndex = 0;
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(116, 179);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(521, 23);
            this.progressBar2.TabIndex = 1;
            // 
            // progressBar3
            // 
            this.progressBar3.Location = new System.Drawing.Point(117, 246);
            this.progressBar3.Name = "progressBar3";
            this.progressBar3.Size = new System.Drawing.Size(521, 23);
            this.progressBar3.TabIndex = 2;
            // 
            // progressBar4
            // 
            this.progressBar4.Location = new System.Drawing.Point(118, 321);
            this.progressBar4.Name = "progressBar4";
            this.progressBar4.Size = new System.Drawing.Size(521, 23);
            this.progressBar4.TabIndex = 3;
            // 
            // start1
            // 
            this.start1.Location = new System.Drawing.Point(12, 111);
            this.start1.Name = "start1";
            this.start1.Size = new System.Drawing.Size(75, 23);
            this.start1.TabIndex = 4;
            this.start1.Text = "Start";
            this.start1.UseVisualStyleBackColor = true;
            this.start1.Click += new System.EventHandler(this.Start1_Click);
            // 
            // start2
            // 
            this.start2.Location = new System.Drawing.Point(12, 179);
            this.start2.Name = "start2";
            this.start2.Size = new System.Drawing.Size(75, 23);
            this.start2.TabIndex = 5;
            this.start2.Text = "Start";
            this.start2.UseVisualStyleBackColor = true;
            this.start2.Click += new System.EventHandler(this.Start2_Click);
            // 
            // start3
            // 
            this.start3.Location = new System.Drawing.Point(12, 246);
            this.start3.Name = "start3";
            this.start3.Size = new System.Drawing.Size(75, 23);
            this.start3.TabIndex = 6;
            this.start3.Text = "Start";
            this.start3.UseVisualStyleBackColor = true;
            this.start3.Click += new System.EventHandler(this.Start3_Click);
            // 
            // start4
            // 
            this.start4.Location = new System.Drawing.Point(12, 321);
            this.start4.Name = "start4";
            this.start4.Size = new System.Drawing.Size(75, 23);
            this.start4.TabIndex = 7;
            this.start4.Text = "Start";
            this.start4.UseVisualStyleBackColor = true;
            this.start4.Click += new System.EventHandler(this.Start4_Click);
            // 
            // stop4
            // 
            this.stop4.Location = new System.Drawing.Point(683, 321);
            this.stop4.Name = "stop4";
            this.stop4.Size = new System.Drawing.Size(75, 23);
            this.stop4.TabIndex = 11;
            this.stop4.Text = "Stop";
            this.stop4.UseVisualStyleBackColor = true;
            this.stop4.Click += new System.EventHandler(this.Stop4_Click);
            // 
            // stop3
            // 
            this.stop3.Location = new System.Drawing.Point(683, 246);
            this.stop3.Name = "stop3";
            this.stop3.Size = new System.Drawing.Size(75, 23);
            this.stop3.TabIndex = 10;
            this.stop3.Text = "Stop";
            this.stop3.UseVisualStyleBackColor = true;
            this.stop3.Click += new System.EventHandler(this.Stop3_Click);
            // 
            // stop2
            // 
            this.stop2.Location = new System.Drawing.Point(683, 179);
            this.stop2.Name = "stop2";
            this.stop2.Size = new System.Drawing.Size(75, 23);
            this.stop2.TabIndex = 9;
            this.stop2.Text = "Stop";
            this.stop2.UseVisualStyleBackColor = true;
            this.stop2.Click += new System.EventHandler(this.Stop2_Click);
            // 
            // stop1
            // 
            this.stop1.Location = new System.Drawing.Point(683, 111);
            this.stop1.Name = "stop1";
            this.stop1.Size = new System.Drawing.Size(75, 23);
            this.stop1.TabIndex = 8;
            this.stop1.Text = "Stop";
            this.stop1.UseVisualStyleBackColor = true;
            this.stop1.Click += new System.EventHandler(this.Stop1_Click);
            // 
            // start_all
            // 
            this.start_all.Location = new System.Drawing.Point(79, 395);
            this.start_all.Name = "start_all";
            this.start_all.Size = new System.Drawing.Size(75, 23);
            this.start_all.TabIndex = 12;
            this.start_all.Text = "Start All";
            this.start_all.UseVisualStyleBackColor = true;
            this.start_all.Click += new System.EventHandler(this.Start_all_Click);
            // 
            // stop_all
            // 
            this.stop_all.Location = new System.Drawing.Point(540, 395);
            this.stop_all.Name = "stop_all";
            this.stop_all.Size = new System.Drawing.Size(75, 23);
            this.stop_all.TabIndex = 13;
            this.stop_all.Text = "Stop All";
            this.stop_all.UseVisualStyleBackColor = true;
            this.stop_all.Click += new System.EventHandler(this.Stop_all_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.stop_all);
            this.Controls.Add(this.start_all);
            this.Controls.Add(this.stop4);
            this.Controls.Add(this.stop3);
            this.Controls.Add(this.stop2);
            this.Controls.Add(this.stop1);
            this.Controls.Add(this.start4);
            this.Controls.Add(this.start3);
            this.Controls.Add(this.start2);
            this.Controls.Add(this.start1);
            this.Controls.Add(this.progressBar4);
            this.Controls.Add(this.progressBar3);
            this.Controls.Add(this.progressBar2);
            this.Controls.Add(this.progressBar1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.ProgressBar progressBar3;
        private System.Windows.Forms.ProgressBar progressBar4;
        private System.Windows.Forms.Button start1;
        private System.Windows.Forms.Button start2;
        private System.Windows.Forms.Button start3;
        private System.Windows.Forms.Button start4;
        private System.Windows.Forms.Button stop4;
        private System.Windows.Forms.Button stop3;
        private System.Windows.Forms.Button stop2;
        private System.Windows.Forms.Button stop1;
        private System.Windows.Forms.Button start_all;
        private System.Windows.Forms.Button stop_all;
    }
}

