﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegati_Tnayin_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string source = "h\re\nl\tl\n\no";

            StringBareviDelegate del = RemoveR;
            del += RemoveN;
            del += RemoveT;

            string result = HelloString(source, del);
            Console.Write(result);
            Console.WriteLine();
            Console.Write(source);

            Console.ReadKey();
        }

        private static string RemoveR(ref string source)
        {
            source = source.Replace("\r", "");

            return source;
        }
        private static string RemoveN(ref string source)
        {
            source = source.Replace("\n", "");

            return source;
        }

        private static string RemoveT(ref string source)
        {
            source = source.Replace("\t", "");

            return source;
        }

        private static string HelloString(string source, StringBareviDelegate filter)
        {
            return filter(ref source).Trim();
        }
    }

    delegate string StringBareviDelegate(ref string source);
}
