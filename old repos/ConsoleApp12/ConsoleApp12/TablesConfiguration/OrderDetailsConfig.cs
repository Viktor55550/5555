﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace ConsoleApp12.TablesConfiguration
{
    class OrderDetailsConfig : IEntityTypeConfiguration<OrderDetails>
    {
        public void Configure(EntityTypeBuilder<OrderDetails> builder)
        {
            builder.Property(a => a.UnitPrice).HasDefaultValue(0);
            builder.Property(a => a.Quantity).HasDefaultValue(1);
            builder.Property(a => a.Discount).HasDefaultValue(0);

            builder.HasKey(a => new { a.ProductID, a.OrderID });

            builder.HasOne(a => a.Orders)
                    .WithMany(a => a.OrderDetails)
                    .HasForeignKey(a => a.OrderID)
                    .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(a => a.Products)
                   .WithMany(a => a.OrderDetails)
                   .HasForeignKey(a => a.ProductID)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasIndex(a => a.ProductID);
            builder.HasIndex(a => a.OrderID);
        }
    }
}
