﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ConsoleApp12.TablesConfiguration
{
    class RegionConfig : IEntityTypeConfiguration<Region>
    {
        public void Configure(EntityTypeBuilder<Region> builder)
        {
            builder.HasKey(a => a.RegionID);
            builder.Property(a => a.RegionID).ValueGeneratedOnAdd();
            builder.Property(a => a.RegionDescription).HasColumnType("nvarchar(50)")
                                                      .IsRequired();
        }
    }
}
