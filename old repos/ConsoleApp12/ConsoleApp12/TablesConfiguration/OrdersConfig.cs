﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ConsoleApp12.TablesConfiguration
{
    class OrdersConfig : IEntityTypeConfiguration<Orders>
    {
        public void Configure(EntityTypeBuilder<Orders> builder)
        {
            builder.HasKey(a => a.OrdersId);
            builder.Property(a => a.Freight).HasDefaultValue(0m);
            builder.Property(a => a.ShipName).HasColumnType("nvarchar(40)");
            builder.Property(a => a.ShipAddress).HasColumnType("nvarchar(60)");
            builder.Property(a => a.ShipCity).HasColumnType("nvarchar(15)");
            builder.Property(a => a.ShipRegion).HasColumnType("nvarchar(15)");
            builder.Property(a => a.ShipPostalCode).HasColumnType("nvarchar(10)");
            builder.Property(a => a.ShipCountry).HasColumnType("nvarchar(15)");

            builder.HasOne(a => a.customer)
                   .WithMany(a => a.Orders)
                   .HasForeignKey(a => a.CustomerID)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(a => a.employee)
                   .WithMany(a => a.Orders)
                   .HasForeignKey(a => a.EmployeeID)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(a => a.shipper)
                   .WithMany(a => a.Orders)
                   .HasForeignKey(a => a.ShipVia)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasIndex(a => a.CustomerID);
            builder.HasIndex(a => a.EmployeeID);
            builder.HasIndex(a => a.OrderDate);
            builder.HasIndex(a => a.ShippedDate);
            builder.HasIndex(a => a.ShipVia);
            builder.HasIndex(a => a.ShipPostalCode);
        }
    }
}
