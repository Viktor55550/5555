﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ConsoleApp12.TablesConfiguration
{
    class ShippersConfig : IEntityTypeConfiguration<Shippers>
    {
        public void Configure(EntityTypeBuilder<Shippers> builder)
        {
            builder.HasKey(a => a.ShippersID);
            builder.Property(a => a.ShippersID).ValueGeneratedOnAdd();
            builder.Property(a => a.CompanyName).HasColumnType("nvarchar(40)")
                                                .IsRequired();
            builder.Property(a => a.Phone).HasColumnType("nvarchar(24)");

        }
    }
}
