﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ConsoleApp12.TablesConfiguration
{
    class TerritoriesConfig : IEntityTypeConfiguration<Territories>
    {
        public void Configure(EntityTypeBuilder<Territories> builder)
        {
            builder.HasKey(a => a.TerritoriesId);
            builder.Property(a => a.TerritoriesId).ValueGeneratedOnAdd();
            builder.Property(a => a.TerritoryDescription).HasColumnType("nchar(50)");

            builder.HasOne(a => a.region)
                    .WithMany(a => a.Territories)
                    .HasForeignKey(a => a.RegionID)
                    .OnDelete(DeleteBehavior.Restrict);



        }
    }
}
