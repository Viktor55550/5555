﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace ConsoleApp12.TablesConfiguration
{
    class ProductsConfig : IEntityTypeConfiguration<Products>
    {
        public void Configure(EntityTypeBuilder<Products> builder)
        {
            builder.HasKey(a => a.ProductsId);
            builder.Property(a => a.ProductsId).ValueGeneratedOnAdd();

            builder.Property(a => a.ProductName).HasColumnType("nvarchar(40)");
            builder.Property(a => a.QuantityPerUnit).HasColumnType("nvarchar(20)")
                                                    .IsRequired();
            builder.Property(a => a.UnitPrice).HasDefaultValue(0m);
            builder.Property(a => a.UnitsInStock).HasDefaultValue(0);
            builder.Property(a => a.UnitsOnOrder).HasDefaultValue(0);
            builder.Property(a => a.ReorderLevel).HasDefaultValue(0);

            builder.HasOne(a => a.Suppliers)
                   .WithMany(a => a.Products)
                   .HasForeignKey(a=>a.SuppliersID)
                   .OnDelete(DeleteBehavior.Restrict);


            builder.HasOne(a => a.Categorys)
                   .WithMany(a => a.Products)
                   .HasForeignKey(a => a.CategoriesId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasIndex(a => a.CategoriesId);
            builder.HasIndex(a => a.SuppliersID);
            builder.HasIndex(a => a.ProductName);
        
        }
    }
}
