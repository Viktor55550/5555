﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ConsoleApp12
{
    class CategoriesConfiguration : IEntityTypeConfiguration<Categories>
    {
        public void Configure(EntityTypeBuilder<Categories> builder)
        {
            builder.HasKey(a => a.CategoriesId);
            builder.Property(a => a.CategoriesId).ValueGeneratedOnAdd();
            builder.Property(a => a.CategoryName).HasColumnType("nvarchar(15)")
                                                 .IsRequired();
            builder.HasIndex(a => a.CategoryName);
            


         }
    }
}
