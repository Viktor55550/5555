﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ConsoleApp12.TablesConfiguration
{
    class CustumerCustuerDemoConfig : IEntityTypeConfiguration<CustomerCustomerDemo>
    {
        public void Configure(EntityTypeBuilder<CustomerCustomerDemo> builder)
        {
            builder.HasKey(a => new { a.CustomerID, a.CustomerTypeID });

            builder.HasOne(a => a.customers)
                   .WithMany(a => a.customerCustomerDemo)
                   .HasForeignKey(a => a.CustomerID)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(a => a.customerDemographics)
                   .WithMany(a => a.customerCustomerDemo)
                   .HasForeignKey(a => a.CustomerTypeID)
                   .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
