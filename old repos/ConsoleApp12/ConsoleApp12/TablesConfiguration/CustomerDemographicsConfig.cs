﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ConsoleApp12.TablesConfiguration
{
    class CustomerDemographicsConfig : IEntityTypeConfiguration<CustomerDemographics>
    {
        public void Configure(EntityTypeBuilder<CustomerDemographics> builder)
        {
            builder.HasKey(a => a.CustomerDemographicsId);
            builder.Property(a => a.CustomerDemographicsId).ValueGeneratedOnAdd();
        }
    }
}
