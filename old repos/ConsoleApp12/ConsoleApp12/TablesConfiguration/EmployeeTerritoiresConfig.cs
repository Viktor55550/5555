﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ConsoleApp12.TablesConfiguration
{
    class EmployeeTerritoiresConfig : IEntityTypeConfiguration<EmployeeTerritories>
    {
        public void Configure(EntityTypeBuilder<EmployeeTerritories> builder)
        {
            builder.HasKey(a => new { a.EmployeeID, a.TerritoryID });
            builder.HasOne(a => a.Employees)
                    .WithMany(a => a.EmployeeTerritories)
                    .HasForeignKey(a => a.EmployeeID)
                    .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(a => a.Territories)
                    .WithMany(a => a.EmployeeTerritories)
                    .HasForeignKey(a => a.TerritoryID)
                    .OnDelete(DeleteBehavior.Restrict);

        }
    }
}
