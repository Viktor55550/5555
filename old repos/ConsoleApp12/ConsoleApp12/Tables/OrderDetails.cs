﻿using System;
using System.Collections.Generic;

using System.Text;

namespace ConsoleApp12
{
    class OrderDetails
    {
        
        public int OrderID { get; set; }
        
        public int ProductID { get; set; }
        public decimal UnitPrice
        {
            get { return UnitPrice; }

            set
            {
                if (value >= 0)
                {
                    UnitPrice = value;
                }
            }
        }
        public short Quantity
        {
            get { return Quantity; }

            set
            {
                if (Quantity> 0)
                {
                    Quantity = value;
                }
            }
        }
        public double Discount
        {
            get { return Discount; }

            set
            {
                if (value >= 0 && value <= 1)
                {
                    Discount = value;
                }
            }
        }
		
        public Orders Orders { get; set; }

        public Products Products { get; set; }
    }
}
