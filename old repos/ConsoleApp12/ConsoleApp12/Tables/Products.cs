﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp12
{
    class Products
    {
        public int ProductsId { get; set; }
        public string ProductName { get; set; }
        public string QuantityPerUnit { get; set; }
        public decimal? UnitPrice
        {
            get { return UnitPrice; }

            set
            {
                if (value >= 0)
                {
                    UnitPrice = value;
                }
            }
        }
       
        public int? UnitsInStock
        {
            get { return UnitsInStock; }

            set
            {
                if (value >= 0)
                {
                    UnitsInStock = value;
                }
            }
        }
        public int? UnitsOnOrder
        {
            get { return UnitsOnOrder; }

            set
            {
                if (value >= 0)
                {
                    UnitsOnOrder = value;
                }
            }
        }
        public int? ReorderLevel
        {
            get { return ReorderLevel; }

            set
            {
                if (value >= 0)
                {
                    ReorderLevel = value;
                }
            }
        }
        public bool Discontinued { get; set; }

        public int SuppliersID { get; set; }

        public int CategoriesId { get; set; }
        public Suppliers Suppliers { get; set; }

        public Categories Categorys { get; set; }
        public ICollection<OrderDetails> OrderDetails { get; set; }
    }
}
