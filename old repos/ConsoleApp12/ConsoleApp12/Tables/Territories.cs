﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp12
{
    class Territories
    {
        public int TerritoriesId { get; set; }
        public string TerritoryDescription { get; set; }
        public int RegionID { get; set; }
            
        public Region region { get; set; }
        public ICollection<EmployeeTerritories> EmployeeTerritories { get; set; }
    }
}
