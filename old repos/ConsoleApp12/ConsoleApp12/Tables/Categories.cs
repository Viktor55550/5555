﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp12
{
    class Categories
    {
        public int CategoriesId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }

        public Byte[] Picture { get; set; }
        public ICollection<Products> Products { get; set; }

      
    }
}
