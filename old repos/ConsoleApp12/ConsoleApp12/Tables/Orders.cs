﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp12
{
    class Orders
    {
        public int OrdersId { get; set; }

        public DateTime? OrderDate { get; set; }
        public DateTime? RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
       
        public decimal? Freight { get; set; }
        public string ShipName { get; set; }
        public string ShipAddress { get; set; }
        public string ShipCity { get; set; }
        public string ShipRegion { get; set; }
        public string ShipPostalCode { get; set; }
        public string ShipCountry { get; set; }

		 public int? ShipVia { get; set; }
        public Shippers shipper { get; set; }
        public int? CustomerID { get; set; }
        public Customers customer { get; set; }
        public int? EmployeeID  { get; set; }

        public Employees employee { get; set; }

        public ICollection<OrderDetails> OrderDetails { get; set; }
    }
}
