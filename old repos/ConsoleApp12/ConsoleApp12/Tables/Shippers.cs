﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp12
{
    class Shippers
    {
        public int ShippersID { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }

        public ICollection<Orders> Orders { get; set; }
    }
}
