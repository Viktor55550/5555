﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp12
{
    class CustomerCustomerDemo
    { 
        
        public int CustomerID { get; set; }
        
        public int CustomerTypeID { get; set; }

        public Customers customers { get; set; }

        public CustomerDemographics customerDemographics { get; set; }
    }
}
