﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp12
{
    class EmployeeTerritories
    {
        
        public int EmployeeID { get; set; }
        
        public int TerritoryID { get; set; }

        public Employees Employees { get; set; }
        public Territories Territories { get; set; }
    }
}
