﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp12
{
    class Region
    {
        public int RegionID { get; set; }
        public string RegionDescription {get; set;}

        public ICollection<Territories> Territories { get; set; }
        public Region( string RegionDescription)
        {
           
            this.RegionDescription = RegionDescription;
        }

    }
}
