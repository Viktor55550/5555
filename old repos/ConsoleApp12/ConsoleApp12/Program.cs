﻿using ConsoleApp12;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp12
{
    class Program
    {
        static void Main(string[] args)
        {
           
            DbInformection();
            GetInformectionFromDb();
        }

        public static void DbInformection()
        {
            using (NewDb db = new NewDb())
            {

                Region region1 = new Region("Eastern");
                Region region2 = new Region("Western");
                
                db.Regions.Add(region1);
                db.Regions.Add(region2);
                db.SaveChanges();
                Console.WriteLine("true");
            }
        }
        public static void GetInformectionFromDb()
        {
            using (NewDb db = new NewDb())
            {
                List<Region> regions = db.Regions.ToList();
                foreach (Region item in regions)
                {
                    Console.WriteLine($"{item.RegionID} - {item.RegionDescription}");
                }
            }



        }
    }

}