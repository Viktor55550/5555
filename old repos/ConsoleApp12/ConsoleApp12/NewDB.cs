﻿using ConsoleApp12;
using ConsoleApp12.TablesConfiguration;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;


namespace ConsoleApp12
{
    class NewDb : DbContext
    {
        public DbSet<Categories> Categories { get; set; }
        public DbSet<CustomerDemographics> CusDem { get; set; }
        public DbSet<Customers> Customers { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Shippers> Shippers { get; set; }
        public DbSet<Suppliers> Suppliers { get; set; }

        public DbSet<CustomerCustomerDemo>  CustomerCustomerDemos { get; set; }

        public DbSet<Products> Products { get; set; }

        public DbSet<Territories> Territories { get; set; }

        public DbSet<Employees> Employees { get; set; }

        public DbSet<EmployeeTerritories> EmployeeTerritories { get; set; }

        public DbSet<Orders> Orders { get; set; }

        public DbSet<OrderDetails> OrderDetails { get; set; }

   
        public NewDb()
        {
            Database.EnsureCreated();
        }



        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=NewDb;Trusted_Connection=True;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new CategoriesConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerDemographicsConfig());
            modelBuilder.ApplyConfiguration(new CustumersConfig());
            modelBuilder.ApplyConfiguration(new RegionConfig());
            modelBuilder.ApplyConfiguration(new ShippersConfig());
            modelBuilder.ApplyConfiguration(new SuppliersConfig());
            modelBuilder.ApplyConfiguration(new CustumerCustuerDemoConfig());
            modelBuilder.ApplyConfiguration(new ProductsConfig());
            modelBuilder.ApplyConfiguration(new TerritoriesConfig());
            modelBuilder.ApplyConfiguration(new EmployeesConfig());
            modelBuilder.ApplyConfiguration(new EmployeeTerritoiresConfig());
            modelBuilder.ApplyConfiguration(new OrdersConfig());
            modelBuilder.ApplyConfiguration(new OrderDetailsConfig());
            
        }

    }
}
