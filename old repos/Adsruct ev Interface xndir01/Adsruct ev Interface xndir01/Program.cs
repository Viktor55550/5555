﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adsruct_ev_Interface_xndir01
{
    interface IMovable
    {
        void Move();
    }

    interface IFlyable
    {
        void Fly();
    }

    interface IDriveble
    {
        void Drive();
    }

    interface ISwimable
    {
        void Swim();
    }

    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public abstract class Transport
    {
        int humanCount { get; set; }

        public abstract void Go();

        public abstract void Stop();
        
    }

    public abstract class AirTransport : Transport, IMovable, IFlyable
    {
        public virtual void Move()
        {

        }

        public virtual void Fly()
        {

        }

        int FlyTime { get; set; }


        public abstract void SOS_Signal();
        

        public override/*abstract*/ void Go()
        {

        }
        

        public override void Stop()
        {

        }
        
    }

    public class Airplane : AirTransport,IMovable, IFlyable
    {
        public override void Move()
        {

        }

        public override void Fly()
        {

        }


        public override void SOS_Signal()
        {

        }

        public override void Go()
        {

        }

        public override void Stop()
        {

        }
    }


    public class Helicopter : AirTransport, IMovable, IFlyable
    {
        public override void Move()
        {

        }

        public override void Fly()
        {

        }



        public override void SOS_Signal()
        {

        }

        public override void Go()
        {

        }

        public override void Stop()
        {

        }
    }

    public class WatherTransport : Transport
    {
        char Material { get; set; }

        public virtual void Swimming_Speed()
        {

        }

        public override void Go()
        {

        }

        public override void Stop()
        {

        }
    }

    public class Boat : WatherTransport
    {
        public override void Swimming_Speed()
        {

        }

        public override void Go()
        {

        }

        public override void Stop()
        {

        }
    }



    public class LandTransport : Transport
    {
        int WhellsCount { get; set; }

        public virtual void Beep()
        {

        }

        public override void Go()
        {

        }

        public override void Stop()
        {

        }
    }


    public class Automobile : LandTransport
    {
        public override void Beep()
        {

        }

        public override void Go()
        {

        }

        public override void Stop()
        {

        }
    }

    public class Bus : Automobile
    {
        public override void Go()
        {

        }

        public override void Beep()
        {

        }

        public override void Stop()
        {

        }
    }

    public class Car : Automobile
    {
        new public void Beep()
        {

        }

        public override void Go()
        {

        }

        public override void Stop()
        {

        }
    }


}
