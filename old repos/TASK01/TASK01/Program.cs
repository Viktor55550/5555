﻿using System;

namespace TASK01
{
    class Program
    {
        static void Main(string[] args)
        {
            int k= 10;
            k = k++;
            Console.WriteLine(k);//10
            k++;              
            Console.WriteLine(k);//11

            int j = 10;
            j = ++j;
            Console.WriteLine(j);//11
            ++j;
            Console.WriteLine(j);//12
            
            
            /*Console.WriteLine($"In this information you can find results of int,string,bool,object and double data types.");
            int year = 2017;
            string name = "Donald Trump";
            bool best = true;
            object office = "White House";
            double height = 1.9;
            Console.WriteLine($"Full name:{name}  Main location:{office}  President since:{year}  Height:{height}M");
            Console.WriteLine($"Is it {best}, that he is one of the best presidents?");

            Console.WriteLine($"Results of byte,sbyte,short,ushort,uint,long,ulong,char by sequence.");

            byte bit1 = 200;
            Console.WriteLine(bit1);
            sbyte sbit1 = -100;
            Console.WriteLine(sbit1);
            short n1 = 0;
            Console.WriteLine(n1);
            ushort z = 100;
            Console.WriteLine(z);
            uint a = 10;
            Console.WriteLine(a);
            long b = -9000000;
            Console.WriteLine(b);
            ulong c = 254;
            Console.WriteLine(c);
            char d = 'A';
            Console.WriteLine(d);

            Console.WriteLine($"Type and size of int");
            Console.WriteLine(typeof(int));
            Console.WriteLine(sizeof(int));

            Console.WriteLine($"Type of string");
            Console.WriteLine(typeof(string));
            //Console.WriteLine(sizeof(string));

            Console.WriteLine($"Type and size of bool");
            Console.WriteLine(typeof(bool));
            Console.WriteLine(sizeof(bool));

            Console.WriteLine($"Type of object");
            Console.WriteLine(typeof(object));
            //Console.WriteLine(sizeof(object));

            Console.WriteLine($"Type and size of double");
            Console.WriteLine(typeof(double));
            Console.WriteLine(sizeof(double));

            Console.WriteLine($"Type and size of byte");
            Console.WriteLine(typeof(byte));
            Console.WriteLine(sizeof(byte));

            Console.WriteLine($"Type and size of sbyte");
            Console.WriteLine(typeof(sbyte));
            Console.WriteLine(sizeof(sbyte));

            Console.WriteLine($"Type and size of short");
            Console.WriteLine(typeof(short));
            Console.WriteLine(sizeof(short));

            Console.WriteLine($"Type and size of ushort");
            Console.WriteLine(typeof(ushort));
            Console.WriteLine(sizeof(ushort));

            Console.WriteLine($"Type and size of uint");
            Console.WriteLine(typeof(uint));
            Console.WriteLine(sizeof(uint));

            Console.WriteLine($"Type and size of long");
            Console.WriteLine(typeof(long));
            Console.WriteLine(sizeof(long));

            Console.WriteLine($"Type and size of ulong");
            Console.WriteLine(typeof(ulong));
            Console.WriteLine(sizeof(ulong));

            Console.WriteLine($"Type and size of char");
            Console.WriteLine(typeof(char));
            Console.WriteLine(sizeof(char));*/



            Console.ReadKey();
        }
    }
}
