﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _01DB_CSHARP_ov_tnayin
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Skizb");
            Console.WriteLine();
            Console.WriteLine("===============01");
            AddCategoriesToDb();
            GetCategoriesFromDb();
            Console.WriteLine("===============01");
            Console.WriteLine();

            Console.WriteLine("===============02");
            AddCustomersDemographicsToDb();
            GetCustomersDemographicsFromDb();
            Console.WriteLine("===============02");
            Console.WriteLine();

            Console.WriteLine("===============03");
            AddCustomersToDb();
            GetCustomersFromDb();
            Console.WriteLine("===============03");
            Console.WriteLine();

            Console.WriteLine("===============04");
            AddRegionToDb();
            GetRegionFromDb();
            Console.WriteLine("===============04");
            Console.WriteLine();

            Console.WriteLine("===============05");
            AddShippersToDb();
            GetShippersFromDb();
            Console.WriteLine("===============05");
            Console.WriteLine();

            Console.WriteLine("===============06");
            AddSuppliersToDb();
            GetSuppliersFromDb();
            Console.WriteLine("===============06");
            Console.WriteLine();


            Console.WriteLine("Avart");

            Console.ReadKey();
        }


        //01//2 methods for Categories Table
        public static void AddCategoriesToDb()
        {
            using (NWDPKContext db = new NWDPKContext())
            {
                Category Category1 = new Category { CategoryName = "Beverages", Description = "Soft drinks,coffees", Picture = "xxx" };
                Category Category2 = new Category { CategoryName = "Condiments", Description = "Sweet and savory sauces,relishes", Picture = "yyy" };

                db.Categories.Add(Category1);
                db.Categories.Add(Category2);
                db.SaveChanges();
                Console.WriteLine("Objects for Caterories saved");
            }
        }
        public static void GetCategoriesFromDb()
        {
            using (NWDPKContext db = new NWDPKContext())
            {
                List<Category> categories = db.Categories.ToList();

                foreach (Category item in categories)
                {
                    Console.WriteLine($"{item.CategoryId} - {item.CategoryName} - {item.Description} - {item.Picture}");
                }
            }
        }



        //02//2 methods for CustomersDemographics Table
        public static void AddCustomersDemographicsToDb()
        {
            using (NWDPKContext db = new NWDPKContext())
            {
                CustomersDemographic CustomersDemographic1 = new CustomersDemographic { CustomerDesc = "nor01" };
                CustomersDemographic CustomersDemographic2 = new CustomersDemographic { CustomerDesc = "nor02" };

                db.CustomersDemographics.Add(CustomersDemographic1);
                db.CustomersDemographics.Add(CustomersDemographic2);
                db.SaveChanges();
                Console.WriteLine("Objects for CustomersDemographics saved");
            }
        }
        public static void GetCustomersDemographicsFromDb()
        {
            using (NWDPKContext db = new NWDPKContext())
            {
                List<CustomersDemographic> customersDemographics = db.CustomersDemographics.ToList();

                foreach (CustomersDemographic item in customersDemographics)
                {
                    Console.WriteLine($"{item.CustomersDemographicId} - {item.CustomerDesc} ");
                }
            }
        }


        //03//2 methods for Customers Table
        public static void AddCustomersToDb()
        {
            using (NWDPKContext db = new NWDPKContext())
            {
                Customer Customer1 = new Customer
                {
                    CompanyName = "Alfreds Futterkiste",
                    ContactName = "Maria Anders",
                    ContactTitle = "Sales Representative",
                    Address = "Obere Str. 57",
                    City = "Berlin",
                    Region = "",
                    PostalCode = 12209,
                    Country = "Germany",
                    Phone = "030-0074321",
                    Fax = "030-0076545"
                };
                Customer Customer2 = new Customer
                {
                    CompanyName = "Bon app",
                    ContactName = "Ana Trujilo",
                    ContactTitle = "Owner",
                    Address = "Mataderos 2312",
                    City = "Mexico DF",
                    Region = "",
                    PostalCode = 05021,
                    Country = "Mexico",
                    Phone = "(5) 555-3932",
                    Fax = ""
                };

                db.Customers.Add(Customer1);
                db.Customers.Add(Customer2);
                db.SaveChanges();
                Console.WriteLine("Objects for Customers saved");
            }
        }
        public static void GetCustomersFromDb()
        {
            using (NWDPKContext db = new NWDPKContext())
            {
                List<Customer> customers = db.Customers.ToList();

                foreach (Customer item in customers)
                {
                    Console.WriteLine($"{item.CustomerId} - {item.CompanyName} - {item.ContactTitle} - {item.Address} - {item.City} - " +
                        $" {item.Region} - {item.PostalCode} - {item.Country} - {item.Phone} - {item.Fax}");
                }
            }
        }



        //04//2 methods for Region Table
        public static void AddRegionToDb()
        {
            using (NWDPKContext db = new NWDPKContext())
            {
                Regions Regions1 = new Regions { RegionDescription = "Eastern" };
                Regions Regions2 = new Regions { RegionDescription = "Western" };

                db.Region.Add(Regions1);
                db.Region.Add(Regions2);
                db.SaveChanges();
                Console.WriteLine("Objects for Region saved");


            }
        }
        public static void GetRegionFromDb()
        {
            using (NWDPKContext db = new NWDPKContext())
            {
                List<Regions> region = db.Region.ToList();

                foreach (Regions item in region)
                {
                    Console.WriteLine($"{item.RegionsId} - {item.RegionDescription}");
                }
            }
        }


        //05//2 methods for Shippers Table
        public static void AddShippersToDb()
        {
            using (NWDPKContext db = new NWDPKContext())
            {
                Shipper Shipper1 = new Shipper{ CompanyName = "Speedy Express", Phone = "(503) 555-9831" };
                Shipper Shipper2 = new Shipper { CompanyName = "United Package", Phone = "(503) 555-3199" };
               
                db.Shippers.Add(Shipper1);
                db.Shippers.Add(Shipper2);
                db.SaveChanges();
                Console.WriteLine("Objects for Shippers saved");


            }
        }
        public static void GetShippersFromDb()
        {
            using (NWDPKContext db = new NWDPKContext())
            {
                List<Shipper> shippers = db.Shippers.ToList();

                foreach (Shipper item in shippers)
                {
                    Console.WriteLine($"{item.ShipperId} - {item.CompanyName} - {item.Phone}");
                }
            }
        }



        //06//2 methods for Suppliers Table
        public static void AddSuppliersToDb()
        {
            using (NWDPKContext db = new NWDPKContext())
            {
                Supplier Supplier1 = new Supplier
                {
                    CompanyName = "Exotic Liquids",
                    ContactName = "Charlotte Cooper",
                    ContactTitle = "Purchasing Manager",
                    Address = "49 Gilbert St.",
                    City = "London",
                    Region = "",
                    PostalCode = 14,
                    Country = "UK",
                    Phone = "(171) 555-2222",
                    Fax = "",
                    HomePage = ""
                };
                Supplier Supplier2 = new Supplier
                {
                    CompanyName = "New orleans Cajun Delights",
                    ContactName = "Shelley Burke",
                    ContactTitle = "Order Admin",
                    Address = "P.O. Box 78934",
                    City = "New Orleans",
                    Region = "LA",
                    PostalCode = 70117,
                    Country = "USA",
                    Phone = "(100) 555-4822",
                    Fax = "",
                    HomePage = "#CAJUN.HTM#"
                };

                db.Suppliers.Add(Supplier1);
                db.Suppliers.Add(Supplier2);
                db.SaveChanges();
                Console.WriteLine("Objects for Suppliers saved");
            }
        }
        public static void GetSuppliersFromDb()
        {
            using (NWDPKContext db = new NWDPKContext())
            {
                List<Supplier> suppliers = db.Suppliers.ToList();

                foreach (Supplier item in suppliers)
                {
                    Console.WriteLine($"{item.SupplierId} - {item.CompanyName} - {item.ContactTitle} - {item.Address} - {item.City} - " +
                        $" {item.Region} - {item.PostalCode} - {item.Country} - {item.Phone} - {item.Fax} - {item.HomePage}");
                }
            }
        }
    }
}
