﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace _01DB_CSHARP_ov_tnayin
{
    class NWDPKContext : DbContext
    {
        // Primery Key-ov tablner
        public DbSet<Category> Categories { get; set; }
        public DbSet<CustomersDemographic> CustomersDemographics { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Regions> Region { get; set; }
        public DbSet<Shipper> Shippers { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }

        public NWDPKContext()
        {
            Database.EnsureCreated();
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=WZTMWZT-LR3IVLV;Database=NWDonlyPKtables;Trusted_Connection=True;");
        }
    }
}
