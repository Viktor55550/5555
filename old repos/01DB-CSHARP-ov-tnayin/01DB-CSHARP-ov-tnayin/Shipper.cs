﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _01DB_CSHARP_ov_tnayin
{
    class Shipper
    {
        public int ShipperId { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }
    }
}
