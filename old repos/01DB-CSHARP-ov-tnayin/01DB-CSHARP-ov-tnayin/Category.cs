﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _01DB_CSHARP_ov_tnayin
{
    class Category
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }
    }
}
