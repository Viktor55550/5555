﻿using System;
using System.Collections.Generic;

namespace xndir_list_generik
{
    class Coordinate<T>
    {
        public T Coordinate_X { get; set; }
        public T Coordinate_Y { get; set; }

        public Coordinate()
        {
        }

        public Coordinate(T coordinate_X, T coordinate_Y)
        {
            this.Coordinate_X = coordinate_X;
            this.Coordinate_Y = coordinate_Y;
        }

        public T GetCorrdinateX()
        {
            return this.Coordinate_X;
        }
        public T GetCorrdinateY()
        {
            return this.Coordinate_Y;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType()) return false;

            Coordinate<T> coordinateObject = (Coordinate<T>)obj;

            return coordinateObject.Coordinate_X.Equals(this.Coordinate_X) &&
                coordinateObject.Coordinate_Y.Equals(this.Coordinate_Y);
        }

        public override int GetHashCode()
        {
            return this.Coordinate_X.GetHashCode() ^
                this.Coordinate_Y.GetHashCode();
        }

        public override string ToString()
        {
            return $"{this.Coordinate_X} - {this.Coordinate_Y}";
        }
    }
}
