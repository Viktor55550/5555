﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace xndir_list_generik
{
    class MyList<T> : IEnumerator<T>, IEnumerable<T>
    {
        private T[] _array { get; set; }

        private int _position = -1;
        public T Current
        {
            get
            {
                return _array[_position];
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return _array[_position];
            }
        }

        public MyList()
        {
            _array = new T[0];
        }

        public T this[int index]
        {
            get
            {
                return this._array[index];
            }
            set
            {
                this._array[index] = value;
            }
        }

        public void Add(T item)
        {
            T[] temp = new T[this._array.Length + 1];

            for (int i = 0; i < this._array.Length; i++)
            {
                temp[i] = _array[i];
            }
            temp[this._array.Length] = item;

            this._array = temp;
        }

        public void AddRange(IEnumerable<T> arr)
        {
            foreach (T item in arr)
            {
                Add(item);
            }
        }

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            if (_position < _array.Length - 1)
            {
                _position++;
                return true;
            }
            return false;
        }

        public void Reset()
        {
            _position = -1;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this;
        }


        public void Clear()
        {
            //int[] datark = new int[0];

            this._array = new T[0];

            //this._array = new <T>datark[0];
        }
    }
}
