﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JarangiXndir01
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public class Transport
    {
        int humanCount { get; set; }

        public virtual void Go()
        {

        }

        public virtual void Stop()
        {

        }
    }

    public class AirTransport : Transport
    {
        int FlyTime { get; set; }
        

        public virtual void SOS_Signal()
        {

        }

        public override void Go()
        {
            
        }

        public override void Stop()
        {
            
        }
    }

    public class Airplane : AirTransport
    {
        public override void SOS_Signal()
        {
            
        }

        public override void Go()
        {
            
        }

        public override void Stop()
        {
            
        }
    }


    public class Helicopter : AirTransport
    {

        public override void SOS_Signal()
        {
            
        }

        public override void Go()
        {
            
        }

        public override void Stop()
        {
            
        }
    }

    public class WatherTransport : Transport
    {
        char Material { get; set; }

        public virtual void Swimming_Speed()
        {

        }

        public override void Go()
        {
            
        }

        public override void Stop()
        {
           
        }
    }

    public class Boat : WatherTransport
    {
        public override void Swimming_Speed()
        {
            
        }

        public override void Go()
        {
            
        }

        public override void Stop()
        {
            
        }
    }



    public class LandTransport : Transport
    {
        int WhellsCount { get; set; }

        public virtual void Beep()
        {

        }

        public override void Go()
        {
            
        }

        public override void Stop()
        {
            
        }
    }


    public class Automobile : LandTransport
    {
        public  void Beeb()
        {
            
        }

        public override void Go()
        {
            
        }

        public override void Stop()
        {
            
        }
    }

    public class Bus : Automobile
    {
        public override void Beep()
        {

        }

        public override void Go()
        {
           
        }
        
        public override void Stop()
        {
            
        }
    }

    public class Car : Automobile
    {
        new public void Beep()
        {
            
        }

        public override void Go()
        {
            
        }

        public override void Stop()
        {
            
        }
    }
}
