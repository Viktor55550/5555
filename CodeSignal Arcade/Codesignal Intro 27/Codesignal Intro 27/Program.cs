﻿using System;
using System.Text.RegularExpressions;

namespace Codesignal_Intro_27
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            string a = "var_1__Int";

            bool x = variableName(a);

            Console.WriteLine(x);
        }

        public static bool variableName(string name)
        {
            string strPattern = @"^[a-zA-Z_$][a-zA-Z_$0-9]*$";
            Regex re = new Regex(strPattern);

            if (re.IsMatch(name))
                return true;
            else
                return false;

        }

    }
}
