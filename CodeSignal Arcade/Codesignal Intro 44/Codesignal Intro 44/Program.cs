﻿using System;

namespace Codesignal_Intro_44
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            string a = "John.Smith@example.com";
            string x = findEmailDomain(a);
            Console.WriteLine(x);
        }
    
        static string findEmailDomain(string address)
        {
            int index = 0;
            for (int i = 0; i < address.Length; i++)
            {
                if (address[i] == '@')//int index = address.LastIndexOf ('@');

                    index = i + 1;
            }
            string res = address.Substring(index);

            return res;
        }
    }
}
