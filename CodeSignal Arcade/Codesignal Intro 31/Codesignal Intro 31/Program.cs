﻿using System;

namespace Codesignal_Intro_31
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int a = 100;
            int b = 20;
            int c = 170;

            int x = depositProfit(a, b, c);
            Console.WriteLine(x);
        }


        public static int depositProfit(int deposit, int rate, int threshold)
        {
            decimal dep = (decimal)deposit;
            int YearCount = 0;
            while (dep < (decimal)threshold)
            {
                dep += (dep * rate) / 100;
                YearCount += 1;
            }

            return YearCount;
        }
    }
}
