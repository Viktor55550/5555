﻿using System;

namespace Codesignal_The_Core_41
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] even = { 45, 23, 12, 33, 12, 453, -234, -45 };
            int[] even1 = { 7, 2, 2, 5, 10, 7 };
            int[] odd = { -5, -5, 10 };

            int[] a = replaceMiddle(even);

            foreach (var item in a)
            {
                Console.WriteLine(item);
            }
        }

        public static int[] replaceMiddle(int[] arr)
        {
            if (arr.Length == 2)
            {
                int[] res = new int[1];
                res[0] = arr[0] + arr[1];
                return res;
            }

            int[] even = new int[arr.Length - 1];

            if (arr.Length % 2 == 0)
            {
                int middle = arr[(arr.Length - 1) / 2] + arr[(arr.Length - 1) / 2 + 1];

                for (int i = 0, j = 0; i < even.Length; i++, j++)
                {
                    if (i == arr.Length / 2 - 1)
                    {
                        even[i] = middle;
                        j++;
                        continue;
                    }

                    even[i] = arr[j];
                }

                return even;
            }
            else
            {
                return arr;
            }
        }
    }
}
