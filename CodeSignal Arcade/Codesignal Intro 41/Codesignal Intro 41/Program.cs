﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Codesignal_Intro_41
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int a = 877;
            int x = digitDegree(a);

            Console.WriteLine(x);
        }

        static int digitDegree(int n)
        {
            if (n < 10)
            {
                return 0;
            }

            int res = 0;
            string x = n.ToString();

            List<int> list = new List<int>();

            for (int i = 0; i <= x.Length; i++)
            {

                if (i < x.Length)
                {
                    list.Add(int.Parse(x[i].ToString()));
                    continue;
                }
                else
                {
                    res++;
                    x = list.Take(list.Count).Sum().ToString();
                    i = -1;
                    list.Clear();
                    if (x.Length == 1)
                    {
                        return res;
                    }
                }
            }

            return -1;
        }
    }
}
