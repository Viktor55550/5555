﻿using System;
using System.Text;

namespace Codesignal_Intro_28
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            string a = "codesignal";

            string x = alphabeticShift(a);
            Console.WriteLine(x);
        }

        public static string alphabeticShift(string inputString)
        {
            StringBuilder sb = new StringBuilder(inputString);
            for (int i = 0; i < sb.Length; i++)
            {
                if (sb[i] == 'z')
                {
                    sb[i] = 'a';
                    continue;
                }
                sb[i] = (char)(((int)sb[i]) + 1);
            }
            inputString = sb.ToString();

            return inputString;
        }
    }
}
