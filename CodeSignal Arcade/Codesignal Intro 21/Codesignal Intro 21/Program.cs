﻿using System;

namespace Codesignal_Intro_21
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            string s = "0.254.255.0";

            bool x = isIPv4Address(s);

            Console.WriteLine(x);
        }

        public static bool isIPv4Address(string inputString)
        {
            string strPattern = @"(^([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$)";
            Regex re = new Regex(strPattern);

            if (re.IsMatch(inputString))
                return true;
            else
                return false;
        }
    }
}
