﻿using System;

namespace Codesignal_The_Core_25
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int a = leastFactorial(17);
            Console.WriteLine(" ->" + a);
        }
        public static int leastFactorial(int n)
        {
            int m = 1;
            for (int i = 1; i <= 120; i++)
            {
                m *= i;
                if (i <= n && n <= m)
                    return m;
            }
            return -1;
        }
    }
}
