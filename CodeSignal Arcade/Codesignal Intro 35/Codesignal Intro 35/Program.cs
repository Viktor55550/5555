﻿using System;

namespace Codesignal_Intro_35
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            string a = "var_1__Int";

            char x = firstDigit(a);

            Console.WriteLine(x);
        }

        static char firstDigit(string inputString)
        {
            int a;
            for (int i = 0; i < inputString.Length; i++)
            {
                bool x = int.TryParse(inputString[i].ToString(), out a);
                if (x == true)
                    return inputString[i];
            }

            return '*';
        }
    }
}
