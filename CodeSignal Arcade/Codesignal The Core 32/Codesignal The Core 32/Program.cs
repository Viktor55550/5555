﻿using System;

namespace Codesignal_The_Core_32
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int res = rounders(99);
            Console.WriteLine(res);
        }
        public static int rounders(int n)
        {
            string str = n.ToString();
            int[] a = new int[str.Length];

            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(str[i].ToString());
            }

            for (int i = a.Length - 1; i >= 0; i--)
            {
                if (a[i] >= 5)
                {
                    if (i == 0)
                    {
                        break;
                    }
                    a[i] = 0;
                    a[i - 1] = a[i - 1] + 1;
                }
                else
                {
                    if (i == 0)
                    {
                        break;
                    }
                    a[i] = 0;
                }
            }

            int resNumber = int.Parse(string.Concat(a));

            return resNumber;
        }
    }
}
