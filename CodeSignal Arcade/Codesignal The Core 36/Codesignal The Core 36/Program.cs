﻿using System;

namespace Codesignal_The_Core_36
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] arr = { 1, 2, 1 };

            int elemToReplace = 1;
            int substitutionElem = 3;

            int[] newArr = arrayReplace(arr, elemToReplace, substitutionElem);

            foreach (var item in newArr)
            {
                Console.WriteLine(item);
            }
        }

        public static int[] arrayReplace(int[] inputArray, int elemToReplace, int substitutionElem)
        {
            for (int i = 0; i < inputArray.Length; i++)
            {
                if (inputArray[i] == elemToReplace)
                {
                    inputArray[i] = substitutionElem;
                }
            }

            return inputArray;
        }
    }
}
