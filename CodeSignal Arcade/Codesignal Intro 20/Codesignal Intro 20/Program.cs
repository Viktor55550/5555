﻿using System;

namespace Codesignal_Intro_20
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] arr = { 2, 4, 1, 0 };

            int x = arrayMaximalAdjacentDifference(arr);

            Console.WriteLine(x);
        }

        public static int arrayMaximalAdjacentDifference(int[] inputArray)
        {
            /*int max = 0;
            for (int i = 1; i < inputArray.Length; i++)
            {
                max = Math.Max(max, Math.Abs(inputArray[i] - inputArray[i - 1]));
            }
            return max;*/

            int res = 0;
            int sum1 = 0;
            int sum2 = 0;
            int sumL = 0;
            int sumR = 0;
            int sumLR = 0;

            int left1 = inputArray[0] - inputArray[1];
            int left2 = inputArray[1] - inputArray[0];
            int last1 = inputArray[inputArray.Length - 1] - inputArray[inputArray.Length - 2];
            int last2 = inputArray[inputArray.Length - 2] - inputArray[inputArray.Length - 1];

            if (left1 > left2)
            {
                sumL = left1;
            }
            else
            {
                sumL = left2;
            }

            if (last1 > last2)
            {
                sumR = last1;
            }
            else
            {
                sumR = last2;
            }

            if (sumL > sumR)
            {
                sumLR = sumL;
            }
            else
            {
                sumLR = sumR;
            }
            int currentMax = 0;
            int loopMax = 0;
            for (int i = 1; i < inputArray.Length - 1; i++)
            {
                sum1 = inputArray[i] - inputArray[i - 1];
                sum2 = inputArray[i] - inputArray[i + 1];

                if (sum1 > sum2)
                {
                    currentMax = sum1;
                }
                else
                {
                    currentMax = sum2;
                }

                if (currentMax > loopMax)
                {
                    loopMax = currentMax;
                }
            }

            if (currentMax > loopMax)
            {
                res = currentMax;
            }
            else
            {
                res = loopMax;
            }

            if (res > sumLR)
            {
                return res;
            }
            else
            {
                return sumLR;
            }

        }
    }
}
