﻿using System;
using System.Collections.Generic;

namespace Codesignal_Intro_26
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            int a = 248622;

            bool x = evenDigitsOnly(a);
            Console.WriteLine(x);
        }

        public static bool evenDigitsOnly(int n)
        {
            string a = n.ToString();
            for (int i = 0; i < a.Length; i++)
            {
                int temp = Convert.ToInt32(a[i]);
                if (temp % 2 != 0)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
