﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Codesignal_The_Core_38
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] a = { 2, 2, 1 };
            int[] b = { 10, 11 };

            int[] res = concatenateArrays(a, b);

            foreach (var item in res)
            {
                Console.WriteLine(item);
            }
        }

        public static int[] concatenateArrays(int[] a, int[] b)
        {
            int[] bigArr = new int[a.Length + b.Length];

            List<int> listArr = a.ToList();
            for (int i = 0; i < b.Length; i++)
            {
                listArr.Add(b[i]);
            }

            bigArr = listArr.ToArray();

            return bigArr;
        }
    }
}
