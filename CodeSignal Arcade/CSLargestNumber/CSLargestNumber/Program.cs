﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSLargestNumber
{
    class Program
    {
        public static int largestNumber(int n)
        {
            if (n == 1)
            {
                return 9;
            }
            else if (n == 2)
            {
                return 99;
            }
            else if (n == 3)
            {
                return 999;
            }
            else if (n == 4)
            {
                return 9999;
            }
            else if (n == 5)
            {
                return 99999;
            }
            else if (n == 6)
            {
                return 999999;
            }
            else if (n == 7)
            {
                return 9999999;
            }
            else if (n == 8)
            {
                return 99999999;
            }
            else if (n == 9)
            {
                return 999999999;
            }
            else
            {
                return 1;
            }
        }
        public static int BestCode(int n)
        {
            string constant = "9";
            string rez = "";

            for (int i = 1; i <= n; i++)
            {
                rez = rez + constant;
            }
            return int.Parse(rez);//Convert.ToInt32(rez);

        }

        static void Main(string[] args)
        {
            Console.Write("nermuceq tiv: ");
            int a = int.Parse(Console.ReadLine());
            int x = largestNumber(a);
            Console.WriteLine(x + "\n");

            Console.Write("nermuceq tiv: ");
            int b = int.Parse(Console.ReadLine());
            int y = BestCode(b);
            Console.WriteLine(y);

            Console.ReadKey();
        }
    }
}
