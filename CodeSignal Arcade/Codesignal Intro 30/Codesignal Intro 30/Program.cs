﻿using System;

namespace Codesignal_Intro_30
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int n = 10;
            int firstNumber = 2;

            int res = circleOfNumbers(n, firstNumber);

            Console.WriteLine(res);
        }

        public static int circleOfNumbers(int n, int firstNumber) //No 6 in Core
        {
            if (firstNumber >= n / 2)
            {
                return firstNumber - n / 2;
            }
            else
            {
                return firstNumber + n / 2;
            }
        }
    }
}
