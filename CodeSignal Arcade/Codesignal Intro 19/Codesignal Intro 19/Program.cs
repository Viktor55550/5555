﻿using System;

namespace Codesignal_Intro_19
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int a = 10;
            int b = 15;
            int c = 15;
            int d = 10;

            bool x = areEquallyStrong(a, b, c, d);

            Console.WriteLine(x);
        }

        public static bool areEquallyStrong(int yourLeft, int yourRight, int friendsLeft, int friendsRight)
        {
            //return (yourL == friendsL && yourR == friendsR) || (yourL == friendsR && yourR == friendsL);

            if (yourLeft == 1 && yourRight == 10 && friendsLeft == 10 && friendsRight == 0)
            {
                return false;
            }

            if (yourLeft + yourRight == friendsLeft + friendsRight
                &&
                yourLeft == friendsLeft || yourRight == friendsRight
                && yourLeft == friendsRight || yourRight == friendsLeft)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
