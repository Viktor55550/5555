﻿using System;

namespace Codesignal_The_Core_35
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int arrSize = 4;

            int[] arr = createArray(arrSize);

            foreach (var item in arr)
            {
                Console.WriteLine(item);
            }
        }

        public static int[] createArray(int size)
        {
            int a = 1;
            int[] res = new int[size];

            for (int i = 0; i < res.Length; i++)
            {
                res[i] = a;
            }

            return res;
        }
    }
}
