﻿using System;

namespace Codesignal_The_Core_40
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] even = { 7, 2, 2, 5, 10, 7 };
            int[] odd = { -5, -5, 10 };

            bool a = isSmooth(even);

            Console.WriteLine(a);
        }

        public static bool isSmooth(int[] arr)
        {
            if (arr.Length % 2 == 0)//even case
            {
                if (arr[0] == arr[(arr.Length - 1) / 2] + arr[(arr.Length - 1) / 2 + 1] &&
                    arr[(arr.Length - 1) / 2] + arr[(arr.Length - 1) / 2 + 1] == arr[arr.Length - 1])
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            if (arr.Length % 2 != 0)//odd case
            {
                if (arr[0] == arr[(arr.Length - 1) / 2] && arr[(arr.Length - 1) / 2] == arr[arr.Length - 1])
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }
    }
}
