﻿using System;

namespace Codesignal_The_Core_37
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] arr = { 1, 2, 3, 4, 5 };

            int[] revArr = firstReverseTry(arr);

            foreach (var item in revArr)
            {
                Console.WriteLine(item);
            }
        }

        public static int[] firstReverseTry(int[] arr)
        {
            if (arr.Length == 0 || arr.Length == 1)
            {
                return arr;
            }

            int temp;
            temp = arr[0];
            arr[0] = arr[arr.Length - 1];
            arr[arr.Length - 1] = temp;

            return arr;
        }
    }
}
