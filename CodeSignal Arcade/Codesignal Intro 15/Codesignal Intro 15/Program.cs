﻿using System;

namespace Codesignal_Intro_15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            string[] a = { "abc", "ded" };
            //string[] a = { "a" };
            //string[] a = { "aa", "**", "zz"};
            //string[] a = { "abcde", "fghij", "klmno", "pqrst", "uvwxy" };
            //string[] a = { "wzy**" };

            string[] b = addBorder(a);

            foreach (var item in b)
            {
                Console.WriteLine(item);
            }
        }

        public static string[] addBorder(string[] picture)
        {

            string[] res = new string[picture.Length + 2];

            string topBottom = null;
            string x = "*";

            //for reith and left borders
            for (int i = 0; i < picture.Length; i++)
            {
                picture[i] = x + picture[i] + x;
            }

            //for top and bottom borders
            for (int i = 0; i < picture[0].Length; i++)
            {
                topBottom = topBottom + x;
            }

            res[0] = topBottom;
            res[res.Length - 1] = topBottom;

            for (int i = 0; i < picture.Length; i++)
            {
                res[i + 1] = picture[i];
            }

            return res;
        }
    }
}
