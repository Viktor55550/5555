﻿using System;

namespace Codesignal_The_Core_43
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int n = 11;
            bool a = isPower(n);

            Console.WriteLine(a);
        }

        public static bool isPower(int n)
        {
            if(n == 1)
            {
                return true;
            }
            bool result = false;
            int a = 2;
            int res = 0;
            for (int i = 0; i < n; i++)
            {
                int b = 2;
                for (int j = 0; j < n; j++)
                {
                    res = (int)Math.Pow(a, b);
                    if (res == n)
                    {
                        result = true;
                    }
                    res = 0;
                    b++;
                }
                a++;
            }

            return result;
        }
    }
}
