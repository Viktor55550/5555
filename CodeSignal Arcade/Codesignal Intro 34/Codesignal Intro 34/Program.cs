﻿using System;
using System.Collections.Generic;

namespace Codesignal_Intro_34
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] a = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            int k = 3;

            int[] b = extractEachKth(a, k);

            foreach (var item in b)
            {
                Console.WriteLine(item);
            }
        }
        static int[] extractEachKth(int[] inputArray, int k)
        {
            int count = 1;

            List<int> list = new List<int>();
            for (int i = 0; i < inputArray.Length; i++)
            {
                if (count % k == 0)
                {
                    count++;
                    continue;
                }

                list.Add(inputArray[i]);
                count++;
            }

            int[] res = list.ToArray();

            return res;
        }
    }
}
