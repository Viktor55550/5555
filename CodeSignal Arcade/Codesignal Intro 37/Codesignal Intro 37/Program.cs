﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Codesignal_Intro_37
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] a = { 2, 3, 5, 1, 6 };
            int k = 2;
            int x = arrayMaxConsecutiveSum(a, k);

            Console.WriteLine(x);
        }

        static int arrayMaxConsecutiveSum(int[] inputArray, int k)
        {
            List<int> list = new List<int>();

            int tempSum = default(int);

            int index = 0;

            for (int i = 0; i <= inputArray.Length - k; i++)
            {
                for (int j = 0; j < k; j++)
                {
                    tempSum += inputArray[j + index];
                }
                index++;

                list.Add(tempSum);
                tempSum = 0;
            }

            foreach (var item in list)
            {
                Console.WriteLine(item);
            }

            return list.Max();
        }
    }
}
