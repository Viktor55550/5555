﻿using System;

namespace Codesignal_Intro_40
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            string a = "0123456789";

            string x = longestDigitsPrefix(a);
            Console.WriteLine(x);
        }

        static string longestDigitsPrefix(string inputString)
        {
            string x = "";

            int temp = 0;

            for (int i = 0; i < inputString.Length; i++)
            {
                if (int.TryParse(inputString[i].ToString(), out temp))
                {
                    x = x + temp.ToString();
                    if (x.Length == inputString.Length)
                    {
                        return inputString;
                    }
                    continue;
                }
                return x;
            }

            return null;
        }
    }
}
