﻿using System;

namespace Codesignal_Intro_14
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] x = { 50, 60, 60, 45, 70 };

            int[] b = alternatingSums(x);

            foreach (var item in b)
            {
                Console.WriteLine(item);
            }
        }

        public static int[] alternatingSums(int[] a)
        {
            int[] res = new int[2];

            int team1 = 0;

            int team2 = 0;

            for (int i = 0; i < a.Length; i++)
            {
                if (i % 2 != 0)
                {
                    team2 = team2 + a[i];
                }
                else
                {
                    team1 = team1 + a[i];
                }
            }

            res[0] = team1;
            res[1] = team2;

            return res;
        }
    }
}
