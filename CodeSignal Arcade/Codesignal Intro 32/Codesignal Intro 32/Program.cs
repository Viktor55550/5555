﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Codesignal_Intro_32
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] a = { -10, -9, -8, -7, -6, -5, -4, -3, -2, -1,
                        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
                        13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
                        24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34,
                        35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45,
                        46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56,
                        57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67,
                        68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78,
                        79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89 };

            //int[] a = { 1, 1, 3, 4 };
            //int[] a = { -4, 1 };

            int x = absoluteValuesSumMinimization(a);
            Console.WriteLine(x);
        }

        public static int absoluteValuesSumMinimization(int[] a)
        {
            int x = 0;

            List<int> xArr = new List<int>();
            List<int> key = new List<int>();
            List<int> val = new List<int>();

            if (a.Length == 2)
            {
                return a[0];
            }

            for (int i = 0; i < a.Length; i++)
            {
                x = a[i];
                int resX = 0;
                xArr.Clear();
                for (int j = 0; j < a.Length; j++)
                {
                    resX = Math.Abs(a[j] - x);
                    xArr.Add(resX);
                }
                int xCount = 0;

                for (int k = 0; k < a.Length; k++)
                {
                    xCount += xArr[k];
                }
                key.Add(xCount);
                val.Add(x);
            }

            int keyMin = key.Min();
            int res = 0;

            for (int i = 0; i < a.Length; i++)
            {
                if ((key[i] == keyMin))
                {
                    res = val[i];
                    break;
                }
            }

            return res;
        }
    }
}
