﻿using System;
using System.Collections.Generic;

namespace Codesignal_Intro_36
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            string a = "cabca";
            int x = differentSymbolsNaive(a);
            Console.WriteLine(x);
        }

        static int differentSymbolsNaive(string s)
        {
            List<char> list = new List<char>();

            for (int i = 0; i < s.Length; i++)
            {
                if (list.Contains(s[i]))
                {
                    continue;
                }
                else
                {
                    list.Add(s[i]);
                }
            }

            return list.Count;
        }
    }
}
