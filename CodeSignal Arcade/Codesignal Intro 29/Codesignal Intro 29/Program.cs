﻿using System;

namespace Codesignal_Intro_29
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            string a = "C8";
            string b = "H1";

            bool x = chessBoardCellColor(a, b);
            Console.WriteLine(x);
        }

        public static bool chessBoardCellColor(string cell1, string cell2)
        {
            if (cell1.Equals(cell2))
            {
                return true;
            }

            int color1 = 0;
            int color2 = 0;

            int char1 = cell1[0];
            int num1 = cell1[1];
            int char2 = cell2[0];
            int num2 = cell2[1];

            if ((char1 % 2 != 0 && num1 % 2 != 0) || (char1 % 2 == 0 && num1 == 0))
            {
                color1 = 0;
            }
            else
            if ((char1 % 2 == 0 && num1 % 2 != 0) || (char1 % 2 != 0 && num1 % 2 == 0))
            {
                color1 = 1;
            }

            if ((char2 % 2 != 0 && num2 % 2 != 0) || (char2 % 2 == 0 && num2 == 0))
            {
                color2 = 0;
            }
            else
            if ((char2 % 2 == 0 && num2 % 2 != 0) || (char2 % 2 != 0 && num2 % 2 == 0))
            {
                color2 = 1;
            }

            bool res = false;
            if (color1 == color2)
            {
                res = true;
            }
            else
            {
                res = false;
            }

            return res;
        }
    }
}
