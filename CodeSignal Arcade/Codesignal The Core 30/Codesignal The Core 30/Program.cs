﻿using System;

namespace Codesignal_The_Core_30
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            int res = appleBoxes(12);
            Console.WriteLine(res);
        }

        public static int appleBoxes(int k)
        {
            int yellow = 0;
            int red = 0;

            if (k % 2 != 0)
            {
                for (int i = k; i >= 1; i -= 2)
                {
                    yellow += i * i;
                }
                for (int i = k - 1; i >= 2; i -= 2)
                {
                    red += i * i;
                }
                Console.WriteLine(yellow);
                Console.WriteLine(red);
                return red - yellow;
            }
            if (k % 2 == 0)
            {
                for (int i = k - 1; i >= 2; i -= 2)
                {
                    yellow += i * i;
                }
                for (int i = k; i >= 1; i -= 2)
                {
                    red += i * i;
                }
                Console.WriteLine(yellow);
                Console.WriteLine(red);
                return red - yellow - 1;
            }

            return 0;
        }
    }
}
