﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Codesignal_Intro_43
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            string a = "aabbb";//"bbbaacdafe";
            bool x = isBeautifulString(a);

            Console.WriteLine(x);
        }

        static bool isBeautifulString(string inputString)
        {
            if (!inputString.Contains('a'))
            {
                return false;
            }

            char[] arr = inputString.ToCharArray();
            Array.Sort(arr);

            foreach (var item in arr)
            {
                Console.WriteLine(item);
            }

            for (int i = 0; i < arr.Length - 1; i++)
            {
                if (!(arr[i] == arr[i + 1] || arr[i] < arr[i + 1]))
                {
                    return false;
                }
            }

            HashSet<char> unique = new HashSet<char>(arr);
            List<char> list = unique.ToList<char>();

            for (int i = 0; i < list.Count - 1; i++)
            {
                if ((int)list[i + 1] - (int)list[i] != 1)
                {
                    return false;
                }
            }

            Dictionary<char, int> dict = new Dictionary<char, int>();

            for (int i = 0; i < list.Count; i++)
            {
                int count = 0;
                for (int j = 0; j < arr.Length; j++)
                {
                    if (list[i] == arr[j])
                        count++;
                }
                dict.Add(list[i], count);
            }

            for (int i = 0; i < dict.Count - 1; i++)
            {
                if (dict[list[i]] < dict[list[i + 1]])
                {
                    return false;
                }
            }

            return true;
        }

    }
}
