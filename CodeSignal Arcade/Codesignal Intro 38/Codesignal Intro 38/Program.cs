﻿using System;

namespace Codesignal_Intro_38
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int upSpped = 100;
            int downSpeed = 10;
            int desireHeight = 910;

            int x = growingPlant(upSpped, downSpeed, desireHeight);

            Console.WriteLine(x);
        }

        static int growingPlant(int upSpeed, int downSpeed, int desiredHeight)
        {
            if (upSpeed >= desiredHeight)
            {
                return 1;
            }

            int day = upSpeed;
            int res = 0;
            while (day <= desiredHeight)
            {
                day += upSpeed - downSpeed;

                res++;
            }

            if (upSpeed + downSpeed == desiredHeight)
                return res + 1;

            return res;
        }
    }
}
