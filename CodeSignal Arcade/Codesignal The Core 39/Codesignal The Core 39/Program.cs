﻿using System;
using System.Collections.Generic;

namespace Codesignal_The_Core_39
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] a = { 2, 3, 2, 3, 4, 5 };

            int l = 2;
            int r = 4;
            int[] res = removeArrayPart(a, l, r);

            foreach (var item in res)
            {
                Console.WriteLine(item);
            }
        }

        public static int[] removeArrayPart(int[] inputArray, int l, int r)
        {
            List<int> list = new List<int>();
            for (int i = 0; i < inputArray.Length; i++)
            {
                if (i >= l && i <= r)
                {
                    continue;
                }
                list.Add(inputArray[i]);
            }

            int[] resArr = list.ToArray();

            return resArr;
        }
    }
}
