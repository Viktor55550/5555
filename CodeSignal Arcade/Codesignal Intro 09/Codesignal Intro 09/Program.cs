﻿using System;
using System.Collections.Generic;

namespace Codesignal_Intro_09
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            string[] input = new string[5] { "aba", "aa", "ad", "vcdd", "abc" };
            //string[] input = new string[3] { "aba","vcd", "abb" };
            //string[] input = new string[1] { "aba" };

            string[] x = AllLongestStrings(input);

            Console.WriteLine("My output");
            foreach (var output in x)
            {
                Console.WriteLine(output);
            }
        }


        public static string[] AllLongestStrings(string[] inputArray)
        {
            //solution with List
            /*int max = 0;
            for (int i = 0; i < inputArray.Length; i++)
            {
                if (inputArray[i].Length >= max)
                {
                    max = inputArray[i].Length;
                }
            }
            var myList = new List<string>();
            for (int i = 0; i < inputArray.Length; i++)
            {
                if (inputArray[i].Length == max) myList.Add(inputArray[i]);
            }

            string[] myOutput = myList.ToArray();

            return myOutput;
            */

            Console.WriteLine("My inputed array");
            for (int i = 0; i < inputArray.Length; i++)
            {
                Console.WriteLine(inputArray[i]);
            }

            if (inputArray.Length == 1)
            {
                return inputArray;
            }

            string temp1 = null;

            for (int i = 0; i < inputArray.Length; i++)
            {
                for (int j = 0; j < inputArray.Length - 1; j++)
                {
                    if (inputArray[j].Length <= inputArray[j + 1].Length)
                    {
                        temp1 = inputArray[j];
                        inputArray[j] = inputArray[j + 1];
                        inputArray[j + 1] = temp1;
                    }
                }
            }

            Console.WriteLine("sorded input");

            foreach (var sortedInput in inputArray)
            {
                Console.WriteLine(sortedInput);
            }

            int lengthOfNewArr = 0;

            for (int i = 0; i < inputArray.Length - 1; i++)
            {
                if (inputArray[i].Length >= inputArray[i + 1].Length)
                {
                    lengthOfNewArr++;
                }
            }

            Console.WriteLine(lengthOfNewArr);

            string[] newArr = new string[lengthOfNewArr];

            string temp2 = null;

            for (int i = 0; i < newArr.Length; i++)
            {
                if (inputArray[i].Length >= inputArray[i + 1].Length)
                {
                    temp2 = inputArray[i];
                    newArr[i] = temp2;
                }
            }
            return newArr;
        }
    }
}
