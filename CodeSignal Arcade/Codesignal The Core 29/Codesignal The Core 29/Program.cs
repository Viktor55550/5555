﻿using System;

namespace Codesignal_The_Core_29
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int res = additionWithoutCarrying(456, 1734);
            Console.WriteLine(res);
        }
        public static int additionWithoutCarrying(int param1, int param2)
        {
            string p1 = param1.ToString();
            string p2 = param2.ToString();

            string tempRes = "";

            while (p1.Length < p2.Length)
            {
                p1 = "0" + p1;
            }
            while (p1.Length > p2.Length)
            {
                p2 = "0" + p2;
            }

            for (int i = 0; i <= p1.Length - 1; i++)
            {
                int a = Convert.ToInt32(p1[i].ToString());
                int b = Convert.ToInt32(p2[i].ToString());

                tempRes += ((a + b) % 10).ToString();
            }
            int res = Convert.ToInt32(tempRes);

            return res;
        }
    }
}
