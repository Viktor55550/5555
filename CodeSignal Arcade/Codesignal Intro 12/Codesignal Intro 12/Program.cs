﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Codesignal_Intro_12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] b = new int[10];
            int[] c = { 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1 };//{ -1, 150, 190, 170, -1, -1, 160, 180 }; //
            int[] a = sortByHeight(c);

            Console.WriteLine(" \n ===================================");

            foreach (var item in a)
            {
                Console.Write(item + " ");
            }
        }


        public static int[] sortByHeigght2(int[] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] > -1)
                {
                    for (int j = 0; j < a.Length; j++)
                    {
                        if (a[j] != -1 && a[i] < a[j])
                        {
                            int k = a[i];
                            a[i] = a[j];
                            a[j] = k;
                        }
                    }
                }
            }

            return a;
        }

        public static int[] sortByHeight(int[] a)
        {
            /*Random rand = new Random();
            Console.Write("unsorted array-> ");
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = rand.Next(-1, 5);

                Console.Write(a[i] + " ");
            }
            Console.WriteLine("===================================");*/

            if (a.Contains(-1) == false)
            {
                Array.Sort(a);
                return a;
            }

            List<int> listFromInput = new List<int>();
            List<int> listOfIndexes = new List<int>();

            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] > 0)
                {
                    listFromInput.Add(a[i]);
                }
            }

            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] == -1)
                {
                    listOfIndexes.Add(i);
                }
            }

            Console.WriteLine(" \n \n indexes");
            foreach (var item in listOfIndexes)
            {
                Console.Write(item + " ");
            }

            Console.WriteLine(" \n \n unsorted list only positives");
            foreach (var item in listFromInput)
            {
                Console.Write(item + " ");
            }

            int[] arrAbove0 = listFromInput.ToArray();
            Array.Sort(arrAbove0);
            Console.WriteLine(" \n \n sorted list only positives as array");


            foreach (var item in arrAbove0)
            {
                Console.Write(item + " ");
            }


            List<int> sortedList = arrAbove0.ToList();

            int[] res = new int[a.Length];
            int[] indexes = listOfIndexes.ToArray();

            int index = 0;
            int sortIndex = 0;

            for (int i = 0; i < res.Length; i++)
            {
                //inser -1s
                if (index == indexes.Length)
                {
                    index = 0;
                }
                if (indexes[index] == i)
                {
                    res[i] = -1;
                    index++;
                }

                //insert sorted positives 
                if (sortIndex == arrAbove0.Length)
                {
                    sortIndex = 0;
                }
                if (res[i] != -1)
                {
                    res[i] = arrAbove0[sortIndex];
                    sortIndex++;
                }
            }
            Console.WriteLine();
            foreach (var item in res)
            {
                Console.WriteLine(item);
            }

            return res;
        }
    }
}
