﻿using System;

namespace Codesignal_The_Core_27
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            int a = magicalWell(1, 1, 1);

            Console.WriteLine(a);
        }
        public static int magicalWell(int a, int b, int n)
        {
            int rez = 0;
            for (int i = 1; i <= n; i++)
            {
                rez = rez + (a + b);
                a++;
                b++;
            }

            return rez;
        }

    }
}
