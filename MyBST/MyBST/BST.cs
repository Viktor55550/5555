﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBST
{
    public class BST
    {
        BST left;
        BST right;
        int data;
        public BST(int data)
        {
            this.data = data;
        }

        public void Visit()
        {
            Console.WriteLine(data);
        }

        public void Insert(int value)
        {
            if (value <= data)
            {
                if (left == null)
                {
                    left = new BST(value);
                }
                else
                {
                    left.Insert(value);
                }
            }
            else
            {
                if (right == null)
                {
                    right = new BST(value);
                }
                else
                {
                    right.Insert(value);
                }
            }
        }

        public bool IsContain(int value)
        {
            if (value == data)
            {
                return true;

            }
            else if (value < data)
            {
                if (left == null)
                {
                    return false;
                }
                else
                {
                    return left.IsContain(value);
                }
            }
            else
            {
                if (right == null)
                {
                    return false;
                }
                else
                {
                    return right.IsContain(value);
                }
            }
        }

        public void Print()
        {
            if(left != null)
            {
                left.Print();
            }
            Console.WriteLine(data);
            if (right != null)
            {
                right.Print();
            }
        }
    }
}
