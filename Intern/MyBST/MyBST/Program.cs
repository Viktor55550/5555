﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBST
{
    class Program
    {
        static void Main(string[] args)
        {
            BST tree = new BST(10);
            tree.Insert(5);
            tree.Insert(20);
            tree.Insert(3);
            tree.Insert(9);
            tree.Insert(15);
            tree.Insert(25);
            tree.Visit();
            //tree.Visit();

            Console.ReadKey();
        }
    }
}
