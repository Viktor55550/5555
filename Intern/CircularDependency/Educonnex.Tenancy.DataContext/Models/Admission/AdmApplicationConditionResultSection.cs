﻿using Educonnex.Core.SystemCodes.Models;
using ForCircularDep;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Educonnex.Tenancy.DataContext.Models
{
    public class AdmApplicationConditionResultSection : IAdmApplicationConditionResultSection
    {
        public int Id { get; set; }

        public int ConditionId { get; set; }

        public int TestId { get; set; }

        public int SecitionId { get; set; }

        public decimal Score { get; set; }

        public DateTime Created { get; set; }

        public DateTime? LastModified { get; set; }


        public virtual SystemCode SystemCode { get; set; }
    }
}
