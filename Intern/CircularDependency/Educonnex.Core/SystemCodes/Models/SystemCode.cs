﻿using ForCircularDep;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Educonnex.Core.SystemCodes.Models
{
    public class SystemCode
    {
        public int? Id { get; set; }

        public int? RegionId { get; set; }

        public string RegionName { get; set; }

        public int? SchoolId { get; set; }

        public string SchoolName { get; set; }

        public int? CampusId { get; set; }

        public string CampusName { get; set; }

        public string CodeType { get; set; }

        public string CodeKey { get; set; }

        public string CodeValue1 { get; set; }

        public string CodeValue2 { get; set; }

        public bool IsActive { get; set; }

        public bool IsLocked { get; set; }

        public int? Order { get; set; }


        public ICollection<IAdmApplicationConditionResultSection> AdmApplicationConditionResultSections { get; set; }
    }
}
