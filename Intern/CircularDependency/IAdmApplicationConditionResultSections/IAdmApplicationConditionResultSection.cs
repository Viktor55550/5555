﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForCircularDep
{
    public interface IAdmApplicationConditionResultSection
    {
        int Id { get; set; }

        int ConditionId { get; set; }

        int TestId { get; set; }

        int SecitionId { get; set; }

        decimal Score { get; set; }

        DateTime Created { get; set; }

        DateTime? LastModified { get; set; }

    }
}
