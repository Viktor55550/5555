﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI_WinFormAppFirstTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure to close this form", "Confirm",
                MessageBoxButtons.YesNo,MessageBoxIcon.Question);

            if (dr == DialogResult.Yes)
            //MessageBox.Show("Are you sure to close this form","Confirm");
            this.Close();
            //MessageBox.Show("Barev aper jan");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            button1.Text = "closeform";
        }
    }
}
