﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySingletone
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("MySingletone");
            MySingletone mySingletone = new MySingletone();

            MySingletone.GetIns();



            Console.ReadKey();
        }
    }

    public class MySingletone
    {
        private static MySingletone ins;

        private static object obj = new object();

        public MySingletone()
        {

        }

        public static MySingletone GetIns()
        {
            if (ins == null)
            {
                lock (obj)
                {
                    if (ins == null)
                    {
                        ins = new MySingletone();
                    }
                }
            }

            return ins;
        }
    }
}
