﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyQueue
{
    public class MyQueue
    {
        LinkedList<int> list;
        public MyQueue()
        {
            list = new LinkedList<int>();
        }

        public void Enqueue(int a)
        {
            var result = list.AddLast(a);
        }
        
        public int Dequeue()
        {
            var result = list.First.Value;
            if (result == null)
                throw new Exception("datarka listd aper jan");
            list.RemoveFirst();

            return result;
        }
        
        public int GetMyFirst()
        {
            var result = list.First.Value;
            return result;
        }

        public int GetMyLast()
        {
            var result = list.Last.Value;
            return result;
        }

        public bool Contains(int b)
        {
            var result = list.Contains(b);

            return result;
        }

        public void NorMethod()
        {
            foreach (var item in list)
            {
                Console.Write(item + " ");
            }

            list.Clear();
        }
    }
}
