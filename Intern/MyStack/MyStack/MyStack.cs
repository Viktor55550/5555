﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStack
{
    public class MyStack<T> where T : //int, decimal
    {
        private T[] _arr;
        private int _currentIndex = -1;
        public MyStack(int Size)
        {
            _arr = new T[Size];
        }

        public void Push(T a)
        {
            _currentIndex++;
            _arr[_currentIndex] = a;
        }

        public T Pop()
        {
            if (_currentIndex < 0)
                throw new Exception("stack@ datark e");

            _currentIndex--;
            return _arr[_currentIndex];
        }

        public void Top()
        {
            Console.WriteLine(_currentIndex);
        }

        public void PushBottom(T Data)
        {
            if(_currentIndex == 0)
            {
                T temp = Pop();
                Push(Data);
                Push(temp);
            }
            else
            {
                T temp = Pop();
                PushBottom(Data);
                Push(temp);
            }
        }
    }
}
