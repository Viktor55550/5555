﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniqueElement
{
    class Program
    {
        public static void FindUniqueElement()
        {
            int[] Arr = new int[] { 1, 7, 1, 6, 5, 4, 5, 6, 7 };

            for (int i = 0; i < Arr.Length; i++)
            {
                bool IsFound = false;
                for (int j = 0; j < Arr.Length; j++)
                {
                    if (Arr[i] == Arr[j] && i != j)
                    {
                        IsFound = true;
                    }
                }
                if (IsFound == false)
                    Console.WriteLine(Arr[i]);
            }
        }
        static void Main(string[] args)
        {
            FindUniqueElement();
            Console.ReadLine();
        }
    }
}
