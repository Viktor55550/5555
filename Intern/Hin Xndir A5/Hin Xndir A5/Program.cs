﻿using System;

namespace Hin_Xndir_A5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Random rand = new Random();

            int[,] b = new int[5, 5];
            for (int i = 0; i < b.GetLength(0); i++)
            {
                for (int j = 0; j < b.GetLength(1); j++)
                {
                    b[i, j] = rand.Next(1, 99);
                    Console.Write(b[i, j] + "\t");
                }
                Console.WriteLine();
            }

            Console.WriteLine("====================================");
            int k = 0;

            for (int i = 0; i < b.GetLength(0); i++)
            {
                for (int j = 0; j < b.GetLength(1); j++)
                {
                    if (b[i, j] % 5 == 0)
                    {
                        k++;
                    }
                }
            }

            int[] newVector = new int[k];
            int temp = 0;

            for (int i = 0; i < b.GetLength(0); i++)
            {
                for (int j = 0; j < b.GetLength(1); j++)
                {
                    if (b[i, j] % 5 == 0)
                    {
                        newVector[temp] = b[i, j];
                        temp++;
                    }
                }
            }

            Console.WriteLine("My vector->");
            for (int i = 0; i < newVector.Length; i++)
            {
                Console.Write(newVector[i] + "\t");
            }
        }
    }
}
