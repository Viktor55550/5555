﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    class Program
    {
        public static void FizzBuzz()
        {
            for (int i = 1; i <= 100; i++)
            {
                string x = "";
                if (i % 3 == 0)
                    x = "Fizz";
                if (i % 5 == 0)
                    x = x + "Buzz";
                if (x.Length == 0)
                    x = i.ToString();
                Console.WriteLine(x);
            }
        }
        static void Main(string[] args)
        {
            FizzBuzz();
            Console.ReadKey();
            
            //loger solution
            for (int i = 1; i <= 100; i++)
            {
                if (i % 3 == 0 && i % 5 == 0)
                {
                    Console.WriteLine(i + " FizzBuzz");
                    continue;
                }
                if (i % 3 == 0)
                {
                    Console.WriteLine(i + " Fizz");
                    continue;
                }
                if (i % 5 == 0)
                {
                    Console.WriteLine(i + " Buzz");
                    continue;
                }

                Console.WriteLine(i);
            }
            
            
        }
    }
}
