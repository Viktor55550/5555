﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Permutation
{
    class Program
    {
        public static void IsPermutation(string tog1, string tog2)
        {
            char[] x1 = tog1.ToLower().ToCharArray();
            Array.Sort(x1);
            string y1 = new string(x1);//x1.ToString()

            char[] x2 = tog2.ToLower().ToCharArray();
            Array.Sort(x2);
            string y2 = new string(x2);//x2.ToString();

            if (y1.Equals(y2))
                Console.WriteLine($"{tog1} and {tog2} IsPermutation true");
            else
                Console.WriteLine($"{tog1} and {tog2} IsPermutation false"); 
        }
        static void Main(string[] args)
        {
            Console.Write($"Nermuceq arajin tog: ");
            string a = Console.ReadLine();
            Console.Write($"Nermuceq erkrord tog: ");
            string b = Console.ReadLine();

            IsPermutation(a, b);
            
            Console.ReadKey();
        }
    }
}
