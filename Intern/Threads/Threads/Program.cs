﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Threads
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                BankAcct acct = new BankAcct(10);
                Thread[] threads = new Thread[15];

                Thread.CurrentThread.Name = "main";

                for (int i = 0; i < 15; i++)
                {
                    Thread a = new Thread(new ThreadStart(acct.IssueWithdraw));
                    a.Name = i.ToString();
                    threads[i] = a;
                }

                for (int i = 0; i < 15; i++)
                {
                    Console.WriteLine($"Thread {threads[i].Name} Alive:" +
                        $" {threads[i].IsAlive}");
                    threads[i].Start();

                    Console.WriteLine($"Thread {threads[i].Name} Alive:" +
                        $" {threads[i].IsAlive}");
                }

                Console.WriteLine($"Current priority {Thread.CurrentThread.Priority}");

                Console.WriteLine($"Thread {Thread.CurrentThread.Name}");
            }

            /*int num = 1;
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(num);
                Thread.Sleep(1000);
                num++;
            }
            Console.WriteLine("ends");*/

            /*Thread t = new Thread(print1);
            t.Start();
            for (int i = 0; i < 1000; i++)
            {
                Console.Write(0);
            }*/

            Console.ReadKey();
        }

        static void print1()
        {
            for (int i = 0; i < 1000; i++)
            {
                Console.Write(1);
            }

            Console.ReadLine();
        }
    }

    public class BankAcct
    {
        private Object acctLock = new object();
        double Balance { get; set; }

        public BankAcct(double bal)
        {
            Balance = bal;
        }

        public double Withdraw(double amt)
        {
            if ((Balance - amt) < 0)
            {
                Console.WriteLine($"Sorry {Balance} in in Account");
                return Balance;
            }

            lock (acctLock)
            {
                if (Balance >= amt)
                {
                    Console.WriteLine($"Removed {amt} and {Balance - amt} left in Account");
                    Balance -= amt;
                }
                return Balance;
            }

        }
        public void IssueWithdraw()
        {
            Withdraw(1);
        }
    }
}
