﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbleSort
{
    class Program
    {
        static void Main(string[] args)
        {
           BubbleSort();
           Console.ReadKey();
        }

        public static void BubbleSort()
        {
            int[] a = new int[10];
            Random rand = new Random();
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = rand.Next(1, 100);//[)
            }
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write(a[i] + "  ");
                if (i == a.Length - 1)
                    Console.WriteLine(" unsorted \n");
            }
            
            int temp;
            for (int i = 0; i < a.Length; i++)
            {
                for (int j = 0; j < a.Length - 1; j++)
                {
                    if (a[j] > a[j + 1])
                    {
                        temp = a[j];
                        a[j] = a[j + 1];
                        a[j + 1] = temp;
                    }
                }
            }

            for (int i = 0; i < a.Length; i++)
            {
                Console.Write(a[i] + "  ");
                if (i == a.Length - 1)
                    Console.WriteLine(" sorted in asending order \n");
            }
            foreach (var item in a)
            {
                Console.Write(item + "  ");
            }
        }
    }
}
