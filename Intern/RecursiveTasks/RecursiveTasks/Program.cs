﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecursiveTasks
{
    class Program
    {
        public static int Fib(int n)
        {
            if (n < 3)
                return 1;
            else
                return (Fib(n - 2) + Fib(n - 1));
        }
        public static int Area(int n)
        {
            int x = 1;
            if (n == 1)
                return 1;
            else
                x = (n - 1) * 4;
            return Area(n - 1) + x;
        } 
        static void Main(string[] args)
        {
            Console.Write("nermuceq 1 ete fib : ");
            int log = int.Parse(Console.ReadLine());
            if (log == 1)
            {
                Console.Write("nermuceq tiv fib-i hamar : ");
                int a = int.Parse(Console.ReadLine());  
                int x = Fib(a);
                Console.WriteLine(x);
            }
            else
            {
                Console.Write("nermuceq tiv vandaki hamar: ");
                int b = int.Parse(Console.ReadLine());
                int y = Area(b);
                Console.WriteLine(y);
            }
            
            Console.ReadKey();
        }
    }
}
