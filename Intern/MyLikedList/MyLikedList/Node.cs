﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLinkedList
{
    public class Node
    {
        private int _data;
        private Node _next;

        public Node(int data, Node next)
        {
            this._data = data;
            this._next = next;
        }

        public int Data
        {
            get { return this._data; }
            set { this._data = value; }
        }

        public Node Next
        {
            get { return this._next; }
            set { this._next = value; }
        }
    }
}
