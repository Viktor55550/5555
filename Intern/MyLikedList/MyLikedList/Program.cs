﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            MyLinkedList list = new MyLinkedList();

            Console.WriteLine("is it empty? " + list.Empty);
            Console.WriteLine("count - " + list.Count);

            list.Add(1);
            list.Add(2);
            list.Add(1, 3);

            Console.WriteLine("is it empty? " + list.Empty);
            Console.WriteLine("count - " + list.Count);

            

            Console.ReadKey();
        }
    }
}
