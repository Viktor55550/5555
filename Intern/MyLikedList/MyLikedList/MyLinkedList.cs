﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLinkedList
{
    public class MyLinkedList
    {
        private Node head;
        private int count;

        public object this[int index]
        {
            get { return this.Get(index); }
        }


        public MyLinkedList()
        {
            this.head = null;
            this.count = 0;
        }

        public bool Empty
        {
            get { return this.count == 0; }
        }

        public int Count
        {
            get { return this.count; }
        }

        public int Add(int index, int a)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException("Index: " + index);

            if (index > count)
                index = count;

            Node current = this.head;

           
            if (this.Empty || index == 0)
            {
                this.head = new Node(a, this.head);
            }
            else
            {
                for (int i = 0; i < index - 1; i++)
                {
                    current = current.Next;

                    current.Next = new Node(a, current.Next);
                }
            }

            count++;

            return a;
        }

        public int Add(int a)
        {
            return this.Add(count, a);
        }

        public object Remove(int index)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException("Index: " + index);

            if (this.Empty)
                return null;

            if (index >= this.count)
                index = count - 1;


            Node current = this.head;
            object result = null;

            if (index == 0)
            {
                result = current.Data;
                this.head = current.Next;
            }
            else
            {
                for (int i = 0; i < index - 1; i++)
                    current = current.Next;

                result = current.Next.Data;
                current.Next = current.Next.Next;
            }

            count--;

            return result;
        }

        public void Clear()
        {
            this.head = null;
            this.count = 0;
        }


        public object Get(int index)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException("Index : " + index);

            if (this.Empty)
                return null;

            if (index >= this.count)
                index = this.count;

            Node current = this.head;

            for (int i = 0; i < index; i++)
                current = current.Next;

            return current.Data;
            

        }

        /*public int IndexOf(int a)
        {
            Node current = this.head;

            for (int i = 0; i < this.count; i++)
            {
                if (current.Data.Equals(a))
                    return i;

                current = current.Next;
            }

            return -1;
        }

        public bool Contains(int a)
        {
            return this.IndexOf(a) >= 0;
        }*/
    }
}
