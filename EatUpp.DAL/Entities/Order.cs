﻿using System;
using System.Collections.Generic;
using System.Text;
using EatUpp.DAL.DALInterfaces;

namespace EatUpp.DAL.Entities
{
    public class Order : IEntityWithId
    {
        public int Id { get ; set ; }

        public int RestaurantId { get; set; }

        public int ChairCount { get; set; }

        public DateTime OrderDate { get; set; }

        public DateTime? CheckDate { get; set; }

        public DateTime? CancelDate { get; set; }

        public bool VIPZone { get; set; }

        public bool SmokingZone { get; set; }


        public Restaurant Restaurant { get; set; }

        public User User { get; set; }
    }
}
