﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace First_DB_app01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("skizb");
            AddUsersToDb();
            GetUsersFromDb();
            Console.WriteLine("verj");

            Console.ReadKey();
        }

        public static void AddUsersToDb()
        {
            using (NWDContext db = new NWDContext())
            {
                // создаем два объекта User
                User user1 = new User { Name = "Tom", Age = 33 };
                User user2 = new User { Name = "Alice", Age = 26 };
                // добавляем их в бд
                db.Users.Add(user1);
                db.Users.Add(user2);
                db.SaveChanges();
                Console.WriteLine("Objects saved");

                
            }
        }
        
        public static void GetUsersFromDb()
        {
            using (NWDContext db = new NWDContext())
            {
                List<User> users = db.Users.ToList();
                
                foreach (User item in users)
                {
                    Console.WriteLine($"{item.Id} - {item.Name} - {item.Age}");
                }
            }
        }
    }
}
