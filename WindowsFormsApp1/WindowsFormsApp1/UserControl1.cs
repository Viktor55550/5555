﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class UserControl1 : UserControl
    {
        Bitmap _bitmap; 
        public UserControl1()
        {
            InitializeComponent();
            _bitmap = new Bitmap(Image.FromFile(@"C:\Users\STUDENT-06\Desktop\001.jpg.webp"));
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void UserControl1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.FillRectangle(Brushes.White, 0, 0, Width, Height);
            g.DrawImage(_bitmap, 10, 10, trackBar1.Value, trackBar2.Value);
        }
    }
}
