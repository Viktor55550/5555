﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            MyQueue myQueue = new MyQueue();
            

            myQueue.Enqueue(5);
            myQueue.Enqueue(6);
            myQueue.Enqueue(7);
            myQueue.Enqueue(45);
            myQueue.Enqueue(234);
            myQueue.Enqueue(75);
            myQueue.Enqueue(35);
            myQueue.Enqueue(354);
            myQueue.Enqueue(212);

            bool tram = myQueue.Contains(5);
            Console.WriteLine(tram);

            int a = myQueue.Dequeue();
            Console.WriteLine(a + " dequeue linelu e '5'");
            
            bool tram2 = myQueue.Contains(5);
            Console.WriteLine(tram2);

            int b = myQueue.GetMyFirst();
            Console.WriteLine(b + " my first");
            int c = myQueue.GetMyLast();
            Console.WriteLine(c + " my last");

            myQueue.NorMethod();

            //int x = myQueue.Dequeue();
            //Console.WriteLine(x + " mijankyal dequeue '?'");


            myQueue.Enqueue(15);
            myQueue.Enqueue(16);
            myQueue.Enqueue(17);
            myQueue.Enqueue(425);
            myQueue.Enqueue(24);
            myQueue.Enqueue(5);
            myQueue.Enqueue(5);
            myQueue.Enqueue(54);
            myQueue.Enqueue(12);

            Console.WriteLine();

            int d = myQueue.Dequeue();
            Console.WriteLine(d + " erkrord dequeue linelu e '15'");

            Console.ReadKey();
        }
    }
}
