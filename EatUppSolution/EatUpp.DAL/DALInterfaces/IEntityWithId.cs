﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.DAL.DALInterfaces
{
    public interface IEntityWithId
    {
        int id { get; set; }
    }
}
