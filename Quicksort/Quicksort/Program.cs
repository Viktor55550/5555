﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quicksort
{
    class Program
    {
        static void Main(string[] args)
        {
            //int[] numbers = new int[] { 1, 5, 0, 34, 3, 8 };
            int[] numbers = new int[50]; //{ 1, 5, 0, 34, 3, 8 };

            Random rand = new Random();

            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = rand.Next(1,100);
            }

            for (int i = 0; i < numbers.Length; i++)
            {
                Console.Write(numbers[i] + " ");

                if (i == numbers.Length - 1)
                    Console.WriteLine(" unsorted \n");
            }

            SortArray(numbers);

            for (int i = 0; i < numbers.Length; i++)
            {
                Console.Write(numbers[i] + " ");

                if (i == numbers.Length - 1)
                    Console.WriteLine(" sorted \n");
            }

            /*foreach (var item in numbers)
            {
                Console.WriteLine(item);
            }*/

            Console.ReadKey();
        }

        private static void SortArray(int[] numbers)
        {
            Quicksort(numbers, 0, numbers.Length - 1);
        }

        private static void Quicksort(int[] numbers, int left, int right)
        {
            int i = left;
            int j = right;

            int mid = numbers[(left + right) / 2];

            while(i <= j)
            {
                while (numbers[i] < mid)
                    i++;

                while (numbers[j] > mid)
                    j--;


                if(i <= j)
                {
                    int temp = numbers[i];
                    numbers[i] = numbers[j];
                    numbers[j] = temp;

                    i++;
                    j--;
                }
            }

            if (left < j)
                Quicksort(numbers, left, j);

            if (i < right)
                Quicksort(numbers, i, right);

        }
    }
}
