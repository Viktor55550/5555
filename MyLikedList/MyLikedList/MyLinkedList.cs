﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLinkedList
{
    public class MyLinkedList
    {
        public int count;
        public Node head;
       

        public object this[int index]
        {
            get { return Get(index); }
        }

        public MyLinkedList()
        {
            head = null;
            count = 0;
        }

        public bool IsEmpty
        {
            get { return count == 0; }
        }

        public int Count
        {
            get { return count; }
        }

        public Node Add(int index, int a)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException("Index: " + index);

            if (index > count)
                throw new ArgumentOutOfRangeException("Index: " + index);

            Node current = head;
           
            if (this.IsEmpty || index == 0)
            {
                head = new Node(a, null);
            }
            else
            {
                for (int i = 0; i < index - 1; i++)
                {
                    current = current.Next;

                    current.Next = new Node(a, current.Next);
                }
            }

            count++;

            return current;
        }

        public Node Add(int a)
        {
            return Add(count, a);
        }

        public void Remove(int index)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException("Index: " + index);

            if (index >= count)
                throw new ArgumentOutOfRangeException("Index: " + index);

            Node current = head;

            if (index == 0)
            {
                head = current.Next;
            }
            else
            {
                for (int i = 0; i < index - 1; i++)
                    current = current.Next;

                current.Next = current?.Next?.Next;
            }

            count--;
        }

        public void Clear()
        {
            head = null;
            count = 0;
        }

        public int Get(int index)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException("Index : " + index);

            if (index >= count)
                throw new ArgumentOutOfRangeException("Index : " + index);

            Node current = head;

            for (int i = 0; i < index; i++)
                current = current.Next;

            return current.Data;
        }

        /*public int IndexOf(int a)
        {
            Node current = head;

            for (int i = 0; i < this.count; i++)
            {
                if (current.Data.Equals(a))
                    return i;

                current = current.Next;
            }

            return -1;
        }

        public bool Contains(int a)
        {
            return IndexOf(a) >= 0;
        }*/
    }
}
