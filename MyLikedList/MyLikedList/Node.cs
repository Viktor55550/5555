﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLinkedList
{
    public class Node
    {
        public int _data;
        public Node _next;

        public Node(int data, Node next)
        {
            _data = data;
            _next = next;
        }

        public int Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public Node Next
        {
            get { return this._next; }
            set { this._next = value; }
        }
    }
}
