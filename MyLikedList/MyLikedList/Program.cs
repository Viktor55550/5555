﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            MyLinkedList myLlist = new MyLinkedList();

            Console.WriteLine("is it empty? " + myLlist.IsEmpty);
            Console.WriteLine("count - " + myLlist.Count);

            myLlist.Add(1);
            myLlist.Add(2);
            myLlist.Add(1, 3);

            //myLlist.Remove(1);
            Console.WriteLine("is it empty? " + myLlist.IsEmpty);
            Console.WriteLine("count - " + myLlist.Count);

            

            Console.ReadKey();
        }
    }
}
